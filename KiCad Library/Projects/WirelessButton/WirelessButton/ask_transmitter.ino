// ask_transmitter.pde
// -*- mode: C++ -*-
// Simple example of how to use RadioHead to transmit messages
// with a simple ASK transmitter in a very simple way.
// Implements a simplex (one-way) transmitter with an TX-C1 module
// Tested on Arduino Mega, Duemilanova, Uno, Due, Teensy, ESP-12

#include <RH_ASK.h>
#ifdef RH_HAVE_HARDWARE_SPI
#include <SPI.h> // Not actually used but needed to compile
#include "LowPower.h"
#endif

volatile RH_ASK driver(2000, 12, 13); // Use pin 1 for transmit, pin 13 for dummy recieve
const int wakeUpPin = 2;

void InterrupSendMessage()
{

}

void setup()
{
  //pinMode(1,OUTPUT);
  #ifdef RH_HAVE_SERIAL
   Serial.begin(9600);  // Debugging only
  #endif
  if (!driver.init())
  #ifdef RH_HAVE_SERIAL
    Serial.println("init failed");
  #else
    ;
  #endif

  pinMode(wakeUpPin,INPUT_PULLUP); //set pin3 to a pullup to be used in conjunction with a button
  attachInterrupt(digitalPinToInterrupt(wakeUpPin),InterrupSendMessage,FALLING);
  //pinMode(13,OUTPUT);
}

void loop()
{

  //put part into a deep sleep mode to save power
  Serial.println("going to sleep");
  LowPower.powerDown(SLEEP_FOREVER, ADC_OFF, BOD_OFF);

  //Determin motor movement, and ButtonID info
  int i;
  char ButtonID = 64;
  char MotorRot = 0;

  for(i=0; i<4; i++){
    ButtonID = ButtonID + (digitalRead(i+3)<<i);
    Serial.println((uint8_t)ButtonID);
  }

  for(i=0; i<4; i++){
    MotorRot = MotorRot + (digitalRead(i+7)<<i);
  }

  //Generate string to send
  int msg_len = 12; //number of characters in the string
  char msg[msg_len] = {'b', 'U', 't', '_', 'H', 'I', 'T', ButtonID, MotorRot, 'E', 'N', 'D'};
  for(i=0; i<3; i++)
  {
    //send message
    driver.send((uint8_t *)msg, msg_len);
    driver.waitPacketSent();
  }
  while(digitalRead(wakeUpPin) == 0)
  {
    delay(100);
  }
}

