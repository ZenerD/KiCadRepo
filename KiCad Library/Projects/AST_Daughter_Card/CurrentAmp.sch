EESchema Schematic File Version 2
LIBS:Capacitors
LIBS:Connectors
LIBS:Discretes
LIBS:ICs
LIBS:Allegro
LIBS:Resistors
LIBS:TestPoints
LIBS:Asek20DaughterCard-cache
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 3 8
Title "ASEK20 Daughter Card"
Date "Friday, January 23, 2015"
Rev "v1.2"
Comp "Allegro Microsystems LLC"
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L 2-BE R23
U 1 1 54A63490
P 4250 4000
F 0 "R23" H 4250 4196 60  0000 C CNN
F 1 "2-BE" H 4150 4100 60  0000 C CNN
F 2 "Resistors:R0603" H 4250 4000 60  0001 C CNN
F 3 "" H 4350 4150 60  0000 C CNN
F 4 "1276-4476-1-ND" H 4268 4268 60  0001 C CNN "DigikeyPN"
F 5 "1%" H 4450 4100 60  0000 C CNN "Precision"
	1    4250 4000
	0    -1   -1   0   
$EndComp
Wire Wire Line
	4250 3400 4250 3800
Text Label 4500 4200 2    60   ~ 0
GND
Wire Wire Line
	6450 3850 7550 3850
Text HLabel 1100 700  0    60   Input ~ 0
5v0SMPS
Text HLabel 1100 800  0    60   Input ~ 0
GND
Text Label 1200 700  0    60   ~ 0
5vPWR
Wire Wire Line
	1100 700  1500 700 
Wire Wire Line
	1100 800  1500 800 
Text Label 1400 800  2    60   ~ 0
GND
Text HLabel 7550 3850 2    60   Output ~ 0
ICC_Measure
Wire Wire Line
	4600 4200 4250 4200
Wire Wire Line
	5550 4200 5900 4200
Connection ~ 5750 4200
Wire Wire Line
	5750 3900 5750 4200
Text HLabel 4250 2800 2    60   Input ~ 0
DUT_GND
$Comp
L LMP2022 U5
U 2 1 54F24351
P 6100 3850
F 0 "U5" H 6300 3700 60  0000 C CNN
F 1 "LMP2022" H 5950 3600 60  0000 C CNN
F 2 "ICs:8-TSSOP" H 6100 3850 60  0001 C CNN
F 3 "" H 6250 4250 60  0000 C CNN
F 4 "LMP2022MME/NOPBCT-ND" H 6350 4350 60  0001 C CNN "DigikeyPN"
	2    6100 3850
	1    0    0    1   
$EndComp
Wire Wire Line
	4250 2700 4250 3000
$Comp
L DMN3026LVT Q9
U 1 1 55024154
P 3300 3250
F 0 "Q9" H 3450 3200 60  0000 C CNN
F 1 "DMN3026LVT" H 3650 3300 60  0000 C CNN
F 2 "Discretes:TSOT236" H 3612 3280 60  0001 C CNN
F 3 "" H 3612 3280 60  0000 C CNN
F 4 "DMN3026LVT-7DICT-ND" H 3722 3852 60  0001 C CNN "DigikeyPN"
	1    3300 3250
	1    0    0    -1  
$EndComp
Wire Wire Line
	3150 3000 3150 2950
Wire Wire Line
	3150 2950 4250 2950
Wire Wire Line
	3450 2950 3450 3000
Wire Wire Line
	3350 3000 3350 2950
Connection ~ 3350 2950
Wire Wire Line
	3250 3000 3250 2950
Connection ~ 3250 2950
Connection ~ 4250 2950
Connection ~ 3450 2950
Wire Wire Line
	3350 3450 3350 3500
Wire Wire Line
	3350 3500 4250 3500
Connection ~ 4250 3500
Text HLabel 2700 3250 0    60   Input ~ 0
GPIO0
Wire Wire Line
	2700 3250 3100 3250
$Comp
L 5006K TP12
U 1 1 55024E72
P 4250 2700
F 0 "TP12" H 4250 3050 60  0000 C CNN
F 1 "GND" H 4250 2950 60  0000 C CNN
F 2 "TestPoints:TP_1p6mm" H 4250 2700 60  0001 C CNN
F 3 "" H 4250 2700 60  0000 C CNN
F 4 "5006K-ND" H 4500 3050 60  0001 C CNN "DigikeyPN"
	1    4250 2700
	1    0    0    -1  
$EndComp
$Comp
L ProtoTP TP184
U 1 1 54F2E90F
P 7400 3800
F 0 "TP184" H 7400 4150 60  0000 C CNN
F 1 "ICC" H 7400 4050 60  0000 C CNN
F 2 "TestPoints:TP_1p0mm" H 7400 3800 60  0001 C CNN
F 3 "" H 7400 3800 60  0000 C CNN
	1    7400 3800
	1    0    0    -1  
$EndComp
Wire Wire Line
	7400 3800 7400 3850
Connection ~ 7400 3850
$Comp
L 47.5-CE R22
U 1 1 553B900B
P 4250 3200
F 0 "R22" H 4250 3396 60  0000 C CNN
F 1 "47.5-CE" H 4150 3300 60  0000 C CNN
F 2 "Resistors:R0805" H 4250 3200 60  0001 C CNN
F 3 "" H 4350 3350 60  0000 C CNN
F 4 "RNCP0805FTD47R5CT-ND" H 4268 3468 60  0001 C CNN "DigikeyPN"
F 5 "1%" H 4450 3300 60  0000 C CNN "Precision"
	1    4250 3200
	0    -1   -1   0   
$EndComp
$Comp
L 14-AE R24
U 1 1 55397F33
P 4800 4200
F 0 "R24" H 4800 4396 60  0000 C CNN
F 1 "14-AE" H 4700 4300 60  0000 C CNN
F 2 "Resistors:R0402" H 4800 4200 60  0001 C CNN
F 3 "" H 4900 4350 60  0000 C CNN
F 4 "P14.0LCT-ND" H 4818 4468 60  0001 C CNN "DigikeyPN"
F 5 "1%" H 5000 4300 60  0000 C CNN "Precision"
	1    4800 4200
	1    0    0    -1  
$EndComp
$Comp
L 976-BE R25
U 1 1 55397FD0
P 5350 4200
F 0 "R25" H 5350 4396 60  0000 C CNN
F 1 "976-BE" H 5250 4300 60  0000 C CNN
F 2 "Resistors:R0603" H 5350 4200 60  0001 C CNN
F 3 "" H 5450 4350 60  0000 C CNN
F 4 "P976HCT-ND" H 5368 4468 60  0001 C CNN "DigikeyPN"
F 5 "1%" H 5550 4300 60  0000 C CNN "Precision"
	1    5350 4200
	1    0    0    -1  
$EndComp
Wire Wire Line
	5150 4200 5000 4200
$Comp
L 3.01k-CE R77
U 1 1 5539806C
P 6750 4200
F 0 "R77" H 6750 4396 60  0000 C CNN
F 1 "3.01k-CE" H 6650 4300 60  0000 C CNN
F 2 "Resistors:R0603" H 6750 4200 60  0001 C CNN
F 3 "" H 6850 4350 60  0000 C CNN
F 4 "P3.01KHCT-ND" H 6768 4468 60  0001 C CNN "DigikeyPN"
F 5 "1%" H 6950 4300 60  0000 C CNN "Precision"
	1    6750 4200
	1    0    0    -1  
$EndComp
$Comp
L 95.3k-BD R78
U 1 1 553980C8
P 6100 4200
F 0 "R78" H 6100 4396 60  0000 C CNN
F 1 "95.3k-BD" H 6000 4300 60  0000 C CNN
F 2 "Resistors:R0603" H 6100 4200 60  0001 C CNN
F 3 "" H 6200 4350 60  0000 C CNN
F 4 "P95.3KHCT-ND" H 6118 4468 60  0001 C CNN "DigikeyPN"
F 5 "0.1%" H 6350 4300 60  0000 C CNN "Precision"
	1    6100 4200
	1    0    0    -1  
$EndComp
Wire Wire Line
	6950 4200 7150 4200
Wire Wire Line
	7150 4200 7150 3850
Connection ~ 7150 3850
Wire Wire Line
	6550 4200 6300 4200
Wire Wire Line
	4250 2900 5650 2900
Wire Wire Line
	5650 2900 5650 3800
Wire Wire Line
	5650 3800 5750 3800
Connection ~ 4250 2900
$Comp
L DMG1012T Q21
U 1 1 553991C9
P 6100 4850
F 0 "Q21" V 6250 4850 60  0000 C CNN
F 1 "DMG1012T" V 5950 5150 60  0000 C CNN
F 2 "Discretes:SOT523" H 5850 5000 60  0001 C CNN
F 3 "" H 5850 5000 60  0000 C CNN
F 4 "DMG1012T-7DICT-ND" H 6600 5000 60  0001 C CNN "DigikeyPN"
	1    6100 4850
	0    1    -1   0   
$EndComp
Wire Wire Line
	5900 4200 5900 4850
Wire Wire Line
	6300 4200 6300 4850
$Comp
L DMG1012T Q20
U 1 1 55399456
P 5450 5500
F 0 "Q20" H 5600 5550 60  0000 C CNN
F 1 "DMG1012T" H 5750 5450 60  0000 C CNN
F 2 "Discretes:SOT523" H 5200 5650 60  0001 C CNN
F 3 "" H 5200 5650 60  0000 C CNN
F 4 "DMG1012T-7DICT-ND" H 5950 5650 60  0001 C CNN "DigikeyPN"
	1    5450 5500
	1    0    0    -1  
$EndComp
Text Label 2800 3250 0    60   ~ 0
GPIO0
Text Label 4950 5500 0    60   ~ 0
GPIO0
Wire Wire Line
	5250 5500 4950 5500
Text Label 5450 4650 1    60   ~ 0
5vPWR
$Comp
L 100k-AF R79
U 1 1 55399DE8
P 5450 4900
F 0 "R79" H 5450 5096 60  0000 C CNN
F 1 "100k-AF" H 5350 5000 60  0000 C CNN
F 2 "Resistors:R0402" H 5450 4900 60  0001 C CNN
F 3 "" H 5550 5050 60  0000 C CNN
F 4 "P100KJCT-ND" H 5468 5168 60  0001 C CNN "DigikeyPN"
F 5 "5%" H 5650 5000 60  0000 C CNN "Precision"
	1    5450 4900
	0    1    1    0   
$EndComp
Wire Wire Line
	5450 5100 5450 5300
Wire Wire Line
	5450 4350 5450 4700
Wire Wire Line
	6100 5050 6100 5200
Wire Wire Line
	6100 5200 5450 5200
Connection ~ 5450 5200
Text Label 5450 5800 3    60   ~ 0
GND
Wire Wire Line
	5450 5700 5450 5950
$Comp
L SP1003 D?
U 1 1 5768FEE2
P 4550 3200
F 0 "D?" H 4622 3306 60  0000 L CNN
F 1 "SP1003" H 4622 3200 60  0000 L CNN
F 2 "Discretes:SOD882" H 4183 3396 60  0001 C CNN
F 3 "" H 4550 3200 60  0000 C CNN
F 4 "F4088CT-ND" H 4383 3596 60  0001 C CNN "DigikeyPN"
F 5 "6V" H 4622 3094 60  0000 L CNN "Clamp"
	1    4550 3200
	1    0    0    -1  
$EndComp
Wire Wire Line
	4550 3050 4550 2900
Connection ~ 4550 2900
Wire Wire Line
	4550 3350 4550 4200
Connection ~ 4550 4200
$EndSCHEMATC
