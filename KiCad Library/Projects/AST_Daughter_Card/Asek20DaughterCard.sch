EESchema Schematic File Version 2
LIBS:Capacitors
LIBS:Connectors
LIBS:Discretes
LIBS:ICs
LIBS:Allegro
LIBS:Resistors
LIBS:TestPoints
LIBS:Asek20DaughterCard-cache
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 8
Title "ASEK20 Daughter Card"
Date "Friday, January 23, 2015"
Rev "v1.2"
Comp "Allegro Microsystems LLC"
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Sheet
S 6250 1200 1150 1200
U 54988839
F0 "IDDQ" 60
F1 "IDDQ_Measure.sch" 60
F2 "DAC0_in" I L 6250 2000 60 
F3 "5v0SMPS" I L 6250 1300 60 
F4 "GND" I L 6250 1400 60 
F5 "VREGD_Measure" O R 7400 1550 60 
F6 "IDDQ_Measure" O R 7400 1650 60 
F7 "GPIO0" I L 6250 2100 60 
F8 "GPIO1" I L 6250 2200 60 
F9 "GPIO2" I L 6250 2300 60 
$EndSheet
$Comp
L ASEK20_Conn J1
U 1 1 54A6481F
P 1800 2300
F 0 "J1" H 1800 3350 60  0000 C CNN
F 1 "ASEK20_Conn" H 1800 3450 60  0000 C CNN
F 2 "Connectors:MOL0878315020" H 1800 2300 60  0001 C CNN
F 3 "" H 1800 3350 60  0000 C CNN
F 4 "WM18568-ND" H 1850 3550 60  0001 C CNN "DigikeyPN"
	1    1800 2300
	1    0    0    -1  
$EndComp
Wire Wire Line
	1050 1300 1350 1300
Wire Wire Line
	2250 1300 2550 1300
Text Label 1050 1300 0    60   ~ 0
5VPWR
Text Label 2300 1300 0    60   ~ 0
5VPWR
Text Label 6200 1300 2    60   ~ 0
5VPWR
Text Label 6200 2900 2    60   ~ 0
5VPWR
Wire Wire Line
	6250 2900 5900 2900
Wire Wire Line
	5900 1300 6250 1300
NoConn ~ 1350 1400
NoConn ~ 2250 1600
Wire Wire Line
	1350 1500 1050 1500
Text Label 1100 1500 0    60   ~ 0
GND
Text Label 1100 1900 0    60   ~ 0
GND
Text Label 2350 2000 0    60   ~ 0
GND
Text Label 2350 2400 0    60   ~ 0
GND
Text Label 2350 2700 0    60   ~ 0
GND
Text Label 1100 3100 0    60   ~ 0
GND
Wire Wire Line
	1050 3100 1350 3100
Wire Wire Line
	1350 1900 1050 1900
Wire Wire Line
	2250 2700 2550 2700
Wire Wire Line
	2250 2400 2550 2400
Wire Wire Line
	2250 2000 2550 2000
Wire Wire Line
	2250 1700 2550 1700
Text Label 2350 1700 0    60   ~ 0
GND
Text Label 1100 2900 0    60   ~ 0
GND
Wire Wire Line
	1350 2900 1050 2900
Wire Wire Line
	2250 1400 2550 1400
Text Label 2300 1400 0    60   ~ 0
3v3PWR
Wire Wire Line
	2250 3700 2550 3700
Wire Wire Line
	1350 3300 1050 3300
Wire Wire Line
	1350 3400 1050 3400
Wire Wire Line
	1350 3500 1050 3500
Wire Wire Line
	1350 3600 1050 3600
Wire Wire Line
	1350 3700 1050 3700
Wire Wire Line
	2250 3600 2550 3600
Wire Wire Line
	2250 3500 2550 3500
Wire Wire Line
	2250 3400 2550 3400
Wire Wire Line
	2250 3300 2550 3300
Text Label 2250 3300 0    60   ~ 0
GPIO_0
Text Label 1350 3400 2    60   ~ 0
GPIO_1
Wire Wire Line
	6250 2300 5900 2300
Wire Wire Line
	6250 2200 5900 2200
Wire Wire Line
	6250 3450 5900 3450
Wire Wire Line
	6250 3550 5900 3550
Text Label 7450 3550 0    60   ~ 0
GPIO_0
Wire Wire Line
	7400 3550 7750 3550
Wire Wire Line
	7400 3650 7750 3650
Text Label 7450 3650 0    60   ~ 0
GPIO_1
Text Label 1350 3300 2    60   ~ 0
D_PWR
Text Label 2250 3700 0    60   ~ 0
GP_PWR
$Sheet
S 8750 1200 1100 1250
U 54A62FA1
F0 "CurrentAmp" 60
F1 "CurrentAmp.sch" 60
F2 "5v0SMPS" I L 8750 1300 60 
F3 "GND" I L 8750 1400 60 
F4 "ICC_Measure" O R 9850 1800 60 
F5 "DUT_GND" I R 9850 1650 60 
F6 "GPIO0" I L 8750 2350 60 
$EndSheet
Text Label 8700 1300 2    60   ~ 0
5VPWR
Wire Wire Line
	8750 1300 8400 1300
Text Label 5950 1400 0    60   ~ 0
GND
Wire Wire Line
	6250 1400 5900 1400
Text Label 8450 1400 0    60   ~ 0
GND
Wire Wire Line
	8750 1400 8400 1400
Wire Wire Line
	6250 3000 5900 3000
Text Label 5950 3000 0    60   ~ 0
GND
Wire Wire Line
	1350 2100 1050 2100
Text Label 1350 2200 2    60   ~ 0
IDDQ_DAC
Text Label 6250 2000 2    60   ~ 0
IDDQ_DAC
Wire Wire Line
	6250 2100 5900 2100
Wire Wire Line
	7750 1550 7400 1550
Wire Wire Line
	7400 1650 7750 1650
Wire Wire Line
	7400 3250 7750 3250
Wire Wire Line
	9850 1800 10150 1800
Text Label 7450 1550 0    60   ~ 0
VREGD
Text Label 7450 1650 0    60   ~ 0
IDDQ
Text Label 7500 3250 0    60   ~ 0
Mux
Text Label 9950 1800 0    60   ~ 0
ICC
Text Label 2350 1900 0    60   ~ 0
Mux
Text Label 1300 1700 2    60   ~ 0
Mux
Wire Wire Line
	1350 1700 1050 1700
Wire Wire Line
	2250 1900 2550 1900
Text Label 1250 2300 2    60   ~ 0
ICC
Text Label 1250 2000 2    60   ~ 0
ICC
Wire Wire Line
	1350 2000 1050 2000
Wire Wire Line
	1350 2300 1050 2300
Wire Wire Line
	1350 1800 1050 1800
Wire Wire Line
	2250 1800 2550 1800
Text Label 2300 1800 0    60   ~ 0
VREGD
Text Label 1300 1800 2    60   ~ 0
IDDQ
Wire Wire Line
	2250 1500 2550 1500
Text Label 2350 1500 0    60   ~ 0
VCC
Wire Wire Line
	1350 2500 1050 2500
Wire Wire Line
	1350 2400 1050 2400
Wire Wire Line
	2250 2500 2550 2500
Wire Wire Line
	2250 2300 2550 2300
Text Label 2300 2300 0    60   ~ 0
S_CLK
Text Label 1100 2400 0    60   ~ 0
S_IN
Text Label 1100 2500 0    60   ~ 0
S_EN
Text Label 2300 2500 0    60   ~ 0
S_OUT
Text Label 1100 2600 0    60   ~ 0
GND
Wire Wire Line
	1350 2600 1050 2600
Text Label 750  5900 0    60   ~ 0
S_CLK
Text Label 1950 6000 0    60   ~ 0
S_OUT
Text Label 1950 6100 0    60   ~ 0
S_IN
Text Label 1950 6200 0    60   ~ 0
S_EN
Wire Wire Line
	1650 5900 2100 5900
Wire Wire Line
	1950 6000 2500 6000
Wire Wire Line
	1950 6100 2900 6100
Wire Wire Line
	1950 6200 3250 6200
$Sheet
S 8750 2750 1100 1300
U 54AA0D0C
F0 "AdditionalADC" 60
F1 "AdditionalADC.sch" 60
F2 "SPI_mosi" O R 9850 2900 60 
F3 "SPI_miso" O R 9850 3000 60 
F4 "SPI_clk" O R 9850 3100 60 
F5 "SPI_cs" O R 9850 3200 60 
F6 "5v0SMPS" I L 8750 2850 60 
F7 "GND" I L 8750 2950 60 
F8 "Mux" I L 8750 3250 60 
F9 "Mux2" I L 8750 3750 60 
F10 "VREGD" I L 8750 3350 60 
F11 "ICC" I L 8750 3550 60 
F12 "IDDQ" I L 8750 3450 60 
F13 "VCC" I L 8750 3650 60 
F14 "3v3PWR" I L 8750 3050 60 
F15 "Sense_1" I L 8750 3850 60 
F16 "Sense_2" I L 8750 3950 60 
$EndSheet
Text Label 8700 2850 2    60   ~ 0
5VPWR
Text Label 8600 2950 2    60   ~ 0
GND
Wire Wire Line
	2250 2600 2550 2600
Text Label 2300 2600 0    60   ~ 0
SPI_clk
Text Label 1300 2700 2    60   ~ 0
SPI_mosi
Text Label 1300 2800 2    60   ~ 0
SPI_cs
Text Label 2300 2800 0    60   ~ 0
SPI_miso
Wire Wire Line
	2250 2800 2550 2800
Wire Wire Line
	1350 2800 1050 2800
Wire Wire Line
	1350 2700 1050 2700
Text Label 9850 3100 0    60   ~ 0
SPI_clk
Text Label 9850 3000 0    60   ~ 0
SPI_miso
Text Label 9850 3200 0    60   ~ 0
SPI_cs
Text Label 9850 2900 0    60   ~ 0
SPI_mosi
Text Label 8700 3250 2    60   ~ 0
Mux
Text Label 8700 3350 2    60   ~ 0
VREGD
Text Label 8700 3450 2    60   ~ 0
IDDQ
Text Label 8700 3550 2    60   ~ 0
ICC
Text Label 8700 3650 2    60   ~ 0
VCC
Wire Wire Line
	9850 2900 10150 2900
Wire Wire Line
	9850 3000 10150 3000
Wire Wire Line
	9850 3100 10150 3100
Wire Wire Line
	9850 3200 10150 3200
Wire Wire Line
	8400 2850 8750 2850
Wire Wire Line
	8400 2950 8750 2950
Text Label 8700 3750 2    60   ~ 0
Mux2
NoConn ~ 2250 3200
Wire Wire Line
	8750 3250 8400 3250
Wire Wire Line
	8750 3350 8400 3350
Wire Wire Line
	8750 3450 8400 3450
Wire Wire Line
	8750 3550 8400 3550
Wire Wire Line
	8400 3650 8750 3650
Wire Wire Line
	8750 3750 8400 3750
Text Label 4850 6850 2    60   ~ 0
Mux2
Wire Wire Line
	4650 6850 5150 6850
Text Label 1650 6750 0    60   ~ 0
3v3PWR
$Comp
L 0.0-BF R1
U 1 1 54A926EB
P 2450 6750
F 0 "R1" H 2450 6946 60  0000 C CNN
F 1 "0.0-BF" H 2350 6850 60  0000 C CNN
F 2 "Resistors:R0603" H 2450 6750 60  0001 C CNN
F 3 "" H 2550 6900 60  0000 C CNN
F 4 "P0.0GCT-ND" H 2468 7018 60  0001 C CNN "DigikeyPN"
F 5 "5%" H 2650 6850 60  0000 C CNN "Precision"
	1    2450 6750
	1    0    0    -1  
$EndComp
$Comp
L 0.0-BF R2
U 1 1 54A92768
P 2450 7050
F 0 "R2" H 2450 7246 60  0000 C CNN
F 1 "0.0-BF" H 2350 7150 60  0000 C CNN
F 2 "Resistors:R0603" H 2450 7050 60  0001 C CNN
F 3 "" H 2550 7200 60  0000 C CNN
F 4 "P0.0GCT-ND" H 2468 7318 60  0001 C CNN "DigikeyPN"
F 5 "5%" H 2650 7150 60  0000 C CNN "Precision"
	1    2450 7050
	1    0    0    -1  
$EndComp
Wire Wire Line
	1600 6750 2250 6750
Wire Wire Line
	1600 6750 1600 7050
Wire Wire Line
	1600 7050 2250 7050
Text Label 2800 6750 0    60   ~ 0
GP_PWR
Wire Wire Line
	2650 6750 3300 6750
Text Label 2800 7050 0    60   ~ 0
D_PWR
Wire Wire Line
	2650 7050 3500 7050
Wire Wire Line
	6250 3350 5900 3350
Wire Wire Line
	6250 3250 5900 3250
Wire Wire Line
	1350 2200 1050 2200
Text Label 1350 2100 2    60   ~ 0
EFORCE_P
Text Label 2250 2100 0    60   ~ 0
EFORCE_N
Wire Wire Line
	2250 2100 2550 2100
$Comp
L ASEK3_Conn J2
U 1 1 54BC959C
P 1800 4600
F 0 "J2" H 1800 4900 60  0000 C CNN
F 1 "ASEK3_Conn" H 1800 5000 60  0000 C CNN
F 2 "Connectors:MOL0878311420" H 1800 4600 60  0001 C CNN
F 3 "" H 1800 4900 60  0000 C CNN
F 4 "WM17469-ND" H 1800 5100 60  0001 C CNN "DigikeyPN"
	1    1800 4600
	1    0    0    -1  
$EndComp
Text Label 2300 4350 0    60   ~ 0
VCC
Wire Wire Line
	2250 2900 2550 2900
Wire Wire Line
	2250 4350 2550 4350
Wire Wire Line
	2250 4450 2550 4450
Wire Wire Line
	1050 4350 1350 4350
Wire Wire Line
	1350 4450 1050 4450
Wire Wire Line
	1050 4650 1350 4650
Wire Wire Line
	1050 4750 1350 4750
Wire Wire Line
	1350 4850 1050 4850
Wire Wire Line
	2550 4650 2250 4650
Wire Wire Line
	2250 4750 2550 4750
Wire Wire Line
	2550 4850 2250 4850
Wire Wire Line
	2250 4950 2550 4950
Wire Wire Line
	1050 3000 1350 3000
Text Label 1150 4850 0    60   ~ 0
SDA
Text Label 2300 2900 0    60   ~ 0
SDA
Text Label 2300 4850 0    60   ~ 0
SCL
Text Label 1100 3000 0    60   ~ 0
SCL
Text Label 1150 4350 0    60   ~ 0
VCC
Text Label 2300 4750 0    60   ~ 0
GND
Text Label 1150 4750 0    60   ~ 0
GND
Text Label 2500 4650 2    60   ~ 0
Mux2
Wire Wire Line
	9850 1650 10150 1650
Text Label 9850 1650 0    60   ~ 0
DUT_GND
Text Label 1350 4450 2    60   ~ 0
DUT_GND
Text Label 2250 4450 0    60   ~ 0
DUT_GND
Wire Wire Line
	7400 3150 7750 3150
Text Label 7400 3150 0    60   ~ 0
DUT_MUX
Text Label 1350 4650 2    60   ~ 0
DUT_MUX
NoConn ~ 2250 3000
Wire Wire Line
	1350 1600 1050 1600
Text Label 1100 1600 0    60   ~ 0
Prog1
$Sheet
S 8750 4450 1100 1350
U 54C1D9E5
F0 "EforceBuffer" 60
F1 "EforceBuffer.sch" 60
F2 "Eforce_P" I L 8750 5300 60 
F3 "Eforce_N" I L 8750 5400 60 
F4 "5v0SMPS" I L 8750 4550 60 
F5 "GND" I L 8750 4650 60 
F6 "Sense_1" O R 9850 5300 60 
F7 "Sense_2" O R 9850 5400 60 
F8 "Sense_1_in" B R 9850 4750 60 
F9 "Sense_2_in" B R 9850 4850 60 
$EndSheet
Text Label 8700 5300 2    60   ~ 0
EFORCE_P
Text Label 8700 5400 2    60   ~ 0
EFORCE_N
Wire Wire Line
	8750 5400 8400 5400
Wire Wire Line
	8750 5300 8400 5300
Wire Wire Line
	8750 3050 8400 3050
Text Label 8700 3050 2    60   ~ 0
D_PWR
Text Label 2300 4950 0    60   ~ 0
5VPWR
NoConn ~ 2250 4550
NoConn ~ 1350 4550
NoConn ~ 1350 4950
Wire Wire Line
	2250 2200 2550 2200
Text Label 2300 2200 0    60   ~ 0
DAC3
Text Label 4850 7400 2    60   ~ 0
DAC3
Wire Wire Line
	4650 7400 5150 7400
Wire Wire Line
	6250 3100 5900 3100
Text Label 6200 3100 2    60   ~ 0
D_PWR
$Comp
L ProbeGround GND1
U 1 1 54C86480
P 6150 7050
F 0 "GND1" H 6450 7700 60  0001 C CNN
F 1 "ProbeGround" H 6150 7350 60  0000 C CNN
F 2 "TestPoints:PROBEGND" H 6150 7050 60  0001 C CNN
F 3 "" H 6150 7050 60  0000 C CNN
	1    6150 7050
	1    0    0    -1  
$EndComp
Text Label 6050 7150 0    60   ~ 0
GND
Wire Wire Line
	5850 7150 6450 7150
Text Label 4150 1650 1    60   ~ 0
3v3PWR
Wire Wire Line
	4150 1700 4150 1350
Text Label 3900 3650 2    60   ~ 0
GND
Text Label 1350 3200 2    60   ~ 0
SCL_2
Wire Wire Line
	1350 3200 1050 3200
Text Label 2300 3100 0    60   ~ 0
SDA_2
Wire Wire Line
	2250 3100 2550 3100
Text Label 4050 4900 3    60   ~ 0
SDA_2
Text Label 3750 4900 3    60   ~ 0
SCL_2
Wire Wire Line
	4050 4850 4050 5150
Wire Wire Line
	3750 4850 3750 5150
NoConn ~ 3750 2100
$Comp
L TC72Z17F U1
U 1 1 54F25604
P 1300 5900
F 0 "U1" H 1500 6050 60  0000 C CNN
F 1 "TC72Z17F" H 1350 6300 60  0000 C CNN
F 2 "Discretes:SOT353" H 1277 5900 60  0001 C CNN
F 3 "" H 1277 5900 60  0000 C CNN
F 4 "TC7SZ17FU(T5LJFTCT-ND" H 1450 6500 60  0001 C CNN "DigikeyPN"
	1    1300 5900
	1    0    0    -1  
$EndComp
Wire Wire Line
	1050 5900 750  5900
Wire Wire Line
	1450 5650 1800 5650
Text Label 1500 6150 0    60   ~ 0
GND
Wire Wire Line
	1450 6150 1800 6150
Text Label 2250 3500 0    60   ~ 0
GPIO_4
Text Label 2250 3600 0    60   ~ 0
GPIO_6
Text Label 1350 3700 2    60   ~ 0
GPIO_7
Text Label 1350 3600 2    60   ~ 0
GPIO_5
Text Label 2250 3400 0    60   ~ 0
GPIO_2
Text Label 1350 3500 2    60   ~ 0
GPIO_3
Wire Wire Line
	4550 2750 4900 2750
Text Label 4550 2750 0    60   ~ 0
GPIO_10
Text Label 4550 2850 0    60   ~ 0
GPIO_11
Text Label 4550 2950 0    60   ~ 0
GPIO_12
Text Label 4550 3050 0    60   ~ 0
GPIO_13
Wire Wire Line
	4550 3150 4900 3150
Text Label 4550 3150 0    60   ~ 0
GPIO_14
Text Label 4550 3250 0    60   ~ 0
GPIO_15
Text Label 4550 3350 0    60   ~ 0
GPIO_16
Text Label 4550 3450 0    60   ~ 0
GPIO_17
Wire Wire Line
	4550 1950 4900 1950
Text Label 4550 1950 0    60   ~ 0
GPIO_01
Text Label 4550 2050 0    60   ~ 0
GPIO_02
Text Label 4550 2150 0    60   ~ 0
GPIO_03
Text Label 4550 2250 0    60   ~ 0
GPIO_04
Wire Wire Line
	4550 2350 4900 2350
Text Label 4550 2350 0    60   ~ 0
GPIO_05
Text Label 4550 2450 0    60   ~ 0
GPIO_06
Text Label 4550 2550 0    60   ~ 0
GPIO_07
Text Label 4550 1850 0    60   ~ 0
GPIO_00
Wire Wire Line
	4550 1850 4900 1850
Wire Wire Line
	4550 2050 4900 2050
Wire Wire Line
	4550 2150 4900 2150
Wire Wire Line
	4550 2250 4900 2250
Wire Wire Line
	4550 2450 4900 2450
Wire Wire Line
	4550 2550 4900 2550
Wire Wire Line
	4550 2850 4900 2850
Wire Wire Line
	4550 2950 4900 2950
Wire Wire Line
	4550 3050 4900 3050
Wire Wire Line
	4550 3250 4900 3250
Wire Wire Line
	4550 3350 4900 3350
Wire Wire Line
	4550 3450 4900 3450
Text Label 6250 3250 2    60   ~ 0
GPIO_00
Text Label 6250 3350 2    60   ~ 0
GPIO_01
Text Label 6250 3450 2    60   ~ 0
GPIO_02
Text Label 6250 3550 2    60   ~ 0
GPIO_03
Text Label 6250 2100 2    60   ~ 0
GPIO_14
Text Label 6250 2200 2    60   ~ 0
GPIO_15
Wire Wire Line
	3750 3000 3700 3000
Wire Wire Line
	3700 3000 3700 3650
Wire Wire Line
	3700 3100 3750 3100
Wire Wire Line
	3700 3200 3750 3200
Connection ~ 3700 3100
Connection ~ 3700 3200
Wire Wire Line
	4000 3600 4000 3600
$Comp
L PCF8575 U2
U 1 1 54F0A6B6
P 4150 2650
F 0 "U2" H 4050 2900 60  0000 C CNN
F 1 "PCF8575" H 4050 2650 60  0000 C CNN
F 2 "ICs:24-QFN-GP" H 4100 2650 60  0001 C CNN
F 3 "" H 4100 2650 60  0000 C CNN
F 4 "296-20982-1-ND" H 4400 3650 60  0001 C CNN "DigikeyPN"
	1    4150 2650
	1    0    0    -1  
$EndComp
Wire Wire Line
	4000 3600 4000 3650
Wire Wire Line
	3700 3650 4100 3650
Wire Wire Line
	4100 3650 4100 3600
Connection ~ 4000 3650
Wire Wire Line
	4950 5450 4950 5150
Text Label 4950 5300 3    60   ~ 0
SCL
Wire Wire Line
	5250 5450 5250 5150
Text Label 5250 5300 3    60   ~ 0
SDA
$Comp
L 5.1k-AF R3
U 1 1 54F32E6F
P 3750 4650
F 0 "R3" H 3750 4846 60  0000 C CNN
F 1 "5.1k-AF" H 3650 4750 60  0000 C CNN
F 2 "Resistors:R0402" H 3750 4650 60  0001 C CNN
F 3 "" H 3850 4800 60  0000 C CNN
F 4 "P5.1KJCT-ND" H 3768 4918 60  0001 C CNN "DigikeyPN"
F 5 "5%" H 3950 4750 60  0000 C CNN "Precision"
	1    3750 4650
	0    -1   -1   0   
$EndComp
$Comp
L 5.1k-AF R4
U 1 1 54F32EBD
P 4050 4650
F 0 "R4" H 4050 4846 60  0000 C CNN
F 1 "5.1k-AF" H 3950 4750 60  0000 C CNN
F 2 "Resistors:R0402" H 4050 4650 60  0001 C CNN
F 3 "" H 4150 4800 60  0000 C CNN
F 4 "P5.1KJCT-ND" H 4068 4918 60  0001 C CNN "DigikeyPN"
F 5 "5%" H 4250 4750 60  0000 C CNN "Precision"
	1    4050 4650
	0    -1   -1   0   
$EndComp
Text Label 4050 4350 2    60   ~ 0
3v3PWR
Wire Wire Line
	3750 4450 3750 4350
Wire Wire Line
	3750 4350 4050 4350
Wire Wire Line
	4050 4350 4050 4450
Text Label 8700 4550 2    60   ~ 0
5VPWR
Text Label 8700 4650 2    60   ~ 0
GND
Wire Wire Line
	8750 4550 8400 4550
Wire Wire Line
	8750 4650 8400 4650
$Comp
L 5121K TP1
U 1 1 54F56A85
P 2100 5900
F 0 "TP1" H 2100 6250 60  0000 C CNN
F 1 "ScanCLK" H 2100 6150 60  0000 C CNN
F 2 "TestPoints:TP_1p6mm" H 2100 5900 60  0001 C CNN
F 3 "" H 2250 6150 60  0000 C CNN
F 4 "5121K-ND" H 2350 6250 60  0001 C CNN "DigikeyPN"
	1    2100 5900
	1    0    0    -1  
$EndComp
$Comp
L 5008K TP2
U 1 1 54F56ACD
P 2500 6000
F 0 "TP2" H 2500 6350 60  0000 C CNN
F 1 "ScanOUT" H 2500 6250 60  0000 C CNN
F 2 "TestPoints:TP_1p6mm" H 2500 6000 60  0001 C CNN
F 3 "" H 2500 6000 60  0000 C CNN
F 4 "5008K-ND" H 2750 6350 60  0001 C CNN "DigikeyPN"
	1    2500 6000
	1    0    0    -1  
$EndComp
$Comp
L 5121K TP3
U 1 1 54F56B13
P 2900 6100
F 0 "TP3" H 2900 6450 60  0000 C CNN
F 1 "ScanIN" H 2900 6350 60  0000 C CNN
F 2 "TestPoints:TP_1p6mm" H 2900 6100 60  0001 C CNN
F 3 "" H 3050 6350 60  0000 C CNN
F 4 "5121K-ND" H 3150 6450 60  0001 C CNN "DigikeyPN"
	1    2900 6100
	1    0    0    -1  
$EndComp
$Comp
L 5121K TP4
U 1 1 54F56B59
P 3250 6200
F 0 "TP4" H 3250 6550 60  0000 C CNN
F 1 "ScanEN" H 3250 6450 60  0000 C CNN
F 2 "TestPoints:TP_1p6mm" H 3250 6200 60  0001 C CNN
F 3 "" H 3400 6450 60  0000 C CNN
F 4 "5121K-ND" H 3500 6550 60  0001 C CNN "DigikeyPN"
	1    3250 6200
	1    0    0    -1  
$EndComp
$Comp
L Bare_TP TP5
U 1 1 54F5A7D9
P 3300 6750
F 0 "TP5" H 3300 7100 60  0000 C CNN
F 1 "GPIO_PWR" H 3300 7000 60  0000 C CNN
F 2 "TestPoints:TP_bare_1p5mm" H 3300 6750 60  0001 C CNN
F 3 "" H 3300 6750 60  0000 C CNN
	1    3300 6750
	1    0    0    -1  
$EndComp
$Comp
L Bare_TP TP6
U 1 1 54F5A870
P 3500 7050
F 0 "TP6" H 3500 7400 60  0000 C CNN
F 1 "Digital_PWR" H 3500 7300 60  0000 C CNN
F 2 "TestPoints:TP_bare_1p5mm" H 3500 7050 60  0001 C CNN
F 3 "" H 3500 7050 60  0000 C CNN
	1    3500 7050
	1    0    0    -1  
$EndComp
$Comp
L 5121K TP9
U 1 1 54F5BACC
P 5150 6850
F 0 "TP9" H 5150 7200 60  0000 C CNN
F 1 "Mux2" H 5150 7100 60  0000 C CNN
F 2 "TestPoints:TP_1p6mm" H 5150 6850 60  0001 C CNN
F 3 "" H 5300 7100 60  0000 C CNN
F 4 "5121K-ND" H 5400 7200 60  0001 C CNN "DigikeyPN"
	1    5150 6850
	1    0    0    -1  
$EndComp
$Comp
L 5009K TP10
U 1 1 54F5BC31
P 5150 7400
F 0 "TP10" H 5150 7750 60  0000 C CNN
F 1 "DAC3" H 5150 7650 60  0000 C CNN
F 2 "TestPoints:TP_1p6mm" H 5150 7400 60  0001 C CNN
F 3 "" H 5300 7650 60  0000 C CNN
F 4 "5009K-ND" H 5400 7750 60  0001 C CNN "DigikeyPN"
	1    5150 7400
	1    0    0    -1  
$EndComp
Wire Wire Line
	8750 3850 8400 3850
Wire Wire Line
	8750 3950 8400 3950
Wire Wire Line
	10200 5300 9850 5300
Wire Wire Line
	10200 5400 9850 5400
Text Label 9850 5300 0    60   ~ 0
Sense_1
Text Label 9850 5400 0    60   ~ 0
Sense_2
Text Label 8750 3850 2    60   ~ 0
Sense_1
Text Label 8750 3950 2    60   ~ 0
Sense_2
Text Label 3450 2000 0    60   ~ 0
SCL_2
Text Label 3450 1900 0    60   ~ 0
SDA_2
Wire Wire Line
	3450 1900 3750 1900
Wire Wire Line
	3450 2000 3750 2000
Text Label 6200 5100 2    60   ~ 0
5VPWR
Text Label 5950 5200 0    60   ~ 0
GND
Wire Wire Line
	6250 5100 5900 5100
Wire Wire Line
	6250 5200 5900 5200
Wire Wire Line
	6250 3850 5900 3850
Wire Wire Line
	6250 3950 5900 3950
Wire Wire Line
	6250 3750 5900 3750
Wire Wire Line
	6250 3650 5900 3650
Text Label 6250 3650 2    60   ~ 0
GPIO_04
Text Label 6250 3750 2    60   ~ 0
GPIO_05
Text Label 6250 3850 2    60   ~ 0
GPIO_06
Text Label 6250 3950 2    60   ~ 0
GPIO_07
Wire Wire Line
	6250 4250 5900 4250
Wire Wire Line
	6250 4350 5900 4350
Wire Wire Line
	6250 4150 5900 4150
Wire Wire Line
	6250 4050 5900 4050
Text Label 6250 4050 2    60   ~ 0
GPIO_10
Text Label 6250 4150 2    60   ~ 0
GPIO_11
Text Label 6250 4250 2    60   ~ 0
GPIO_12
Text Label 6250 4350 2    60   ~ 0
GPIO_13
Wire Wire Line
	6250 2000 5900 2000
Text Label 6250 2300 2    60   ~ 0
GPIO_16
Text Label 8750 2350 2    60   ~ 0
GPIO_17
$Sheet
S 6250 2800 1150 1650
U 549996BC
F0 "MuxOutput" 60
F1 "MuxOut.sch" 60
F2 "Mux_Measure" O R 7400 3250 60 
F3 "5v0SMPS" I L 6250 2900 60 
F4 "GND" I L 6250 3000 60 
F5 "GPIO0" O R 7400 3550 60 
F6 "GPIO1" O R 7400 3650 60 
F7 "GPIO5" I L 6250 3550 60 
F8 "GPIO4" I L 6250 3450 60 
F9 "DUT_MUX" I R 7400 3150 60 
F10 "3v3PWR" I L 6250 3100 60 
F11 "GPIO6" I L 6250 3650 60 
F12 "GPIO7" I L 6250 3750 60 
F13 "GPIO8" I L 6250 3850 60 
F14 "GPIO9" I L 6250 3950 60 
F15 "GPIO13" I L 6250 4350 60 
F16 "GPIO12" I L 6250 4250 60 
F17 "GPIO3" I L 6250 3350 60 
F18 "GPIO2" I L 6250 3250 60 
$EndSheet
Wire Wire Line
	8750 2350 8400 2350
NoConn ~ 1350 1600
Text Label 5100 6050 0    60   ~ 0
GPIO_2
Text Label 5100 6250 0    60   ~ 0
GPIO_3
$Comp
L 5008K TP8
U 1 1 5506D2AD
P 4450 6000
F 0 "TP8" H 4450 6350 60  0000 C CNN
F 1 "GPIO2" H 4450 6250 60  0000 C CNN
F 2 "TestPoints:TP_1p6mm" H 4450 6000 60  0001 C CNN
F 3 "" H 4450 6000 60  0000 C CNN
F 4 "5008K-ND" H 4700 6350 60  0001 C CNN "DigikeyPN"
	1    4450 6000
	1    0    0    -1  
$EndComp
Wire Wire Line
	4450 6000 4450 6050
$Comp
L 5008K TP7
U 1 1 5506DCC9
P 4000 6000
F 0 "TP7" H 4000 6350 60  0000 C CNN
F 1 "GPIO3" H 4000 6250 60  0000 C CNN
F 2 "TestPoints:TP_1p6mm" H 4000 6000 60  0001 C CNN
F 3 "" H 4000 6000 60  0000 C CNN
F 4 "5008K-ND" H 4250 6350 60  0001 C CNN "DigikeyPN"
	1    4000 6000
	1    0    0    -1  
$EndComp
Wire Wire Line
	4000 6000 4000 6250
$Comp
L 5.1k-AF R8
U 1 1 5506E3AE
P 4800 6050
F 0 "R8" H 4800 6246 60  0000 C CNN
F 1 "5.1k-AF" H 4700 6150 60  0000 C CNN
F 2 "Resistors:R0402" H 4800 6050 60  0001 C CNN
F 3 "" H 4900 6200 60  0000 C CNN
F 4 "P5.1KJCT-ND" H 4818 6318 60  0001 C CNN "DigikeyPN"
F 5 "5%" H 5000 6150 60  0000 C CNN "Precision"
	1    4800 6050
	1    0    0    -1  
$EndComp
$Comp
L 5.1k-AF R6
U 1 1 5506E408
P 4550 6250
F 0 "R6" H 4550 6446 60  0000 C CNN
F 1 "5.1k-AF" H 4450 6350 60  0000 C CNN
F 2 "Resistors:R0402" H 4550 6250 60  0001 C CNN
F 3 "" H 4650 6400 60  0000 C CNN
F 4 "P5.1KJCT-ND" H 4568 6518 60  0001 C CNN "DigikeyPN"
F 5 "5%" H 4750 6350 60  0000 C CNN "Precision"
	1    4550 6250
	-1   0    0    1   
$EndComp
Wire Wire Line
	4000 6250 4350 6250
Wire Wire Line
	4750 6250 5400 6250
Wire Wire Line
	4450 6050 4600 6050
Wire Wire Line
	5000 6050 5400 6050
$Sheet
S 6250 4950 1150 850 
U 54F630F4
F0 "ProtoBoard" 60
F1 "ProtoBoard.sch" 60
F2 "5v0SMPS" I L 6250 5100 60 
F3 "GND" I L 6250 5200 60 
F4 "GPIO0" B L 6250 5400 60 
F5 "GPIO1" B L 6250 5500 60 
F6 "GPIO2" B L 6250 5600 60 
F7 "GPIO3" B L 6250 5700 60 
F8 "VCC" I R 7400 5050 60 
F9 "Mux" I R 7400 5150 60 
F10 "DUT_GND" I R 7400 5250 60 
F11 "3v3PWR" I L 6250 5000 60 
F12 "Sense_1" I R 7400 5550 60 
F13 "Sense_2" I R 7400 5650 60 
$EndSheet
Text Label 6250 5400 2    60   ~ 0
GPIO_4
Text Label 6250 5600 2    60   ~ 0
GPIO_6
Text Label 6250 5500 2    60   ~ 0
GPIO_5
Text Label 6250 5700 2    60   ~ 0
GPIO_7
Wire Wire Line
	6250 5400 5900 5400
Wire Wire Line
	6250 5500 5900 5500
Wire Wire Line
	6250 5600 5900 5600
Wire Wire Line
	6250 5700 5900 5700
Text Label 1450 5650 0    60   ~ 0
3v3PWR
Wire Wire Line
	7750 5050 7400 5050
Wire Wire Line
	7400 5150 7750 5150
Wire Wire Line
	7400 5250 7750 5250
Wire Wire Line
	6250 5000 5900 5000
Text Label 6250 5000 2    60   ~ 0
3v3PWR
Text Label 7400 5150 0    60   ~ 0
DUT_MUX
Text Label 7400 5250 0    60   ~ 0
DUT_GND
Text Label 7650 5050 2    60   ~ 0
VCC
Wire Wire Line
	9850 4750 10200 4750
Wire Wire Line
	9850 4850 10200 4850
Text Label 9850 4750 0    60   ~ 0
Sense_1_B
Text Label 9850 4850 0    60   ~ 0
Sense_2_B
Text Label 7400 5550 0    60   ~ 0
Sense_1_B
Text Label 7400 5650 0    60   ~ 0
Sense_2_B
Wire Wire Line
	7400 5550 7750 5550
Wire Wire Line
	7400 5650 7750 5650
$Comp
L 1n-AC C51
U 1 1 54F8CB57
P 5300 1100
F 0 "C51" H 5460 1210 60  0000 C CNN
F 1 "1n-AC" H 5556 1120 60  0000 C CNN
F 2 "Capacitors:C0402" H 5300 1102 60  0001 C CNN
F 3 "" H 5300 1102 60  0000 C CNN
F 4 "490-1303-1-ND" H 5616 1304 60  0001 C CNN "DigikeyPN"
F 5 "50V" H 5492 1036 60  0000 C CNN "MaxVoltage"
	1    5300 1100
	1    0    0    -1  
$EndComp
$Comp
L 10u-BD C50
U 1 1 54F8CFDA
P 4700 1100
F 0 "C50" H 4550 1200 60  0000 C CNN
F 1 "10u-BD" H 4450 1100 60  0000 C CNN
F 2 "Capacitors:C0603" H 4700 1102 60  0001 C CNN
F 3 "" H 4700 1102 60  0000 C CNN
F 4 "490-7200-1-ND" H 5016 1304 60  0001 C CNN "DigikeyPN"
F 5 "10V" H 4550 1000 60  0000 C CNN "MaxVoltage"
	1    4700 1100
	-1   0    0    -1  
$EndComp
Wire Wire Line
	4700 1250 4700 1350
Wire Wire Line
	4700 1350 5300 1350
Wire Wire Line
	5300 1350 5300 1250
Wire Wire Line
	5300 950  5300 850 
Wire Wire Line
	5300 850  4700 850 
Wire Wire Line
	4700 850  4700 950 
Text Label 5150 850  2    60   ~ 0
3v3PWR
Text Label 5100 1350 2    60   ~ 0
GND
$Comp
L 1n-AC C53
U 1 1 54F890D3
P 3600 1100
F 0 "C53" H 3760 1210 60  0000 C CNN
F 1 "1n-AC" H 3856 1120 60  0000 C CNN
F 2 "Capacitors:C0402" H 3600 1102 60  0001 C CNN
F 3 "" H 3600 1102 60  0000 C CNN
F 4 "490-1303-1-ND" H 3916 1304 60  0001 C CNN "DigikeyPN"
F 5 "50V" H 3792 1036 60  0000 C CNN "MaxVoltage"
	1    3600 1100
	1    0    0    -1  
$EndComp
$Comp
L 10u-BD C52
U 1 1 54F890DB
P 3000 1100
F 0 "C52" H 2850 1200 60  0000 C CNN
F 1 "10u-BD" H 2750 1100 60  0000 C CNN
F 2 "Capacitors:C0603" H 3000 1102 60  0001 C CNN
F 3 "" H 3000 1102 60  0000 C CNN
F 4 "490-7200-1-ND" H 3316 1304 60  0001 C CNN "DigikeyPN"
F 5 "10V" H 2850 1000 60  0000 C CNN "MaxVoltage"
	1    3000 1100
	-1   0    0    -1  
$EndComp
Wire Wire Line
	3000 1250 3000 1350
Wire Wire Line
	3000 1350 3600 1350
Wire Wire Line
	3600 1350 3600 1250
Wire Wire Line
	3600 950  3600 850 
Wire Wire Line
	3600 850  3000 850 
Wire Wire Line
	3000 850  3000 950 
Text Label 3450 850  2    60   ~ 0
3v3PWR
Text Label 3400 1350 2    60   ~ 0
GND
$Sheet
S 4450 3900 1050 900 
U 577AED3A
F0 "Sheet577AED39" 60
F1 "I2CSelect.sch" 60
$EndSheet
$EndSCHEMATC
