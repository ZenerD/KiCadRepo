EESchema Schematic File Version 2
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:Capacitors
LIBS:Connectors
LIBS:Discretes
LIBS:ICs
LIBS:Inductors
LIBS:Allegro
LIBS:Resistors
LIBS:TestPoints
LIBS:sixteen_one_socket_clamshell-cache
EELAYER 26 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L 63067 U1
U 1 1 5DA078B7
P 5745 3505
F 0 "U1" H 5913 3542 60  0000 L CNN
F 1 "63067" H 5913 3452 39  0000 L CNN
F 2 "Connectors:63067" H 5745 3505 60  0001 C CNN
F 3 "" H 5745 3505 60  0001 C CNN
	1    5745 3505
	1    0    0    -1  
$EndComp
$Comp
L 63067 U2
U 1 1 5DA07CB7
P 5745 3650
F 0 "U2" H 5913 3687 60  0000 L CNN
F 1 "90 deg header" H 5913 3597 39  0000 L CNN
F 2 "Connectors:90_deg_header" H 5745 3650 60  0001 C CNN
F 3 "" H 5745 3650 60  0001 C CNN
	1    5745 3650
	1    0    0    -1  
$EndComp
$Comp
L 5006 TP2
U 1 1 5DA07D88
P 5715 3650
F 0 "TP2" H 5627 3637 60  0000 R CNN
F 1 "5006" H 5627 3743 60  0000 R CNN
F 2 "TestPoints:TP_1p6mm" H 5627 3849 60  0000 R CNN
F 3 "" H 5715 3650 60  0000 C CNN
F 4 "36-5006-ND" H 5965 4000 60  0001 C CNN "DigikeyPN"
	1    5715 3650
	-1   0    0    1   
$EndComp
$Comp
L 5006 TP1
U 1 1 5DA0809A
P 5655 3505
F 0 "TP1" H 5733 3705 60  0000 L CNN
F 1 "5006" H 5733 3599 60  0000 L CNN
F 2 "TestPoints:TP_1p6mm" H 5733 3493 60  0000 L CNN
F 3 "" H 5655 3505 60  0000 C CNN
F 4 "36-5006-ND" H 5905 3855 60  0001 C CNN "DigikeyPN"
	1    5655 3505
	1    0    0    -1  
$EndComp
$Comp
L 5006 TP3
U 1 1 5DA080BE
P 5775 3650
F 0 "TP3" H 5687 3637 60  0000 R CNN
F 1 "5006" H 5687 3743 60  0000 R CNN
F 2 "TestPoints:TP_1p6mm" H 5687 3849 60  0000 R CNN
F 3 "" H 5775 3650 60  0000 C CNN
F 4 "36-5006-ND" H 6025 4000 60  0001 C CNN "DigikeyPN"
	1    5775 3650
	-1   0    0    1   
$EndComp
$Comp
L ProtoTP TP6
U 1 1 5DA08BF4
P 6550 3790
F 0 "TP6" H 6628 3990 60  0000 L CNN
F 1 "ProtoTP" H 6628 3884 60  0000 L CNN
F 2 "connectors:1pin" H 6628 3778 60  0000 L CNN
F 3 "" H 6550 3790 60  0000 C CNN
	1    6550 3790
	1    0    0    -1  
$EndComp
$Comp
L ProtoTP TP7
U 1 1 5DA08C38
P 6665 3790
F 0 "TP7" H 6743 3990 60  0000 L CNN
F 1 "ProtoTP" H 6743 3884 60  0000 L CNN
F 2 "connectors:1pin" H 6743 3778 60  0000 L CNN
F 3 "" H 6665 3790 60  0000 C CNN
	1    6665 3790
	1    0    0    -1  
$EndComp
$Comp
L ProtoTP TP5
U 1 1 5DA08C6E
P 6435 3790
F 0 "TP5" H 6513 3990 60  0000 L CNN
F 1 "ProtoTP" H 6513 3884 60  0000 L CNN
F 2 "connectors:1pin" H 6513 3778 60  0000 L CNN
F 3 "" H 6435 3790 60  0000 C CNN
	1    6435 3790
	1    0    0    -1  
$EndComp
$Comp
L ProtoTP TP8
U 1 1 5DA08CBC
P 6780 3790
F 0 "TP8" H 6858 3990 60  0000 L CNN
F 1 "ProtoTP" H 6858 3884 60  0000 L CNN
F 2 "connectors:1pin" H 6858 3778 60  0000 L CNN
F 3 "" H 6780 3790 60  0000 C CNN
	1    6780 3790
	1    0    0    -1  
$EndComp
$Comp
L 5006 TP4
U 1 1 5DA080E4
P 5835 3505
F 0 "TP4" H 5913 3705 60  0000 L CNN
F 1 "5006" H 5913 3599 60  0000 L CNN
F 2 "TestPoints:TP_1p6mm" H 5913 3493 60  0000 L CNN
F 3 "" H 5835 3505 60  0000 C CNN
F 4 "36-5006-ND" H 6085 3855 60  0001 C CNN "DigikeyPN"
	1    5835 3505
	1    0    0    -1  
$EndComp
Wire Wire Line
	5655 3650 5655 3505
Wire Wire Line
	5715 3505 5715 3650
Wire Wire Line
	5775 3505 5775 3650
Wire Wire Line
	5835 3650 5835 3505
Wire Wire Line
	6435 3790 6780 3790
Connection ~ 6550 3790
Connection ~ 6665 3790
$EndSCHEMATC
