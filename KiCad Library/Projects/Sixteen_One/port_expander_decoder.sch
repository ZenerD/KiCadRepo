EESchema Schematic File Version 2
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:Capacitors
LIBS:Connectors
LIBS:Discretes
LIBS:ICs
LIBS:Allegro
LIBS:Resistors
LIBS:TestPoints
LIBS:sixteen_one-cache
EELAYER 26 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 3 4
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Text Notes 8160 7640 0    60   ~ 0
8/26/16
Text Notes 10590 7640 0    60   ~ 0
1.5\n
Text Notes 7430 7510 0    79   ~ 0
16 Site GTS Board Controller\n
Text HLabel 6060 2020 0    59   Input ~ 0
5V
Text HLabel 6050 2420 0    59   Input ~ 0
GND
Text HLabel 3590 3420 0    60   Input ~ 0
SDA
Text HLabel 3820 3520 0    60   Input ~ 0
SCL
Text Label 4500 2730 0    60   ~ 0
5V
Text Label 4700 5150 0    60   ~ 0
GND
NoConn ~ 4680 3620
$Comp
L 1k-BE R21
U 1 1 5907F653
P 4370 3520
F 0 "R21" H 4370 3716 60  0000 C CNN
F 1 "1k-BD" H 4270 3620 60  0000 C CNN
F 2 "Resistors:R0603" H 4370 3520 60  0001 C CNN
F 3 "" H 4470 3670 60  0000 C CNN
F 4 "P1.0KDBCT-ND" H 4388 3788 60  0001 C CNN "DigikeyPN"
F 5 "0.1%" H 4570 3620 60  0000 C CNN "Precision"
	1    4370 3520
	1    0    0    1   
$EndComp
$Comp
L 10-BD R20
U 1 1 5907FCF0
P 4340 3420
F 0 "R20" H 4340 3616 60  0000 C CNN
F 1 "10-BD" H 4240 3520 60  0000 C CNN
F 2 "Resistors:R0603" H 4340 3420 60  0001 C CNN
F 3 "" H 4440 3570 60  0000 C CNN
F 4 "TNP10.0AACT-ND" H 4358 3688 60  0001 C CNN "DigikeyPN"
F 5 "0.1%" H 4540 3520 60  0000 C CNN "Precision"
	1    4340 3420
	1    0    0    -1  
$EndComp
$Comp
L 10k-CD R19
U 1 1 59081782
P 4070 2980
F 0 "R19" H 4070 3176 60  0000 C CNN
F 1 "10k-CD" H 3970 3080 60  0000 C CNN
F 2 "Resistors:R0805" H 4070 2980 60  0001 C CNN
F 3 "" H 4170 3130 60  0000 C CNN
F 4 "A103142CT-ND" H 4088 3248 60  0001 C CNN "DigikeyPN"
F 5 "0.1%" H 4320 3080 60  0000 C CNN "Precision"
	1    4070 2980
	0    1    1    0   
$EndComp
$Comp
L 10k-CD R18
U 1 1 59081862
P 3980 2980
F 0 "R18" H 3980 3176 60  0000 C CNN
F 1 "10k-CD" H 3880 3080 60  0000 C CNN
F 2 "Resistors:R0805" H 3980 2980 60  0001 C CNN
F 3 "" H 4080 3130 60  0000 C CNN
F 4 "A103142CT-ND" H 3998 3248 60  0001 C CNN "DigikeyPN"
F 5 "0.1%" H 4230 3080 60  0000 C CNN "Precision"
	1    3980 2980
	0    -1   1    0   
$EndComp
$Comp
L 100n-CC C1
U 1 1 59082FC2
P 6790 2230
F 0 "C1" H 6950 2340 60  0000 C CNN
F 1 "100n-CC" H 7046 2250 60  0000 C CNN
F 2 "Capacitors:C0805" H 6790 2520 60  0001 C CNN
F 3 "" H 6790 2232 60  0000 C CNN
F 4 "490-4789-1-ND" H 7106 2434 60  0001 C CNN "DigikeyPN"
F 5 "100V" H 6982 2166 60  0000 C CNN "MaxVoltage"
	1    6790 2230
	1    0    0    -1  
$EndComp
$Comp
L 100n-CC C2
U 1 1 590830AA
P 8270 2225
F 0 "C2" H 8430 2335 60  0000 C CNN
F 1 "100n-CC" H 8526 2245 60  0000 C CNN
F 2 "Capacitors:C0805" H 8270 2515 60  0001 C CNN
F 3 "" H 8270 2227 60  0000 C CNN
F 4 "490-4789-1-ND" H 8586 2429 60  0001 C CNN "DigikeyPN"
F 5 "100V" H 8462 2161 60  0000 C CNN "MaxVoltage"
	1    8270 2225
	1    0    0    -1  
$EndComp
Text Label 6380 2420 0    60   ~ 0
GND
Text Label 6510 2020 0    60   ~ 0
5V
Text Label 8020 5100 0    60   ~ 0
GND
NoConn ~ 8000 3570
Text HLabel 5590 4270 2    39   Output ~ 0
D1_en
Text HLabel 5590 4070 2    39   Output ~ 0
D2_en
Text HLabel 5590 4370 2    39   Output ~ 0
D3_en
Text HLabel 5590 3970 2    39   Output ~ 0
D4_en
Text HLabel 5590 4470 2    39   Output ~ 0
D5_en
Text HLabel 5590 3870 2    39   Output ~ 0
D6_en
Text HLabel 5590 4970 2    39   Output ~ 0
D7_en
Text HLabel 5590 3770 2    39   Output ~ 0
D8_en
Text HLabel 5590 4870 2    39   Output ~ 0
D9_en
Text HLabel 5590 3670 2    39   Output ~ 0
D10_en
Text HLabel 5590 4770 2    39   Output ~ 0
D11_en
Text HLabel 5590 3570 2    39   Output ~ 0
D12_en
Text HLabel 5590 4670 2    39   Output ~ 0
D13_en
Text HLabel 5590 3470 2    39   Output ~ 0
D14_en
Text HLabel 5590 4570 2    39   Output ~ 0
D15_en
Text HLabel 5590 3370 2    39   Output ~ 0
D16_en
Text HLabel 8930 4520 2    30   Output ~ 0
VCC_pin1_en
Text HLabel 8930 4620 2    30   Output ~ 0
VCC_pin2_en
Text HLabel 8930 3920 2    30   Output ~ 0
VCC_pin3_en
Text HLabel 8930 4920 2    30   Output ~ 0
VCC_pin4_en
Text HLabel 8930 4720 2    30   Output ~ 0
Mux_pin1_en
Text HLabel 8930 4820 2    30   Output ~ 0
Mux_pin2_en
Text HLabel 8930 3720 2    30   Output ~ 0
Mux_pin3_en
Text HLabel 8930 3820 2    30   Output ~ 0
Mux_pin4_en
Text HLabel 8930 4420 2    30   Output ~ 0
Mux2_pin1_en
Text HLabel 8930 4320 2    30   Output ~ 0
Mux2_pin2_en
Text HLabel 8930 3620 2    30   Output ~ 0
Mux2_pin3_en
Text HLabel 8930 3520 2    30   Output ~ 0
Mux2_pin4_en
Text HLabel 8930 4020 2    30   Output ~ 0
GND_pin1_en
Text HLabel 8930 4220 2    30   Output ~ 0
GND_pin2_en
Text HLabel 8930 3420 2    30   Output ~ 0
GND_pin3_en
Text HLabel 8930 3320 2    30   Output ~ 0
GND_pin4_en
Text Label 7660 4015 0    60   ~ 0
5V
Text Label 8400 2960 0    60   ~ 0
5V
Text Label 7690 3470 0    60   ~ 0
SCL_in
Text Label 7690 3370 0    60   ~ 0
SDA_in
Text Label 4640 3420 1    60   ~ 0
SDA_in
Text Label 4630 3520 3    60   ~ 0
SCL_in
$Comp
L PCF8575-ssop U11
U 1 1 590DD7CC
P 5080 4170
F 0 "U11" H 4980 4420 60  0000 C CNN
F 1 "PCF8575-ssop" H 4980 4170 60  0000 C CNN
F 2 "ICs:24-SSOP" H 5030 4170 60  0001 C CNN
F 3 "" H 5030 4170 60  0000 C CNN
F 4 "568-1079-1-ND" H 5330 5170 60  0001 C CNN "DigikeyPN"
	1    5080 4170
	1    0    0    -1  
$EndComp
$Comp
L PCF8575-ssop U12
U 1 1 590DDC25
P 8400 4120
F 0 "U12" H 8300 4370 60  0000 C CNN
F 1 "PCF8575-ssop" H 8300 4120 60  0000 C CNN
F 2 "ICs:24-SSOP" H 8350 4120 60  0001 C CNN
F 3 "" H 8350 4120 60  0000 C CNN
F 4 "568-1079-1-ND" H 8650 5120 60  0001 C CNN "DigikeyPN"
	1    8400 4120
	1    0    0    -1  
$EndComp
$Comp
L 1u-BD C4
U 1 1 59935929
P 7995 2225
F 0 "C4" H 7835 2340 60  0000 C CNN
F 1 "1u-BD" H 7735 2240 60  0000 C CNN
F 2 "Capacitors:C0603" H 7995 2227 60  0000 C CNN
F 3 "" H 7995 2227 60  0000 C CNN
F 4 "587-1248-1-ND" H 8311 2429 60  0001 C CNN "DigikeyPN"
F 5 "25V" H 7785 2140 60  0000 C CNN "MaxVoltage"
	1    7995 2225
	1    0    0    -1  
$EndComp
$Comp
L 1u-BD C3
U 1 1 59935AD2
P 6605 2230
F 0 "C3" H 6445 2350 60  0000 C CNN
F 1 "1u-BD" H 6350 2255 60  0000 C CNN
F 2 "Capacitors:C0603" H 6605 2232 60  0000 C CNN
F 3 "" H 6605 2232 60  0000 C CNN
F 4 "587-1248-1-ND" H 6921 2434 60  0001 C CNN "DigikeyPN"
F 5 "25V" H 6340 2165 60  0000 C CNN "MaxVoltage"
	1    6605 2230
	1    0    0    -1  
$EndComp
$Comp
L ProtoTP TP13
U 1 1 59BD3C4E
P 7670 4670
F 0 "TP13" H 7748 4870 60  0000 L CNN
F 1 "ProtoTP" H 7748 4764 60  0000 L CNN
F 2 "TestPoints:TP_1p0mm" H 6845 4970 60  0001 L CNN
F 3 "" H 7670 4670 60  0000 C CNN
	1    7670 4670
	0    -1   -1   0   
$EndComp
$Comp
L ProtoTP TP16
U 1 1 59BD3E9A
P 7530 4570
F 0 "TP16" H 7608 4770 60  0000 L CNN
F 1 "ProtoTP" H 7608 4664 60  0000 L CNN
F 2 "TestPoints:TP_1p0mm" H 6705 4870 60  0001 L CNN
F 3 "" H 7530 4570 60  0000 C CNN
	1    7530 4570
	0    -1   -1   0   
$EndComp
$Comp
L ProtoTP TP17
U 1 1 59C0219E
P 7625 5100
F 0 "TP17" H 7675 5305 60  0000 L CNN
F 1 "ProtoTP" H 7625 5385 60  0000 L CNN
F 2 "TestPoints:TP_1p0mm" H 6800 5400 60  0001 L CNN
F 3 "" H 7625 5100 60  0000 C CNN
	1    7625 5100
	1    0    0    -1  
$EndComp
$Comp
L ProtoTP TP14
U 1 1 59C021E8
P 7420 5100
F 0 "TP14" H 7440 5300 60  0000 L CNN
F 1 "ProtoTP" H 7420 5380 60  0000 L CNN
F 2 "TestPoints:TP_1p0mm" H 6595 5400 60  0001 L CNN
F 3 "" H 7420 5100 60  0000 C CNN
	1    7420 5100
	1    0    0    -1  
$EndComp
$Comp
L ProtoTP TP15
U 1 1 59C03710
P 7495 4015
F 0 "TP15" H 7545 4220 60  0000 L CNN
F 1 "ProtoTP" H 7495 4300 60  0000 L CNN
F 2 "TestPoints:TP_1p0mm" H 6670 4315 60  0001 L CNN
F 3 "" H 7495 4015 60  0000 C CNN
	1    7495 4015
	1    0    0    -1  
$EndComp
$Comp
L ProtoTP TP12
U 1 1 59C03716
P 7290 4015
F 0 "TP12" H 7310 4215 60  0000 L CNN
F 1 "ProtoTP" H 7290 4295 60  0000 L CNN
F 2 "TestPoints:TP_1p0mm" H 6465 4315 60  0001 L CNN
F 3 "" H 7290 4015 60  0000 C CNN
	1    7290 4015
	1    0    0    -1  
$EndComp
Text Label 4395 4285 0    60   ~ 0
5V
$Comp
L ProtoTP TP7
U 1 1 59C0398E
P 4230 4285
F 0 "TP7" H 4280 4490 60  0000 L CNN
F 1 "ProtoTP" H 4230 4570 60  0000 L CNN
F 2 "TestPoints:TP_1p0mm" H 3405 4585 60  0001 L CNN
F 3 "" H 4230 4285 60  0000 C CNN
	1    4230 4285
	1    0    0    -1  
$EndComp
$Comp
L ProtoTP TP3
U 1 1 59C03994
P 4025 4285
F 0 "TP3" H 4045 4485 60  0000 L CNN
F 1 "ProtoTP" H 4025 4565 60  0000 L CNN
F 2 "TestPoints:TP_1p0mm" H 3200 4585 60  0001 L CNN
F 3 "" H 4025 4285 60  0000 C CNN
	1    4025 4285
	1    0    0    -1  
$EndComp
$Comp
L ProtoTP TP8
U 1 1 59C03B79
P 4305 5150
F 0 "TP8" H 4355 5355 60  0000 L CNN
F 1 "ProtoTP" H 4305 5435 60  0000 L CNN
F 2 "TestPoints:TP_1p0mm" H 3480 5450 60  0001 L CNN
F 3 "" H 4305 5150 60  0000 C CNN
	1    4305 5150
	1    0    0    -1  
$EndComp
$Comp
L ProtoTP TP5
U 1 1 59C03B7F
P 4100 5150
F 0 "TP5" H 4120 5350 60  0000 L CNN
F 1 "ProtoTP" H 4100 5430 60  0000 L CNN
F 2 "TestPoints:TP_1p0mm" H 3275 5450 60  0001 L CNN
F 3 "" H 4100 5150 60  0000 C CNN
	1    4100 5150
	1    0    0    -1  
$EndComp
$Comp
L ProtoTP TP4
U 1 1 59C03F2E
P 4350 4720
F 0 "TP4" H 4428 4920 60  0000 L CNN
F 1 "ProtoTP" H 4428 4814 60  0000 L CNN
F 2 "TestPoints:TP_1p0mm" H 3525 5020 60  0001 L CNN
F 3 "" H 4350 4720 60  0000 C CNN
	1    4350 4720
	0    -1   -1   0   
$EndComp
$Comp
L ProtoTP TP6
U 1 1 59C03F34
P 4210 4620
F 0 "TP6" H 4288 4820 60  0000 L CNN
F 1 "ProtoTP" H 4288 4714 60  0000 L CNN
F 2 "TestPoints:TP_1p0mm" H 3385 4920 60  0001 L CNN
F 3 "" H 4210 4620 60  0000 C CNN
	1    4210 4620
	0    -1   -1   0   
$EndComp
Connection ~ 6790 2020
Connection ~ 6790 2420
Wire Wire Line
	5050 5150 5050 5120
Wire Wire Line
	4680 3520 4570 3520
Wire Wire Line
	4070 3180 4070 3520
Connection ~ 4070 3520
Wire Wire Line
	3980 3180 3980 3420
Connection ~ 3980 3420
Wire Wire Line
	3590 3420 4140 3420
Wire Wire Line
	3980 2730 5080 2730
Connection ~ 4070 2730
Wire Wire Line
	5080 2730 5080 3220
Wire Wire Line
	3820 3520 4170 3520
Wire Wire Line
	4540 3420 4680 3420
Wire Wire Line
	3980 2780 3980 2730
Wire Wire Line
	4070 2730 4070 2780
Wire Wire Line
	6790 2020 6790 2080
Wire Wire Line
	6790 2420 6790 2380
Wire Wire Line
	8270 2420 8270 2375
Wire Wire Line
	8270 2020 8270 2075
Wire Wire Line
	8370 5100 8370 5070
Wire Wire Line
	8400 2960 8400 3170
Wire Wire Line
	7670 4670 8000 4670
Wire Wire Line
	5480 3370 5590 3370
Wire Wire Line
	5590 3470 5480 3470
Wire Wire Line
	5480 3570 5590 3570
Wire Wire Line
	5590 3670 5480 3670
Wire Wire Line
	5480 3770 5590 3770
Wire Wire Line
	5590 3870 5480 3870
Wire Wire Line
	5480 3970 5590 3970
Wire Wire Line
	5590 4070 5480 4070
Wire Wire Line
	5590 4270 5480 4270
Wire Wire Line
	5480 4370 5590 4370
Wire Wire Line
	5590 4470 5480 4470
Wire Wire Line
	5480 4570 5590 4570
Wire Wire Line
	5590 4670 5480 4670
Wire Wire Line
	5480 4770 5590 4770
Wire Wire Line
	5480 4870 5590 4870
Wire Wire Line
	5480 4970 5590 4970
Wire Wire Line
	8930 4920 8800 4920
Wire Wire Line
	8800 4820 8930 4820
Wire Wire Line
	8930 4720 8800 4720
Wire Wire Line
	8930 4620 8800 4620
Wire Wire Line
	8800 4520 8930 4520
Wire Wire Line
	8930 4420 8800 4420
Wire Wire Line
	8800 4320 8930 4320
Wire Wire Line
	8800 4220 8930 4220
Wire Wire Line
	8930 4020 8800 4020
Wire Wire Line
	8930 3920 8800 3920
Wire Wire Line
	8930 3820 8800 3820
Wire Wire Line
	8930 3720 8800 3720
Wire Wire Line
	8930 3620 8800 3620
Wire Wire Line
	8930 3520 8800 3520
Wire Wire Line
	8930 3420 8800 3420
Wire Wire Line
	8930 3320 8800 3320
Wire Wire Line
	6050 2420 8270 2420
Wire Wire Line
	6060 2020 8270 2020
Wire Wire Line
	7690 3370 8000 3370
Wire Wire Line
	7690 3470 8000 3470
Wire Wire Line
	7995 2075 7995 2020
Connection ~ 7995 2020
Wire Wire Line
	7995 2375 7995 2420
Connection ~ 7995 2420
Wire Wire Line
	6605 2080 6605 2020
Connection ~ 6605 2020
Wire Wire Line
	6605 2380 6605 2420
Connection ~ 6605 2420
Wire Wire Line
	7530 4570 8000 4570
Connection ~ 7625 5100
Connection ~ 7420 5100
Connection ~ 7495 4015
Connection ~ 7290 4015
Connection ~ 4230 4285
Connection ~ 4025 4285
Connection ~ 4305 5150
Connection ~ 4100 5150
Wire Wire Line
	4350 4720 4680 4720
Wire Wire Line
	4210 4620 4680 4620
Wire Wire Line
	4100 5150 5050 5150
Wire Wire Line
	4680 4520 4395 4520
Wire Wire Line
	4395 4520 4395 4285
Wire Wire Line
	4395 4285 4025 4285
Wire Wire Line
	8000 4470 7840 4470
Wire Wire Line
	7840 4470 7840 5100
Connection ~ 7840 5100
Wire Wire Line
	7420 5100 8370 5100
Wire Wire Line
	7290 4015 7660 4015
$EndSCHEMATC
