EESchema Schematic File Version 2
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:Capacitors
LIBS:Connectors
LIBS:Discretes
LIBS:ICs
LIBS:Allegro
LIBS:Resistors
LIBS:TestPoints
LIBS:sixteen_one_socket-cache
EELAYER 26 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Text GLabel 6710 5585 1    30   Input ~ 0
P4_D11
Text GLabel 6810 5585 1    30   Input ~ 0
P4_D13
Text GLabel 6910 5585 1    30   Input ~ 0
P4_D15
Text GLabel 6610 5585 1    30   Input ~ 0
P4_D9
Text GLabel 5925 5650 3    30   Input ~ 0
P3_D3
Text GLabel 6710 5685 3    30   Input ~ 0
P4_D5
Text GLabel 6125 5550 1    30   Input ~ 0
P3_D16
Text GLabel 6910 5685 3    30   Input ~ 0
P4_D1
Text GLabel 6325 5550 1    30   Input ~ 0
P3_D12
Text GLabel 3810 5515 1    30   Input ~ 0
P1_D14
Text GLabel 6610 5685 3    30   Input ~ 0
P4_D7
Text GLabel 4010 5515 1    30   Input ~ 0
P1_D10
Text GLabel 7110 5685 3    30   Input ~ 0
P4_D4
Text GLabel 3910 5615 3    30   Input ~ 0
P1_D6
Text GLabel 5725 5650 3    30   Input ~ 0
P3_D7
Text GLabel 3710 5615 3    30   Input ~ 0
P1_D2
Text GLabel 7210 5585 1    30   Input ~ 0
P4_D12
Text GLabel 5825 5550 1    30   Input ~ 0
P3_D11
Text GLabel 4965 5640 1    30   Input ~ 0
P2_D14
Text GLabel 5925 5550 1    30   Input ~ 0
P3_D13
Text GLabel 7310 5685 3    30   Input ~ 0
P4_D8
Text GLabel 6025 5550 1    30   Input ~ 0
P3_D15
Text GLabel 5165 5640 1    30   Input ~ 0
P2_D10
Text GLabel 5725 5550 1    30   Input ~ 0
P3_D9
Text GLabel 4010 5615 3    30   Input ~ 0
P1_D8
Text GLabel 6810 5685 3    30   Input ~ 0
P4_D3
Text GLabel 5065 5740 3    30   Input ~ 0
P2_D6
Text GLabel 5825 5650 3    30   Input ~ 0
P3_D5
Text GLabel 3810 5615 3    30   Input ~ 0
P1_D4
Text GLabel 7010 5585 1    30   Input ~ 0
P4_D16
Text GLabel 4865 5740 3    30   Input ~ 0
P2_D2
Text GLabel 6025 5650 3    30   Input ~ 0
P3_D1
Text GLabel 3710 5515 1    30   Input ~ 0
P1_D16
Text GLabel 4565 5640 1    30   Input ~ 0
P2_D11
Text GLabel 6225 5550 1    30   Input ~ 0
P3_D14
Text GLabel 4665 5640 1    30   Input ~ 0
P2_D13
Text GLabel 3910 5515 1    30   Input ~ 0
P1_D12
Text GLabel 4765 5640 1    30   Input ~ 0
P2_D15
Text GLabel 6425 5550 1    30   Input ~ 0
P3_D10
Text GLabel 4465 5640 1    30   Input ~ 0
P2_D9
Text GLabel 5165 5740 3    30   Input ~ 0
P2_D8
Text GLabel 3310 5615 3    30   Input ~ 0
P1_D7
Text GLabel 6325 5650 3    30   Input ~ 0
P3_D6
Text GLabel 4565 5740 3    30   Input ~ 0
P2_D5
Text GLabel 4965 5740 3    30   Input ~ 0
P2_D4
Text GLabel 3510 5615 3    30   Input ~ 0
P1_D3
Text GLabel 6125 5650 3    30   Input ~ 0
P3_D2
Text GLabel 4765 5740 3    30   Input ~ 0
P2_D1
Text GLabel 4865 5640 1    30   Input ~ 0
P2_D16
Text GLabel 3410 5515 1    30   Input ~ 0
P1_D11
Text GLabel 7110 5585 1    30   Input ~ 0
P4_D14
Text GLabel 5065 5640 1    30   Input ~ 0
P2_D12
Text GLabel 7310 5585 1    30   Input ~ 0
P4_D10
Text GLabel 6425 5650 3    30   Input ~ 0
P3_D8
Text GLabel 7210 5685 3    30   Input ~ 0
P4_D6
Text GLabel 6225 5650 3    30   Input ~ 0
P3_D4
Text GLabel 7010 5685 3    30   Input ~ 0
P4_D2
Text GLabel 3510 5515 1    30   Input ~ 0
P1_D13
Text GLabel 3610 5515 1    30   Input ~ 0
P1_D15
Text GLabel 3310 5515 1    30   Input ~ 0
P1_D9
Text GLabel 4465 5740 3    30   Input ~ 0
P2_D7
Text GLabel 3410 5615 3    30   Input ~ 0
P1_D5
Text GLabel 4665 5740 3    30   Input ~ 0
P2_D3
Text GLabel 3610 5615 3    30   Input ~ 0
P1_D1
Text GLabel 4720 2160 3    30   Input ~ 0
P1_D2
Text GLabel 4780 2160 3    30   Input ~ 0
P2_D2
Text GLabel 4840 2160 3    30   Input ~ 0
P3_D2
Text GLabel 4900 2160 3    30   Input ~ 0
P4_D2
Text GLabel 6030 2160 3    30   Input ~ 0
P4_D3
Text GLabel 7000 2150 3    30   Input ~ 0
P4_D4
Text GLabel 3740 3030 3    30   Input ~ 0
P4_D5
Text GLabel 4900 3030 3    30   Input ~ 0
P4_D6
Text GLabel 6030 3030 3    30   Input ~ 0
P4_D7
Text GLabel 7000 3020 3    30   Input ~ 0
P4_D8
Text GLabel 7000 3720 3    30   Input ~ 0
P4_D12
Text GLabel 7000 4660 3    30   Input ~ 0
P4_D16
Text GLabel 6030 3730 3    30   Input ~ 0
P4_D11
Text GLabel 6030 4670 3    30   Input ~ 0
P4_D15
Text GLabel 4900 3730 3    30   Input ~ 0
P4_D10
Text GLabel 4900 4670 3    30   Input ~ 0
P4_D14
Text GLabel 3740 3730 3    30   Input ~ 0
P4_D9
Text GLabel 3740 4670 3    30   Input ~ 0
P4_D13
Text GLabel 3680 4670 3    30   Input ~ 0
P3_D13
Text GLabel 3680 3730 3    30   Input ~ 0
P3_D9
Text GLabel 4840 3730 3    30   Input ~ 0
P3_D10
Text GLabel 4840 4670 3    30   Input ~ 0
P3_D14
Text GLabel 5970 3730 3    30   Input ~ 0
P3_D11
Text GLabel 5970 4670 3    30   Input ~ 0
P3_D15
Text GLabel 6940 4660 3    30   Input ~ 0
P3_D16
Text GLabel 6940 3720 3    30   Input ~ 0
P3_D12
Text GLabel 6940 3020 3    30   Input ~ 0
P3_D8
Text GLabel 6940 2150 3    30   Input ~ 0
P3_D4
Text GLabel 5970 3030 3    30   Input ~ 0
P3_D7
Text GLabel 5970 2160 3    30   Input ~ 0
P3_D3
Text GLabel 4840 3030 3    30   Input ~ 0
P3_D6
Text GLabel 3680 3030 3    30   Input ~ 0
P3_D5
Text GLabel 3620 3030 3    30   Input ~ 0
P2_D5
Text GLabel 4780 3030 3    30   Input ~ 0
P2_D6
Text GLabel 5910 2160 3    30   Input ~ 0
P2_D3
Text GLabel 6880 2150 3    30   Input ~ 0
P2_D4
Text GLabel 6880 3020 3    30   Input ~ 0
P2_D8
Text GLabel 5910 3030 3    30   Input ~ 0
P2_D7
Text GLabel 3620 3730 3    30   Input ~ 0
P2_D9
Text GLabel 4780 3730 3    30   Input ~ 0
P2_D10
Text GLabel 3620 4670 3    30   Input ~ 0
P2_D13
Text GLabel 4780 4670 3    30   Input ~ 0
P2_D14
Text GLabel 5910 3730 3    30   Input ~ 0
P2_D11
Text GLabel 5910 4670 3    30   Input ~ 0
P2_D15
Text GLabel 6880 3720 3    30   Input ~ 0
P2_D12
Text GLabel 6880 4660 3    30   Input ~ 0
P2_D16
Text GLabel 6820 4660 3    30   Input ~ 0
P1_D16
Text GLabel 6820 3720 3    30   Input ~ 0
P1_D12
Text GLabel 5850 3730 3    30   Input ~ 0
P1_D11
Text GLabel 5850 4670 3    30   Input ~ 0
P1_D15
Text GLabel 4720 4670 3    30   Input ~ 0
P1_D14
Text GLabel 4720 3730 3    30   Input ~ 0
P1_D10
Text GLabel 3560 4670 3    30   Input ~ 0
P1_D13
Text GLabel 3560 3730 3    30   Input ~ 0
P1_D9
Text GLabel 3560 3030 3    30   Input ~ 0
P1_D5
Text GLabel 4720 3030 3    30   Input ~ 0
P1_D6
Text GLabel 5850 3030 3    30   Input ~ 0
P1_D7
Text GLabel 6820 3020 3    30   Input ~ 0
P1_D8
Text GLabel 6820 2150 3    30   Input ~ 0
P1_D4
Text GLabel 5850 2160 3    30   Input ~ 0
P1_D3
$Comp
L 63067 U2
U 1 1 59652D33
P 4810 2160
F 0 "U2" H 5020 2190 60  0000 C CNN
F 1 "63067" H 4810 2260 39  0000 C CNN
F 2 "Connectors:63067_socket" H 4810 2160 60  0001 C CNN
F 3 "" H 4810 2160 60  0001 C CNN
	1    4810 2160
	1    0    0    -1  
$EndComp
$Comp
L 63067 U3
U 1 1 59652DA4
P 5940 2160
F 0 "U3" H 6150 2190 60  0000 C CNN
F 1 "63067" H 5940 2260 39  0000 C CNN
F 2 "Connectors:63067_socket" H 5940 2160 60  0001 C CNN
F 3 "" H 5940 2160 60  0001 C CNN
	1    5940 2160
	1    0    0    -1  
$EndComp
$Comp
L 63067 U4
U 1 1 59652E76
P 6910 2150
F 0 "U4" H 7120 2180 60  0000 C CNN
F 1 "63067" H 6910 2250 39  0000 C CNN
F 2 "Connectors:63067_socket" H 6910 2150 60  0001 C CNN
F 3 "" H 6910 2150 60  0001 C CNN
	1    6910 2150
	1    0    0    -1  
$EndComp
$Comp
L 63067 U8
U 1 1 59652F60
P 6910 3020
F 0 "U8" H 7120 3050 60  0000 C CNN
F 1 "63067" H 6910 3120 39  0000 C CNN
F 2 "Connectors:63067_socket" H 6910 3020 60  0001 C CNN
F 3 "" H 6910 3020 60  0001 C CNN
	1    6910 3020
	1    0    0    -1  
$EndComp
$Comp
L 63067 U7
U 1 1 596530BA
P 5940 3030
F 0 "U7" H 6150 3060 60  0000 C CNN
F 1 "63067" H 5940 3130 39  0000 C CNN
F 2 "Connectors:63067_socket" H 5940 3030 60  0001 C CNN
F 3 "" H 5940 3030 60  0001 C CNN
	1    5940 3030
	1    0    0    -1  
$EndComp
$Comp
L 63067 U6
U 1 1 59653216
P 4810 3030
F 0 "U6" H 5020 3060 60  0000 C CNN
F 1 "63067" H 4810 3130 39  0000 C CNN
F 2 "Connectors:63067_socket" H 4810 3030 60  0001 C CNN
F 3 "" H 4810 3030 60  0001 C CNN
	1    4810 3030
	1    0    0    -1  
$EndComp
$Comp
L 63067 U5
U 1 1 59653380
P 3650 3030
F 0 "U5" H 3860 3060 60  0000 C CNN
F 1 "63067" H 3650 3130 39  0000 C CNN
F 2 "Connectors:63067_socket" H 3650 3030 60  0001 C CNN
F 3 "" H 3650 3030 60  0001 C CNN
	1    3650 3030
	1    0    0    -1  
$EndComp
$Comp
L 63067 U9
U 1 1 59653519
P 3650 3730
F 0 "U9" H 3860 3760 60  0000 C CNN
F 1 "63067" H 3650 3830 39  0000 C CNN
F 2 "Connectors:63067_socket" H 3650 3730 60  0001 C CNN
F 3 "" H 3650 3730 60  0001 C CNN
	1    3650 3730
	1    0    0    -1  
$EndComp
$Comp
L 63067 U10
U 1 1 596536B7
P 4810 3730
F 0 "U10" H 5020 3760 60  0000 C CNN
F 1 "63067" H 4810 3830 39  0000 C CNN
F 2 "Connectors:63067_socket" H 4810 3730 60  0001 C CNN
F 3 "" H 4810 3730 60  0001 C CNN
	1    4810 3730
	1    0    0    -1  
$EndComp
$Comp
L 63067 U11
U 1 1 596538AC
P 5940 3730
F 0 "U11" H 6150 3760 60  0000 C CNN
F 1 "63067" H 5940 3830 39  0000 C CNN
F 2 "Connectors:63067_socket" H 5940 3730 60  0001 C CNN
F 3 "" H 5940 3730 60  0001 C CNN
	1    5940 3730
	1    0    0    -1  
$EndComp
$Comp
L 63067 U12
U 1 1 59653AF0
P 6910 3720
F 0 "U12" H 7120 3750 60  0000 C CNN
F 1 "63067" H 6910 3820 39  0000 C CNN
F 2 "Connectors:63067_socket" H 6910 3720 60  0001 C CNN
F 3 "" H 6910 3720 60  0001 C CNN
	1    6910 3720
	1    0    0    -1  
$EndComp
$Comp
L 63067 U16
U 1 1 59653C88
P 6910 4660
F 0 "U16" H 7120 4690 60  0000 C CNN
F 1 "63067" H 6910 4760 39  0000 C CNN
F 2 "Connectors:63067_socket" H 6910 4660 60  0001 C CNN
F 3 "" H 6910 4660 60  0001 C CNN
	1    6910 4660
	1    0    0    -1  
$EndComp
$Comp
L 63067 U15
U 1 1 59653ED0
P 5940 4670
F 0 "U15" H 6150 4700 60  0000 C CNN
F 1 "63067" H 5940 4770 39  0000 C CNN
F 2 "Connectors:63067_socket" H 5940 4670 60  0001 C CNN
F 3 "" H 5940 4670 60  0001 C CNN
	1    5940 4670
	1    0    0    -1  
$EndComp
$Comp
L 63067 U14
U 1 1 59654160
P 4810 4670
F 0 "U14" H 5020 4700 60  0000 C CNN
F 1 "63067" H 4810 4770 39  0000 C CNN
F 2 "Connectors:63067_socket" H 4810 4670 60  0001 C CNN
F 3 "" H 4810 4670 60  0001 C CNN
	1    4810 4670
	1    0    0    -1  
$EndComp
$Comp
L 63067 U13
U 1 1 596543FA
P 3650 4670
F 0 "U13" H 3860 4700 60  0000 C CNN
F 1 "63067" H 3650 4770 39  0000 C CNN
F 2 "Connectors:63067_socket" H 3650 4670 60  0001 C CNN
F 3 "" H 3650 4670 60  0001 C CNN
	1    3650 4670
	1    0    0    -1  
$EndComp
$Comp
L 63067 U1
U 1 1 59652C54
P 3650 2160
F 0 "U1" H 3860 2190 60  0000 C CNN
F 1 "63067" H 3650 2260 39  0000 C CNN
F 2 "Connectors:63067_socket" H 3650 2160 60  0001 C CNN
F 3 "" H 3650 2160 60  0001 C CNN
	1    3650 2160
	1    0    0    -1  
$EndComp
Text GLabel 3740 2160 3    30   Input ~ 0
P4_D1
Text GLabel 3680 2160 3    30   Input ~ 0
P3_D1
Text GLabel 3620 2160 3    30   Input ~ 0
P2_D1
Text GLabel 3560 2160 3    30   Input ~ 0
P1_D1
$Comp
L 16_bare_wire U20
U 1 1 5D9CF408
P 6960 5635
F 0 "U20" H 6954 5385 60  0000 C CNN
F 1 "16_bare_wire" H 6954 5491 60  0000 C CNN
F 2 "Connectors:16_Bare_wire" H 7335 5735 60  0001 C CNN
F 3 "" H 7335 5735 60  0001 C CNN
	1    6960 5635
	-1   0    0    1   
$EndComp
$Comp
L 16_bare_wire U19
U 1 1 5D9CF5AA
P 6075 5600
F 0 "U19" H 6069 5350 60  0000 C CNN
F 1 "16_bare_wire" H 6069 5456 60  0000 C CNN
F 2 "Connectors:16_Bare_wire" H 6450 5700 60  0001 C CNN
F 3 "" H 6450 5700 60  0001 C CNN
	1    6075 5600
	-1   0    0    1   
$EndComp
$Comp
L 16_bare_wire U17
U 1 1 5D9CF5EE
P 3660 5565
F 0 "U17" H 3654 5315 60  0000 C CNN
F 1 "16_bare_wire" H 3654 5421 60  0000 C CNN
F 2 "Connectors:16_Bare_wire" H 4035 5665 60  0001 C CNN
F 3 "" H 4035 5665 60  0001 C CNN
	1    3660 5565
	-1   0    0    1   
$EndComp
$Comp
L 16_bare_wire U18
U 1 1 5D9CF630
P 4815 5690
F 0 "U18" H 4809 5440 60  0000 C CNN
F 1 "16_bare_wire" H 4809 5546 60  0000 C CNN
F 2 "Connectors:16_Bare_wire" H 5190 5790 60  0001 C CNN
F 3 "" H 5190 5790 60  0001 C CNN
	1    4815 5690
	-1   0    0    1   
$EndComp
$Comp
L ProtoTP TP2
U 1 1 5D9F6DEB
P 8695 2360
F 0 "TP2" H 8773 2507 60  0000 L CNN
F 1 "ProtoTP" H 8773 2401 60  0000 L CNN
F 2 "connectors:1pin" H 8695 2360 60  0000 C CNN
F 3 "" H 8695 2360 60  0000 C CNN
	1    8695 2360
	1    0    0    -1  
$EndComp
$Comp
L ProtoTP TP3
U 1 1 5D9F6E57
P 9215 2350
F 0 "TP3" H 9293 2497 60  0000 L CNN
F 1 "ProtoTP" H 9293 2391 60  0000 L CNN
F 2 "connectors:1pin" H 9215 2350 60  0000 C CNN
F 3 "" H 9215 2350 60  0000 C CNN
	1    9215 2350
	1    0    0    -1  
$EndComp
$Comp
L ProtoTP TP1
U 1 1 5D9F6EE1
P 8680 2820
F 0 "TP1" H 8758 2967 60  0000 L CNN
F 1 "ProtoTP" H 8758 2861 60  0000 L CNN
F 2 "connectors:1pin" H 8680 2820 60  0000 C CNN
F 3 "" H 8680 2820 60  0000 C CNN
	1    8680 2820
	1    0    0    -1  
$EndComp
$Comp
L ProtoTP TP4
U 1 1 5D9F6F6F
P 9240 2805
F 0 "TP4" H 9318 2952 60  0000 L CNN
F 1 "ProtoTP" H 9318 2846 60  0000 L CNN
F 2 "connectors:1pin" H 9240 2805 60  0000 C CNN
F 3 "" H 9240 2805 60  0000 C CNN
	1    9240 2805
	1    0    0    -1  
$EndComp
Wire Wire Line
	8695 2360 8695 2440
Wire Wire Line
	8695 2440 9215 2440
Wire Wire Line
	8950 2440 8950 2855
Wire Wire Line
	9240 2855 8680 2855
Wire Wire Line
	8680 2855 8680 2820
Wire Wire Line
	9240 2805 9240 2855
Connection ~ 8950 2855
Wire Wire Line
	9215 2440 9215 2350
Connection ~ 8950 2440
Text Label 8950 2545 0    60   ~ 0
GND
$EndSCHEMATC
