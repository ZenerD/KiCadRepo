EESchema Schematic File Version 2
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:Capacitors
LIBS:Connectors
LIBS:Discretes
LIBS:ICs
LIBS:Allegro
LIBS:Resistors
LIBS:TestPoints
LIBS:sixteen_one-cache
EELAYER 26 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 2 4
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L LAA710 U9
U 2 1 57B7568C
P 8460 2490
F 0 "U9" H 8465 2987 60  0000 C CNN
F 1 "LAA710" H 8465 2881 60  0000 C CNN
F 2 "ICs:LAA710S" H 8450 2850 60  0001 C CNN
F 3 "" H 8450 2850 60  0001 C CNN
	2    8460 2490
	1    0    0    -1  
$EndComp
$Comp
L LAA710 U8
U 1 1 57B75717
P 8450 5340
F 0 "U8" H 8455 5837 60  0000 C CNN
F 1 "LAA710" H 8455 5731 60  0000 C CNN
F 2 "ICs:LAA710S" H 8440 5700 60  0001 C CNN
F 3 "" H 8440 5700 60  0001 C CNN
F 4 "LAA710S-ND" H 8450 5340 60  0001 C CNN "DigikeyPN"
	1    8450 5340
	1    0    0    -1  
$EndComp
$Comp
L LAA710 U6
U 2 1 57B7638B
P 5230 3410
F 0 "U6" H 5235 3907 60  0000 C CNN
F 1 "LAA710" H 5235 3801 60  0000 C CNN
F 2 "ICs:LAA710S" H 5220 3770 60  0001 C CNN
F 3 "" H 5220 3770 60  0001 C CNN
	2    5230 3410
	1    0    0    -1  
$EndComp
$Comp
L LAA710 U6
U 1 1 57B76397
P 5230 3110
F 0 "U6" H 5235 3607 60  0000 C CNN
F 1 "LAA710" H 5235 3501 60  0000 C CNN
F 2 "ICs:LAA710S" H 5220 3470 60  0001 C CNN
F 3 "" H 5220 3470 60  0001 C CNN
F 4 "LAA710S-ND" H 5230 3110 60  0001 C CNN "DigikeyPN"
	1    5230 3110
	1    0    0    -1  
$EndComp
$Comp
L LAA710 U5
U 2 1 57B763A3
P 5230 2510
F 0 "U5" H 5235 3007 60  0000 C CNN
F 1 "LAA710" H 5235 2901 60  0000 C CNN
F 2 "ICs:LAA710S" H 5220 2870 60  0001 C CNN
F 3 "" H 5220 2870 60  0001 C CNN
	2    5230 2510
	1    0    0    -1  
$EndComp
$Comp
L LAA710 U9
U 1 1 57BB2AE0
P 8460 2190
F 0 "U9" H 8465 2687 60  0000 C CNN
F 1 "LAA710" H 8465 2581 60  0000 C CNN
F 2 "ICs:LAA710S" H 8450 2550 60  0001 C CNN
F 3 "" H 8450 2550 60  0001 C CNN
F 4 "LAA710S-ND" H 8460 2190 60  0001 C CNN "DigikeyPN"
	1    8460 2190
	1    0    0    -1  
$EndComp
$Comp
L LAA710 U5
U 1 1 57B763AF
P 5230 2210
F 0 "U5" H 5235 2707 60  0000 C CNN
F 1 "LAA710" H 5235 2601 60  0000 C CNN
F 2 "ICs:LAA710S" H 5220 2570 60  0001 C CNN
F 3 "" H 5220 2570 60  0001 C CNN
F 4 "LAA710S-ND" H 5230 2210 60  0001 C CNN "DigikeyPN"
	1    5230 2210
	1    0    0    -1  
$EndComp
Text Notes 7390 7500 0    79   ~ 0
16 Site GTS Board Controller\n
Text Notes 8160 7640 0    60   ~ 0
8/26/16
Text Notes 10590 7640 0    60   ~ 0
1.5\n
Text HLabel 3320 1930 0    30   Input ~ 0
VCC_pin1_en
Text HLabel 3320 2830 0    30   Input ~ 0
VCC_pin2_en
Text HLabel 2960 3010 0    30   Input ~ 0
VCC_pin3_en
Text HLabel 2960 3910 0    30   Input ~ 0
VCC_pin4_en
Text HLabel 4070 1920 0    60   Input ~ 0
D_VCC
Text HLabel 6760 1900 0    60   Input ~ 0
D_Mux2
Text HLabel 6810 2010 0    30   Input ~ 0
Mux2_pin1_en
Text HLabel 6810 2910 0    30   Input ~ 0
Mux2_pin2_en
Text HLabel 6390 2890 0    30   Input ~ 0
Mux2_pin3_en
Text HLabel 6390 3790 0    30   Input ~ 0
Mux2_pin4_en
Text HLabel 9510 3830 2    39   Output ~ 0
P1_mid
Text HLabel 9510 3890 2    39   Output ~ 0
P2_mid
Text HLabel 9510 3950 2    39   Output ~ 0
P3_mid
Text HLabel 9510 4010 2    39   Output ~ 0
P4_mid
Text HLabel 4070 4180 0    60   Input ~ 0
D_Mux
Text HLabel 6840 4140 0    60   Input ~ 0
D_GND
Text HLabel 3390 4310 0    30   Input ~ 0
Mux_pin1_en
Text HLabel 3390 5210 0    30   Input ~ 0
Mux_pin2_en
Text HLabel 2990 5400 0    30   Input ~ 0
Mux_pin3_en
Text HLabel 2990 6300 0    30   Input ~ 0
Mux_pin4_en
Text HLabel 6880 4290 0    30   Input ~ 0
GND_pin1_en
Text HLabel 6880 5190 0    30   Input ~ 0
GND_pin2_en
Text HLabel 6450 5220 0    30   Input ~ 0
GND_pin3_en
Text HLabel 6450 6120 0    30   Input ~ 0
GND_pin4_en
$Comp
L LAA710 U8
U 2 1 57B76385
P 8450 5640
F 0 "U8" H 8455 6137 60  0000 C CNN
F 1 "LAA710" H 8455 6031 60  0000 C CNN
F 2 "ICs:LAA710S" H 8440 6000 60  0001 C CNN
F 3 "" H 8440 6000 60  0001 C CNN
	2    8450 5640
	1    0    0    -1  
$EndComp
Wire Wire Line
	5520 2110 5810 2110
Wire Wire Line
	5810 1920 5810 3310
Wire Wire Line
	5520 2410 5810 2410
Connection ~ 5810 2110
Wire Wire Line
	5520 3010 5810 3010
Connection ~ 5810 2410
Wire Wire Line
	5810 3310 5520 3310
Connection ~ 5810 3010
Wire Wire Line
	5500 4380 5800 4380
Wire Wire Line
	5800 4180 5800 5580
Wire Wire Line
	5500 4680 5800 4680
Connection ~ 5800 4380
Wire Wire Line
	5500 5280 5800 5280
Connection ~ 5800 4680
Wire Wire Line
	5800 5580 5500 5580
Connection ~ 5800 5280
Wire Wire Line
	8750 2090 9050 2090
Wire Wire Line
	8740 4340 9040 4340
Wire Wire Line
	9040 4140 9040 5540
Connection ~ 9040 4340
Wire Wire Line
	8740 5240 9040 5240
Wire Wire Line
	8740 4640 9040 4640
Connection ~ 9040 4640
Wire Wire Line
	9050 2390 8750 2390
Wire Wire Line
	9050 2990 8750 2990
Connection ~ 9040 5240
Wire Wire Line
	9040 5540 8740 5540
Wire Wire Line
	9050 3290 8750 3290
Wire Wire Line
	3290 5780 4930 5780
Wire Wire Line
	3290 5480 4930 5480
Wire Wire Line
	3680 4880 4930 4880
Wire Wire Line
	3680 4580 4930 4580
Wire Wire Line
	6790 5740 8170 5740
Wire Wire Line
	6790 5440 8170 5440
Wire Wire Line
	7190 4840 8170 4840
Wire Wire Line
	7130 4540 8170 4540
Wire Wire Line
	3230 3210 4950 3210
Wire Wire Line
	3610 2610 4950 2610
Wire Wire Line
	3610 2310 4950 2310
Wire Wire Line
	6690 3490 8180 3490
Wire Wire Line
	6690 3190 8180 3190
Wire Wire Line
	5520 2310 5960 2310
Wire Wire Line
	5960 2310 5960 4580
Wire Wire Line
	5960 4580 5500 4580
Wire Wire Line
	5920 4880 5500 4880
Wire Wire Line
	5920 2610 5920 4880
Wire Wire Line
	5920 2610 5520 2610
Wire Wire Line
	5880 5480 5500 5480
Wire Wire Line
	5880 3210 5880 5480
Wire Wire Line
	5880 3210 5520 3210
Wire Wire Line
	5520 3510 5840 3510
Wire Wire Line
	5840 3510 5840 5780
Wire Wire Line
	5840 5780 5500 5780
Wire Wire Line
	8750 2290 9260 2290
Wire Wire Line
	9260 2290 9260 4540
Wire Wire Line
	9260 4540 8740 4540
Wire Wire Line
	9220 4840 8740 4840
Wire Wire Line
	9220 2590 9220 4840
Wire Wire Line
	9220 2590 8750 2590
Wire Wire Line
	9180 5440 8740 5440
Wire Wire Line
	9180 3190 9180 5440
Wire Wire Line
	9180 3190 8750 3190
Wire Wire Line
	8750 3490 9140 3490
Wire Wire Line
	9140 3490 9140 5740
Wire Wire Line
	9140 5740 8740 5740
Wire Wire Line
	5960 3830 9510 3830
Connection ~ 9260 3830
Connection ~ 5960 3830
Wire Wire Line
	5920 3890 9510 3890
Connection ~ 9220 3890
Connection ~ 5920 3890
Wire Wire Line
	5880 3950 9510 3950
Connection ~ 9180 3950
Connection ~ 5880 3950
Wire Wire Line
	5840 4010 9510 4010
Connection ~ 9140 4010
Connection ~ 5840 4010
Wire Wire Line
	4070 4180 5800 4180
Wire Wire Line
	5810 1920 4070 1920
Wire Wire Line
	6840 4140 9040 4140
Wire Wire Line
	6760 1900 9050 1900
Wire Wire Line
	9050 1900 9050 3290
Connection ~ 9050 2990
Connection ~ 9050 2390
Connection ~ 9050 2090
Wire Wire Line
	4950 3510 3240 3510
Wire Wire Line
	3240 3510 3240 3660
Wire Wire Line
	3240 3660 3210 3660
Wire Wire Line
	3210 3260 3230 3260
Wire Wire Line
	3230 3260 3230 3210
Wire Wire Line
	3610 2310 3610 2180
Wire Wire Line
	3610 2180 3570 2180
Wire Wire Line
	3570 2580 3610 2580
Wire Wire Line
	3610 2580 3610 2610
Wire Wire Line
	3290 5480 3290 5650
Wire Wire Line
	3290 5650 3240 5650
Wire Wire Line
	3290 5780 3290 6050
Wire Wire Line
	3290 6050 3240 6050
Wire Wire Line
	3680 4880 3680 4960
Wire Wire Line
	3680 4960 3640 4960
Wire Wire Line
	3640 4560 3680 4560
Wire Wire Line
	3680 4560 3680 4580
Wire Wire Line
	6790 5740 6790 5870
Wire Wire Line
	6790 5870 6700 5870
Wire Wire Line
	6700 5470 6790 5470
Wire Wire Line
	6790 5470 6790 5440
Wire Wire Line
	7190 4840 7190 4940
Wire Wire Line
	7190 4940 7130 4940
Wire Wire Line
	6640 3140 6690 3140
Wire Wire Line
	6690 3140 6690 3190
Wire Wire Line
	7110 2590 7110 2660
Wire Wire Line
	7110 2660 7060 2660
Wire Wire Line
	7060 2260 7110 2260
Wire Wire Line
	7110 2260 7110 2290
Wire Wire Line
	6690 3490 6690 3540
Wire Wire Line
	6690 3540 6640 3540
Wire Wire Line
	7170 5540 7330 5540
Wire Wire Line
	7170 2090 7170 5540
Wire Wire Line
	7170 4340 7320 4340
Wire Wire Line
	7170 4640 7320 4640
Connection ~ 7170 4640
Wire Wire Line
	7170 5240 7330 5240
Connection ~ 7170 5240
Wire Wire Line
	7170 3290 7310 3290
Connection ~ 7170 4340
Wire Wire Line
	7170 2990 7310 2990
Connection ~ 7170 3290
Wire Wire Line
	7170 2390 7320 2390
Connection ~ 7170 2990
Wire Wire Line
	7170 2090 7320 2090
Connection ~ 7170 2390
Wire Wire Line
	6140 5670 6250 5670
Wire Wire Line
	6140 3340 6190 3340
Wire Wire Line
	6140 2460 6610 2460
Connection ~ 6140 3340
Wire Wire Line
	6140 4740 6680 4740
Connection ~ 6140 4740
Wire Wire Line
	2620 5850 2790 5850
Wire Wire Line
	2620 3460 2760 3460
Connection ~ 2620 3460
Wire Wire Line
	2620 2380 2620 5850
Wire Wire Line
	4060 5580 4240 5580
Wire Wire Line
	4060 2110 4260 2110
Wire Wire Line
	4260 2410 4060 2410
Connection ~ 4060 2410
Wire Wire Line
	4060 3010 4260 3010
Connection ~ 4060 3010
Wire Wire Line
	4060 3310 4250 3310
Connection ~ 4060 3310
Wire Wire Line
	4060 4380 4250 4380
Connection ~ 4060 4380
Wire Wire Line
	4060 4680 4240 4680
Connection ~ 4060 4680
Wire Wire Line
	4060 5280 4250 5280
Connection ~ 4060 5280
$Comp
L LAA710 U3
U 2 1 58D69330
P 5210 4780
F 0 "U3" H 5215 5277 60  0000 C CNN
F 1 "LAA710" H 5215 5171 60  0000 C CNN
F 2 "ICs:LAA710S" H 5200 5140 60  0001 C CNN
F 3 "" H 5200 5140 60  0001 C CNN
	2    5210 4780
	1    0    0    -1  
$EndComp
$Comp
L LAA710 U3
U 1 1 58D69336
P 5210 4480
F 0 "U3" H 5215 4977 60  0000 C CNN
F 1 "LAA710" H 5215 4871 60  0000 C CNN
F 2 "ICs:LAA710S" H 5200 4840 60  0001 C CNN
F 3 "" H 5200 4840 60  0001 C CNN
F 4 "LAA710S-ND" H 5210 4480 60  0001 C CNN "DigikeyPN"
	1    5210 4480
	1    0    0    -1  
$EndComp
$Comp
L LAA710 U4
U 2 1 58D69A0C
P 5210 5680
F 0 "U4" H 5215 6177 60  0000 C CNN
F 1 "LAA710" H 5215 6071 60  0000 C CNN
F 2 "ICs:LAA710S" H 5200 6040 60  0001 C CNN
F 3 "" H 5200 6040 60  0001 C CNN
	2    5210 5680
	1    0    0    -1  
$EndComp
$Comp
L LAA710 U4
U 1 1 58D69A12
P 5210 5380
F 0 "U4" H 5215 5877 60  0000 C CNN
F 1 "LAA710" H 5215 5771 60  0000 C CNN
F 2 "ICs:LAA710S" H 5200 5740 60  0001 C CNN
F 3 "" H 5200 5740 60  0001 C CNN
F 4 "LAA710S-ND" H 5210 5380 60  0001 C CNN "DigikeyPN"
	1    5210 5380
	1    0    0    -1  
$EndComp
Wire Wire Line
	7110 2290 8180 2290
Wire Wire Line
	8180 2590 7110 2590
$Comp
L LAA710 U7
U 1 1 58D6B75A
P 8450 4440
F 0 "U7" H 8455 4937 60  0000 C CNN
F 1 "LAA710" H 8455 4831 60  0000 C CNN
F 2 "ICs:LAA710S" H 8440 4800 60  0001 C CNN
F 3 "" H 8440 4800 60  0001 C CNN
F 4 "LAA710S-ND" H 8450 4440 60  0001 C CNN "DigikeyPN"
	1    8450 4440
	1    0    0    -1  
$EndComp
$Comp
L LAA710 U7
U 2 1 58D6B760
P 8450 4740
F 0 "U7" H 8455 5237 60  0000 C CNN
F 1 "LAA710" H 8455 5131 60  0000 C CNN
F 2 "ICs:LAA710S" H 8440 5100 60  0001 C CNN
F 3 "" H 8440 5100 60  0001 C CNN
	2    8450 4740
	1    0    0    -1  
$EndComp
$Comp
L LAA710 U10
U 1 1 58D6B840
P 8460 3090
F 0 "U10" H 8465 3587 60  0000 C CNN
F 1 "LAA710" H 8465 3481 60  0000 C CNN
F 2 "ICs:LAA710S" H 8450 3450 60  0001 C CNN
F 3 "" H 8450 3450 60  0001 C CNN
F 4 "LAA710S-ND" H 8460 3090 60  0001 C CNN "DigikeyPN"
	1    8460 3090
	1    0    0    -1  
$EndComp
$Comp
L LAA710 U10
U 2 1 58D6B846
P 8460 3390
F 0 "U10" H 8465 3887 60  0000 C CNN
F 1 "LAA710" H 8465 3781 60  0000 C CNN
F 2 "ICs:LAA710S" H 8450 3750 60  0001 C CNN
F 3 "" H 8450 3750 60  0001 C CNN
	2    8460 3390
	1    0    0    -1  
$EndComp
Text HLabel 2050 1150 0    60   Input ~ 0
GND
Text HLabel 2050 1040 0    60   Input ~ 0
5V
Wire Wire Line
	6140 2460 6140 5670
Text Label 6250 2460 0    60   ~ 0
GND
Text Label 7170 3680 1    60   ~ 0
5V
Text Label 2270 1040 2    60   ~ 0
5V
Wire Wire Line
	2050 1040 2270 1040
Wire Wire Line
	2050 1150 2270 1150
Text Label 2270 1150 2    60   ~ 0
GND
Wire Wire Line
	3120 2380 2620 2380
Text Label 2720 2380 0    60   ~ 0
GND
Wire Wire Line
	4060 2110 4060 5580
Text Label 4060 3830 1    60   ~ 0
5V
Wire Wire Line
	2620 4760 3190 4760
Connection ~ 2620 4760
$Comp
L 1k-BE R7
U 1 1 59087E19
P 4460 2110
F 0 "R7" H 4460 2306 60  0000 C CNN
F 1 "2k-BF" H 4360 2210 60  0000 C CNN
F 2 "Resistors:R0603" H 4460 2110 60  0001 C CNN
F 3 "" H 4560 2260 60  0000 C CNN
F 4 "RMCF0603JT2K00TR-ND" H 4478 2378 60  0001 C CNN "DigikeyPN"
F 5 "1%" H 4660 2210 60  0000 C CNN "Precision"
	1    4460 2110
	1    0    0    -1  
$EndComp
Wire Wire Line
	4640 5580 4930 5580
Wire Wire Line
	4930 5280 4650 5280
Wire Wire Line
	4930 4680 4640 4680
Wire Wire Line
	4650 4380 4930 4380
Wire Wire Line
	4950 3310 4650 3310
Wire Wire Line
	4950 3010 4660 3010
Wire Wire Line
	4950 2410 4660 2410
Wire Wire Line
	4660 2110 4950 2110
Wire Wire Line
	8170 5540 7730 5540
Wire Wire Line
	7730 5240 8170 5240
Wire Wire Line
	8170 4640 7720 4640
Wire Wire Line
	8170 4340 7720 4340
Wire Wire Line
	7710 3290 8180 3290
Wire Wire Line
	8180 2990 7710 2990
Wire Wire Line
	7720 2390 8180 2390
Wire Wire Line
	8180 2090 7720 2090
$Comp
L QS5K2 Q3
U 1 1 596EAB89
P 3320 2380
F 0 "Q3" H 3288 2556 60  0000 C CNN
F 1 "QS5K2" H 3318 2452 60  0000 C CNN
F 2 "Discretes:SOT25T" H 3320 2380 60  0001 C CNN
F 3 "" H 3320 2380 60  0000 C CNN
F 4 "QS5K2CT-ND" H 3318 2680 60  0001 C CNN "DigikeyPN"
	1    3320 2380
	0    1    1    0   
$EndComp
$Comp
L QS5K2 Q1
U 1 1 596EB31B
P 2960 3460
F 0 "Q1" H 2928 3636 60  0000 C CNN
F 1 "QS5K2" H 2958 3532 60  0000 C CNN
F 2 "Discretes:SOT25T" H 2960 3460 60  0001 C CNN
F 3 "" H 2960 3460 60  0000 C CNN
F 4 "QS5K2CT-ND" H 2958 3760 60  0001 C CNN "DigikeyPN"
	1    2960 3460
	0    1    1    0   
$EndComp
$Comp
L QS5K2 Q4
U 1 1 596EBE11
P 3390 4760
F 0 "Q4" H 3358 4936 60  0000 C CNN
F 1 "QS5K2" H 3388 4832 60  0000 C CNN
F 2 "Discretes:SOT25T" H 3390 4760 60  0001 C CNN
F 3 "" H 3390 4760 60  0000 C CNN
F 4 "QS5K2CT-ND" H 3388 5060 60  0001 C CNN "DigikeyPN"
	1    3390 4760
	0    1    1    0   
$EndComp
$Comp
L QS5K2 Q2
U 1 1 596EC3AB
P 2990 5850
F 0 "Q2" H 2958 6026 60  0000 C CNN
F 1 "QS5K2" H 2988 5922 60  0000 C CNN
F 2 "Discretes:SOT25T" H 2990 5850 60  0001 C CNN
F 3 "" H 2990 5850 60  0000 C CNN
F 4 "QS5K2CT-ND" H 2988 6150 60  0001 C CNN "DigikeyPN"
	1    2990 5850
	0    1    1    0   
$EndComp
$Comp
L QS5K2 Q6
U 1 1 596ECCBF
P 6450 5670
F 0 "Q6" H 6418 5846 60  0000 C CNN
F 1 "QS5K2" H 6448 5742 60  0000 C CNN
F 2 "Discretes:SOT25T" H 6450 5670 60  0001 C CNN
F 3 "" H 6450 5670 60  0000 C CNN
F 4 "QS5K2CT-ND" H 6448 5970 60  0001 C CNN "DigikeyPN"
	1    6450 5670
	0    1    1    0   
$EndComp
$Comp
L QS5K2 Q8
U 1 1 596ED29F
P 6880 4740
F 0 "Q8" H 6848 4916 60  0000 C CNN
F 1 "QS5K2" H 6878 4812 60  0000 C CNN
F 2 "Discretes:SOT25T" H 6880 4740 60  0001 C CNN
F 3 "" H 6880 4740 60  0000 C CNN
F 4 "QS5K2CT-ND" H 6878 5040 60  0001 C CNN "DigikeyPN"
	1    6880 4740
	0    1    1    0   
$EndComp
$Comp
L QS5K2 Q5
U 1 1 596ED9D9
P 6390 3340
F 0 "Q5" H 6358 3516 60  0000 C CNN
F 1 "QS5K2" H 6388 3412 60  0000 C CNN
F 2 "Discretes:SOT25T" H 6390 3340 60  0001 C CNN
F 3 "" H 6390 3340 60  0000 C CNN
F 4 "QS5K2CT-ND" H 6388 3640 60  0001 C CNN "DigikeyPN"
	1    6390 3340
	0    1    1    0   
$EndComp
$Comp
L QS5K2 Q7
U 1 1 596EDF8F
P 6810 2460
F 0 "Q7" H 6778 2636 60  0000 C CNN
F 1 "QS5K2" H 6808 2532 60  0000 C CNN
F 2 "Discretes:SOT25T" H 6810 2460 60  0001 C CNN
F 3 "" H 6810 2460 60  0000 C CNN
F 4 "QS5K2CT-ND" H 6808 2760 60  0001 C CNN "DigikeyPN"
	1    6810 2460
	0    1    1    0   
$EndComp
$Comp
L 1k-BE R8
U 1 1 59982FDE
P 4460 2410
F 0 "R8" H 4460 2606 60  0000 C CNN
F 1 "2k-BF" H 4360 2510 60  0000 C CNN
F 2 "Resistors:R0603" H 4460 2410 60  0001 C CNN
F 3 "" H 4560 2560 60  0000 C CNN
F 4 "RMCF0603JT2K00TR-ND" H 4478 2678 60  0001 C CNN "DigikeyPN"
F 5 "1%" H 4660 2510 60  0000 C CNN "Precision"
	1    4460 2410
	1    0    0    -1  
$EndComp
$Comp
L 1k-BE R9
U 1 1 599831C0
P 4460 3010
F 0 "R9" H 4460 3206 60  0000 C CNN
F 1 "2k-BF" H 4360 3110 60  0000 C CNN
F 2 "Resistors:R0603" H 4460 3010 60  0001 C CNN
F 3 "" H 4560 3160 60  0000 C CNN
F 4 "RMCF0603JT2K00TR-ND" H 4478 3278 60  0001 C CNN "DigikeyPN"
F 5 "1%" H 4660 3110 60  0000 C CNN "Precision"
	1    4460 3010
	1    0    0    -1  
$EndComp
$Comp
L 1k-BE R4
U 1 1 59983503
P 4450 3310
F 0 "R4" H 4450 3506 60  0000 C CNN
F 1 "2k-BF" H 4350 3410 60  0000 C CNN
F 2 "Resistors:R0603" H 4450 3310 60  0001 C CNN
F 3 "" H 4550 3460 60  0000 C CNN
F 4 "RMCF0603JT2K00TR-ND" H 4468 3578 60  0001 C CNN "DigikeyPN"
F 5 "1%" H 4650 3410 60  0000 C CNN "Precision"
	1    4450 3310
	1    0    0    -1  
$EndComp
$Comp
L 1k-BE R5
U 1 1 5998382B
P 4450 4380
F 0 "R5" H 4450 4576 60  0000 C CNN
F 1 "2k-BF" H 4350 4480 60  0000 C CNN
F 2 "Resistors:R0603" H 4450 4380 60  0001 C CNN
F 3 "" H 4550 4530 60  0000 C CNN
F 4 "RMCF0603JT2K00TR-ND" H 4468 4648 60  0001 C CNN "DigikeyPN"
F 5 "1%" H 4650 4480 60  0000 C CNN "Precision"
	1    4450 4380
	1    0    0    -1  
$EndComp
$Comp
L 1k-BE R2
U 1 1 59983C01
P 4440 4680
F 0 "R2" H 4440 4876 60  0000 C CNN
F 1 "2k-BF" H 4340 4780 60  0000 C CNN
F 2 "Resistors:R0603" H 4440 4680 60  0001 C CNN
F 3 "" H 4540 4830 60  0000 C CNN
F 4 "RMCF0603JT2K00TR-ND" H 4458 4948 60  0001 C CNN "DigikeyPN"
F 5 "1%" H 4640 4780 60  0000 C CNN "Precision"
	1    4440 4680
	1    0    0    -1  
$EndComp
$Comp
L 1k-BE R6
U 1 1 59983F59
P 4450 5280
F 0 "R6" H 4450 5476 60  0000 C CNN
F 1 "2k-BF" H 4350 5380 60  0000 C CNN
F 2 "Resistors:R0603" H 4450 5280 60  0001 C CNN
F 3 "" H 4550 5430 60  0000 C CNN
F 4 "RMCF0603JT2K00TR-ND" H 4468 5548 60  0001 C CNN "DigikeyPN"
F 5 "1%" H 4650 5380 60  0000 C CNN "Precision"
	1    4450 5280
	1    0    0    -1  
$EndComp
$Comp
L 1k-BE R3
U 1 1 5998430F
P 4440 5580
F 0 "R3" H 4440 5776 60  0000 C CNN
F 1 "2k-BF" H 4340 5680 60  0000 C CNN
F 2 "Resistors:R0603" H 4440 5580 60  0001 C CNN
F 3 "" H 4540 5730 60  0000 C CNN
F 4 "RMCF0603JT2K00TR-ND" H 4458 5848 60  0001 C CNN "DigikeyPN"
F 5 "1%" H 4640 5680 60  0000 C CNN "Precision"
	1    4440 5580
	1    0    0    -1  
$EndComp
$Comp
L 1k-BE R17
U 1 1 59984781
P 7530 5540
F 0 "R17" H 7530 5736 60  0000 C CNN
F 1 "2k-BF" H 7430 5640 60  0000 C CNN
F 2 "Resistors:R0603" H 7530 5540 60  0001 C CNN
F 3 "" H 7630 5690 60  0000 C CNN
F 4 "RMCF0603JT2K00TR-ND" H 7548 5808 60  0001 C CNN "DigikeyPN"
F 5 "1%" H 7730 5640 60  0000 C CNN "Precision"
	1    7530 5540
	1    0    0    -1  
$EndComp
$Comp
L 1k-BE R16
U 1 1 59984D21
P 7530 5240
F 0 "R16" H 7530 5436 60  0000 C CNN
F 1 "2k-BF" H 7430 5340 60  0000 C CNN
F 2 "Resistors:R0603" H 7530 5240 60  0001 C CNN
F 3 "" H 7630 5390 60  0000 C CNN
F 4 "RMCF0603JT2K00TR-ND" H 7548 5508 60  0001 C CNN "DigikeyPN"
F 5 "1%" H 7730 5340 60  0000 C CNN "Precision"
	1    7530 5240
	1    0    0    -1  
$EndComp
$Comp
L 1k-BE R15
U 1 1 5998509D
P 7520 4640
F 0 "R15" H 7520 4836 60  0000 C CNN
F 1 "2k-BF" H 7420 4740 60  0000 C CNN
F 2 "Resistors:R0603" H 7520 4640 60  0001 C CNN
F 3 "" H 7620 4790 60  0000 C CNN
F 4 "RMCF0603JT2K00TR-ND" H 7538 4908 60  0001 C CNN "DigikeyPN"
F 5 "1%" H 7720 4740 60  0000 C CNN "Precision"
	1    7520 4640
	1    0    0    -1  
$EndComp
$Comp
L 1k-BE R14
U 1 1 59985513
P 7520 4340
F 0 "R14" H 7520 4536 60  0000 C CNN
F 1 "2k-BF" H 7420 4440 60  0000 C CNN
F 2 "Resistors:R0603" H 7520 4340 60  0001 C CNN
F 3 "" H 7620 4490 60  0000 C CNN
F 4 "RMCF0603JT2K00TR-ND" H 7538 4608 60  0001 C CNN "DigikeyPN"
F 5 "1%" H 7720 4440 60  0000 C CNN "Precision"
	1    7520 4340
	1    0    0    -1  
$EndComp
$Comp
L 1k-BE R11
U 1 1 599858B5
P 7510 3290
F 0 "R11" H 7510 3486 60  0000 C CNN
F 1 "2k-BF" H 7410 3390 60  0000 C CNN
F 2 "Resistors:R0603" H 7510 3290 60  0001 C CNN
F 3 "" H 7610 3440 60  0000 C CNN
F 4 "RMCF0603JT2K00TR-ND" H 7528 3558 60  0001 C CNN "DigikeyPN"
F 5 "1%" H 7710 3390 60  0000 C CNN "Precision"
	1    7510 3290
	1    0    0    -1  
$EndComp
$Comp
L 1k-BE R10
U 1 1 59985DF6
P 7510 2990
F 0 "R10" H 7510 3186 60  0000 C CNN
F 1 "2k-BF" H 7410 3090 60  0000 C CNN
F 2 "Resistors:R0603" H 7510 2990 60  0001 C CNN
F 3 "" H 7610 3140 60  0000 C CNN
F 4 "RMCF0603JT2K00TR-ND" H 7528 3258 60  0001 C CNN "DigikeyPN"
F 5 "1%" H 7710 3090 60  0000 C CNN "Precision"
	1    7510 2990
	1    0    0    -1  
$EndComp
$Comp
L 1k-BE R13
U 1 1 59986314
P 7520 2390
F 0 "R13" H 7520 2586 60  0000 C CNN
F 1 "2k-BF" H 7420 2490 60  0000 C CNN
F 2 "Resistors:R0603" H 7520 2390 60  0001 C CNN
F 3 "" H 7620 2540 60  0000 C CNN
F 4 "RMCF0603JT2K00TR-ND" H 7538 2658 60  0001 C CNN "DigikeyPN"
F 5 "1%" H 7720 2490 60  0000 C CNN "Precision"
	1    7520 2390
	1    0    0    -1  
$EndComp
$Comp
L 1k-BE R12
U 1 1 5998676D
P 7520 2090
F 0 "R12" H 7520 2286 60  0000 C CNN
F 1 "2k-BF" H 7420 2190 60  0000 C CNN
F 2 "Resistors:R0603" H 7520 2090 60  0001 C CNN
F 3 "" H 7620 2240 60  0000 C CNN
F 4 "RMCF0603JT2K00TR-ND" H 7538 2358 60  0001 C CNN "DigikeyPN"
F 5 "1%" H 7720 2190 60  0000 C CNN "Precision"
	1    7520 2090
	1    0    0    -1  
$EndComp
$EndSCHEMATC
