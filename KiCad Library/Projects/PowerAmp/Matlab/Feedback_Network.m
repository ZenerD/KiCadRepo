clear all; close all; clc;
f = logspace(-1,9,5000);
w = f*2*pi;
s = 1i*w;

r2 = 300;
r1 = 2700;
c1 = 6e-12;

filt = r2./(r1*r2*c1*s+r1+r2); %transfer function of feedback network
gain = 20*log10(abs(filt));
cutoff = max(gain)-3;
cutoff_freq = f(find(gain <= cutoff,1,'first'));
disp(['Cutoff Frequency = ',num2str(cutoff_freq/1e6,4),'MHz']);

subplot(2,1,1)
semilogx(f,gain,'b');
hold on
semilogx(f,ones(1,length(gain))*cutoff,'r');
grid on
xlim([f(1) f(end)]);
xlabel('Frequency (Hz)');
ylabel('Gain (dB)');

subplot(2,1,2)
semilogx(f,unwrap(angle(filt))*180/pi)
grid on
xlim([f(1) f(end)]);
xlabel('Frequency (Hz)');
ylabel(['Phase (',char(176),')']);