///////May 29, 2018////////
1. Add switch to oen the 50ohm input resistor. Systems uses the original board in both 50ohm and high impedance mode.
2. Add the socket for the op-amp. If the op-amp does blow up then popping the op-amp out of the socket is 1000X easier than de-soldering it.
3. Mirror the footprint of the op-amp.
-Ben Leary