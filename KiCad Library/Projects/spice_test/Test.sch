EESchema Schematic File Version 2
LIBS:Capacitors
LIBS:Connectors
LIBS:Discretes
LIBS:ICs
LIBS:Allegro
LIBS:Resistors
LIBS:TestPoints
LIBS:Test-cache
EELAYER 26 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L DXT2012P5 Q?
U 1 1 595AF14A
P 5500 3900
F 0 "Q?" H 5541 3953 60  0000 L CNN
F 1 "DXT2012P5" H 5541 3847 60  0000 L CNN
F 2 "" H 5500 3900 60  0001 C CNN
F 3 "" H 5500 3900 60  0001 C CNN
F 4 "Q" H 5850 4450 60  0001 C CNN "Spice_Primitive"
F 5 "DXT2012P5" H 4750 4400 60  0001 C CNN "Spice_Model"
F 6 "Y" H 6050 4450 60  0001 C CNN "Spice_Netlist_Enabled"
F 7 "..\\..\\Parts\\Discrete\\Spice\\DXT2012P5.spice" H 5300 4600 60  0001 C CNN "Spice_Lib_File"
	1    5500 3900
	1    0    0    -1  
$EndComp
$EndSCHEMATC
