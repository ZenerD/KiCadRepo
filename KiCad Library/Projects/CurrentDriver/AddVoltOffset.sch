EESchema Schematic File Version 2
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:Capacitors
LIBS:Connectors
LIBS:Discretes
LIBS:ICs
LIBS:Inductors
LIBS:Allegro
LIBS:Resistors
LIBS:TestPoints
LIBS:Current_Driver-cache
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 4 4
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L 10k-CD R?
U 1 1 5DC29A6F
P 7600 3300
F 0 "R?" H 7600 3496 60  0000 C CNN
F 1 "10k-CD" H 7500 3400 60  0000 C CNN
F 2 "Resistors:R0805" H 7600 3300 60  0001 C CNN
F 3 "" H 7700 3450 60  0000 C CNN
F 4 "A103142CT-ND" H 7618 3568 60  0001 C CNN "DigikeyPN"
F 5 "0.1%" H 7850 3400 60  0000 C CNN "Precision"
	1    7600 3300
	-1   0    0    1   
$EndComp
$Comp
L 10k-CD R?
U 1 1 5DC29A78
P 7600 2450
F 0 "R?" H 7600 2646 60  0000 C CNN
F 1 "10k-CD" H 7500 2550 60  0000 C CNN
F 2 "Resistors:R0805" H 7600 2450 60  0001 C CNN
F 3 "" H 7700 2600 60  0000 C CNN
F 4 "A103142CT-ND" H 7618 2718 60  0001 C CNN "DigikeyPN"
F 5 "0.1%" H 7850 2550 60  0000 C CNN "Precision"
	1    7600 2450
	-1   0    0    1   
$EndComp
$Comp
L Bare_TP Vout_TP
U 1 1 5DC29A7F
P 8050 2450
F 0 "Vout_TP" H 8200 2700 60  0000 C CNN
F 1 "Bare_TP" H 7900 2700 60  0000 C CNN
F 2 "" H 8050 2450 60  0000 C CNN
F 3 "" H 8050 2450 60  0000 C CNN
	1    8050 2450
	1    0    0    -1  
$EndComp
Wire Wire Line
	8050 3300 7800 3300
Wire Wire Line
	7400 3300 7150 3300
Wire Wire Line
	8050 2450 8050 3300
$Comp
L LT1001ACN8 U?
U 1 1 5DC29A8B
P 3500 2450
F 0 "U?" H 3850 2250 60  0000 C CNN
F 1 "LT1001ACN8" H 4100 2650 60  0000 C CNN
F 2 "" H 3850 2250 60  0001 C CNN
F 3 "" H 3850 2250 60  0001 C CNN
	1    3500 2450
	1    0    0    -1  
$EndComp
Wire Wire Line
	3750 2100 3750 2000
Wire Wire Line
	3750 2800 3750 2900
Text HLabel 4000 2000 2    60   Input ~ 0
V+
Text HLabel 4000 2900 2    60   Input ~ 0
V-
Text HLabel 8700 2450 2    60   Output ~ 0
Vout
Wire Wire Line
	3750 2000 4000 2000
Wire Wire Line
	3750 2900 4000 2900
NoConn ~ 3600 2800
NoConn ~ 3500 2800
Wire Wire Line
	4400 2450 7400 2450
Wire Wire Line
	7800 2450 8700 2450
Connection ~ 8050 2450
Text HLabel 7150 3300 0    60   Input ~ 0
Voffset
Text HLabel 2700 2300 0    60   Input ~ 0
VoltAddOffset
Wire Wire Line
	3000 2300 2700 2300
$Comp
L 10k-CD R?
U 1 1 5DC2B1D5
P 4700 2950
F 0 "R?" H 4700 3146 60  0000 C CNN
F 1 "10k-CD" H 4600 3050 60  0000 C CNN
F 2 "Resistors:R0805" H 4700 2950 60  0001 C CNN
F 3 "" H 4800 3100 60  0000 C CNN
F 4 "A103142CT-ND" H 4718 3218 60  0001 C CNN "DigikeyPN"
F 5 "0.1%" H 4950 3050 60  0000 C CNN "Precision"
	1    4700 2950
	0    -1   -1   0   
$EndComp
$Comp
L 10k-CD R?
U 1 1 5DC2B26A
P 4700 3800
F 0 "R?" H 4700 3996 60  0000 C CNN
F 1 "10k-CD" H 4600 3900 60  0000 C CNN
F 2 "Resistors:R0805" H 4700 3800 60  0001 C CNN
F 3 "" H 4800 3950 60  0000 C CNN
F 4 "A103142CT-ND" H 4718 4068 60  0001 C CNN "DigikeyPN"
F 5 "0.1%" H 4950 3900 60  0000 C CNN "Precision"
	1    4700 3800
	0    -1   -1   0   
$EndComp
$Comp
L GND #PWR?
U 1 1 5DC2B2F4
P 4700 4100
F 0 "#PWR?" H 4700 3850 50  0001 C CNN
F 1 "GND" H 4700 3950 50  0000 C CNN
F 2 "" H 4700 4100 50  0001 C CNN
F 3 "" H 4700 4100 50  0001 C CNN
	1    4700 4100
	1    0    0    -1  
$EndComp
Wire Wire Line
	4700 3150 4700 3600
Wire Wire Line
	4700 4100 4700 4000
Wire Wire Line
	4700 2750 4700 2450
Wire Wire Line
	3000 2600 2900 2600
Wire Wire Line
	2900 2600 2900 3350
Wire Wire Line
	2900 3350 4700 3350
Connection ~ 4700 3350
Connection ~ 4700 2450
Text Notes 3050 1750 0    60   ~ 0
Gain = x2 \n\nThis is to overcome the /2\nfound in the summing node
Text Notes 7350 1750 0    60   ~ 0
Add the -5V offset\n\nUsing KCL we get\n[V1 + Voffset] / 2
$EndSCHEMATC
