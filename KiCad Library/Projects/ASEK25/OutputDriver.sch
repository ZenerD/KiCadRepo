EESchema Schematic File Version 2
LIBS:ICs
LIBS:Resistors
LIBS:ASEK25-cache
EELAYER 24 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 2 2
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L OPA552 U?
U 1 1 5485252F
P 5850 3500
F 0 "U?" H 6150 3850 60  0000 C CNN
F 1 "OPA552" H 6100 3950 60  0000 C CNN
F 2 "" H 5850 3500 60  0000 C CNN
F 3 "" H 5850 3500 60  0000 C CNN
	1    5850 3500
	1    0    0    -1  
$EndComp
Text HLabel 1200 650  0    60   Input ~ 0
Analog_35v0
Text HLabel 1200 750  0    60   Input ~ 0
Analog_n23v0
Text HLabel 1200 850  0    60   Input ~ 0
Analog_5v0
Text HLabel 1200 950  0    60   Input ~ 0
Analog_GND
Text HLabel 1200 1050 0    60   Input ~ 0
Current_Limit
Text HLabel 1200 1150 0    60   Input ~ 0
Output_Voltage
Text HLabel 10800 600  2    60   Output ~ 0
Ouput
$Comp
L Value R?
U 1 1 548D03D3
P 5000 3350
F 0 "R?" H 5000 3250 60  0000 C CNN
F 1 "10k" H 5000 3450 60  0000 C CNN
F 2 "" H 5000 3350 60  0000 C CNN
F 3 "" H 5000 3350 60  0000 C CNN
	1    5000 3350
	1    0    0    -1  
$EndComp
$Comp
L Value R?
U 1 1 548D0408
P 5000 3650
F 0 "R?" H 5000 3550 60  0000 C CNN
F 1 "10k" H 5000 3750 60  0000 C CNN
F 2 "" H 5000 3650 60  0000 C CNN
F 3 "" H 5000 3650 60  0000 C CNN
	1    5000 3650
	1    0    0    -1  
$EndComp
$Comp
L Value R?
U 1 1 548D0433
P 5350 3050
F 0 "R?" H 5350 2950 60  0000 C CNN
F 1 "157k" H 5350 3150 60  0000 C CNN
F 2 "" H 5350 3050 60  0000 C CNN
F 3 "" H 5350 3050 60  0000 C CNN
	1    5350 3050
	0    -1   -1   0   
$EndComp
$Comp
L Value R?
U 1 1 548D045E
P 5800 4200
F 0 "R?" H 5800 4100 60  0000 C CNN
F 1 "150k" H 5800 4300 60  0000 C CNN
F 2 "" H 5800 4200 60  0000 C CNN
F 3 "" H 5800 4200 60  0000 C CNN
	1    5800 4200
	1    0    0    -1  
$EndComp
Wire Wire Line
	5250 3650 5400 3650
Wire Wire Line
	5400 3650 5400 4200
Wire Wire Line
	5400 4200 5600 4200
Wire Wire Line
	6050 4200 6450 4200
Wire Wire Line
	6450 4200 6450 3500
Wire Wire Line
	6450 3500 6300 3500
Wire Wire Line
	5250 3350 5400 3350
Wire Wire Line
	5350 3250 5350 3350
Connection ~ 5350 3350
Wire Wire Line
	5350 2250 5350 2800
Text Label 5350 2750 1    60   ~ 0
Analog_gnd
Wire Wire Line
	1200 850  1800 850 
Wire Wire Line
	4100 3350 4800 3350
Text Label 4100 3350 0    60   ~ 0
3p3V_reference
Wire Wire Line
	4800 3650 4100 3650
Text Label 4100 3650 0    60   ~ 0
Input_reference
Wire Wire Line
	5800 3750 5800 3850
Wire Wire Line
	5800 3850 6300 3850
Text Label 5800 3850 0    60   ~ 0
Analog_n23V
Wire Wire Line
	5800 2700 5800 3250
Text Label 5800 3200 1    60   ~ 0
Analog_35V
Text Label 1300 650  0    60   ~ 0
Analog_35V
Text Label 1250 750  0    60   ~ 0
Analog_n23V
Wire Wire Line
	1200 750  1800 750 
Wire Wire Line
	1200 650  1800 650 
Text Label 1300 950  0    60   ~ 0
Analog_gnd
Wire Wire Line
	1200 950  1800 950 
$EndSCHEMATC
