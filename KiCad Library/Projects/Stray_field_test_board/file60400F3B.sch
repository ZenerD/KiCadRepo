EESchema Schematic File Version 4
LIBS:stray_field_test_board-cache
EELAYER 26 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 2 6
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Device:C C?
U 1 1 602412FF
P 3050 3900
AR Path="/602412FF" Ref="C?"  Part="1" 
AR Path="/60400F3C/602412FF" Ref="C2"  Part="1" 
F 0 "C2" H 3165 3946 50  0000 L CNN
F 1 ".1u-FC" H 3165 3855 50  0000 L CNN
F 2 "Capacitors:radial_3mm_cer" H 3088 3750 50  0001 C CNN
F 3 "~" H 3050 3900 50  0001 C CNN
	1    3050 3900
	1    0    0    -1  
$EndComp
$Comp
L Device:CP C?
U 1 1 60241306
P 3950 3900
AR Path="/60241306" Ref="C?"  Part="1" 
AR Path="/60400F3C/60241306" Ref="C3"  Part="1" 
F 0 "C3" H 4068 3946 50  0000 L CNN
F 1 "1u-FI" H 4068 3855 50  0000 L CNN
F 2 "Capacitors:Radial_3mm_pol" H 3988 3750 50  0001 C CNN
F 3 "~" H 3950 3900 50  0001 C CNN
	1    3950 3900
	1    0    0    -1  
$EndComp
$Comp
L Device:R_US R?
U 1 1 60241314
P 3350 4100
AR Path="/60241314" Ref="R?"  Part="1" 
AR Path="/60400F3C/60241314" Ref="R5"  Part="1" 
F 0 "R5" H 3418 4146 50  0000 L CNN
F 1 "1.1K-FE-dnp" H 3418 4055 50  0000 L CNN
F 2 "Resistors:R-thru" V 3390 4090 50  0001 C CNN
F 3 "~" H 3350 4100 50  0001 C CNN
	1    3350 4100
	1    0    0    -1  
$EndComp
Wire Wire Line
	3650 3450 3750 3450
Wire Wire Line
	3750 3550 3750 3450
Connection ~ 3750 3450
Wire Wire Line
	3950 3750 3950 3450
Wire Wire Line
	3750 3850 3350 3850
Wire Wire Line
	3350 3850 3350 3750
Wire Wire Line
	3350 3950 3350 3850
Connection ~ 3350 3850
Wire Wire Line
	3050 3750 3050 3450
Connection ~ 3050 3450
Wire Wire Line
	3050 4050 3050 4250
Wire Wire Line
	3350 4250 3350 4400
Wire Wire Line
	3050 4250 3350 4250
Connection ~ 3350 4250
Wire Wire Line
	3950 4050 3950 4250
Wire Wire Line
	3950 4250 3600 4250
$Comp
L power:GND #PWR?
U 1 1 6024132E
P 3350 4400
AR Path="/6024132E" Ref="#PWR?"  Part="1" 
AR Path="/60400F3C/6024132E" Ref="#PWR08"  Part="1" 
F 0 "#PWR08" H 3350 4150 50  0001 C CNN
F 1 "GND" H 3355 4227 50  0000 C CNN
F 2 "" H 3350 4400 50  0001 C CNN
F 3 "" H 3350 4400 50  0001 C CNN
	1    3350 4400
	1    0    0    -1  
$EndComp
$Comp
L Device:R_US R?
U 1 1 60241349
P 4900 4100
AR Path="/60241349" Ref="R?"  Part="1" 
AR Path="/60400F3C/60241349" Ref="R8"  Part="1" 
F 0 "R8" H 4968 4146 50  0000 L CNN
F 1 "1.49K-FE-dnp" H 4968 4055 50  0000 L CNN
F 2 "Resistors:R-thru" V 4940 4090 50  0001 C CNN
F 3 "~" H 4900 4100 50  0001 C CNN
	1    4900 4100
	1    0    0    -1  
$EndComp
Wire Wire Line
	5200 3450 5300 3450
Wire Wire Line
	5300 3550 5300 3450
Connection ~ 5300 3450
Wire Wire Line
	5500 3750 5500 3450
Wire Wire Line
	5300 3850 4900 3850
Wire Wire Line
	4900 3850 4900 3750
Wire Wire Line
	4900 3950 4900 3850
Connection ~ 4900 3850
Wire Wire Line
	4600 4050 4600 4250
Wire Wire Line
	4600 4250 4900 4250
Connection ~ 4900 4250
Wire Wire Line
	5500 4050 5500 4250
Wire Wire Line
	5500 4250 5150 4250
$Comp
L Device:R_US R?
U 1 1 6024137E
P 6450 4100
AR Path="/6024137E" Ref="R?"  Part="1" 
AR Path="/60400F3C/6024137E" Ref="R11"  Part="1" 
F 0 "R11" H 6518 4146 50  0000 L CNN
F 1 "2.3K-FE-dnp" H 6518 4055 50  0000 L CNN
F 2 "Resistors:R-thru" V 6490 4090 50  0001 C CNN
F 3 "~" H 6450 4100 50  0001 C CNN
	1    6450 4100
	1    0    0    -1  
$EndComp
Wire Wire Line
	6750 3450 6850 3450
Wire Wire Line
	6850 3550 6850 3450
Connection ~ 6850 3450
Wire Wire Line
	7050 3750 7050 3450
Wire Wire Line
	6850 3850 6450 3850
Wire Wire Line
	6450 3850 6450 3750
Wire Wire Line
	6450 3950 6450 3850
Connection ~ 6450 3850
Wire Wire Line
	6150 4050 6150 4250
Wire Wire Line
	6150 4250 6450 4250
Connection ~ 6450 4250
Wire Wire Line
	7050 4050 7050 4250
Wire Wire Line
	7050 4250 6700 4250
$Comp
L power:GND #PWR?
U 1 1 60241398
P 6450 5350
AR Path="/60241398" Ref="#PWR?"  Part="1" 
AR Path="/60400F3C/60241398" Ref="#PWR010"  Part="1" 
F 0 "#PWR010" H 6450 5100 50  0001 C CNN
F 1 "GND" H 6455 5177 50  0000 C CNN
F 2 "" H 6450 5350 50  0001 C CNN
F 3 "" H 6450 5350 50  0001 C CNN
	1    6450 5350
	1    0    0    -1  
$EndComp
$Comp
L Device:R_US R?
U 1 1 602413AC
P 8450 3700
AR Path="/602413AC" Ref="R?"  Part="1" 
AR Path="/60400F3C/602413AC" Ref="R15"  Part="1" 
F 0 "R15" H 8518 3746 50  0000 L CNN
F 1 "240-CC" H 8518 3655 50  0000 L CNN
F 2 "Resistors:R0805" V 8490 3690 50  0001 C CNN
F 3 "~" H 8450 3700 50  0001 C CNN
	1    8450 3700
	1    0    0    -1  
$EndComp
$Comp
L Device:R_US R?
U 1 1 602413B3
P 8050 4100
AR Path="/602413B3" Ref="R?"  Part="1" 
AR Path="/60400F3C/602413B3" Ref="R14"  Part="1" 
F 0 "R14" H 8118 4146 50  0000 L CNN
F 1 "4.5K-FE-dnp" H 8118 4055 50  0000 L CNN
F 2 "Resistors:R-thru" V 8090 4090 50  0001 C CNN
F 3 "~" H 8050 4100 50  0001 C CNN
	1    8050 4100
	1    0    0    -1  
$EndComp
Wire Wire Line
	8350 3450 8450 3450
Wire Wire Line
	8450 3550 8450 3450
Connection ~ 8450 3450
Wire Wire Line
	8650 3750 8650 3450
Wire Wire Line
	8450 3850 8050 3850
Wire Wire Line
	8050 3850 8050 3750
Wire Wire Line
	8050 3950 8050 3850
Connection ~ 8050 3850
Wire Wire Line
	7750 4050 7750 4250
Wire Wire Line
	7750 4250 8050 4250
Connection ~ 8050 4250
Wire Wire Line
	8650 4050 8650 4250
Wire Wire Line
	8650 4250 8300 4250
Wire Wire Line
	6150 3150 4600 3150
Connection ~ 4600 3150
Wire Wire Line
	4600 3150 4600 3000
$Comp
L Regulator_Linear:LM317L_TO92 U?
U 1 1 602413F4
P 3350 3450
AR Path="/602413F4" Ref="U?"  Part="1" 
AR Path="/60400F3C/602413F4" Ref="U5"  Part="1" 
F 0 "U5" H 3350 3692 50  0000 C CNN
F 1 "LM317HV" H 3350 3601 50  0000 C CNN
F 2 "Discretes:TO-220" H 3350 3675 50  0001 C CIN
F 3 "" H 3350 3450 50  0001 C CNN
	1    3350 3450
	1    0    0    -1  
$EndComp
Text HLabel 8650 3450 2    50   Output ~ 0
UV_supply
Text HLabel 7050 3450 2    50   Output ~ 0
HV_supply
Text HLabel 5500 3450 2    50   Output ~ 0
MV_supply
Text HLabel 4050 3450 2    50   Output ~ 0
6V_supply
Text HLabel 4450 3000 0    50   Input ~ 0
Batt_18V
Text HLabel 7750 3000 0    50   Input ~ 0
Batt_36V
Text HLabel 3050 4250 0    50   Input ~ 0
GND
Text HLabel 2900 3000 0    50   Input ~ 0
Batt_9V
Wire Wire Line
	3050 3000 3050 3450
$Comp
L Device:R_POT_TRIM_US RV1
U 1 1 602847E2
P 3750 4250
F 0 "RV1" H 3683 4204 50  0000 R CNN
F 1 "10k_trim" H 3683 4295 50  0000 R CNN
F 2 "Resistors:3299W_trim_pot" H 3750 4250 50  0001 C CNN
F 3 "~" H 3750 4250 50  0001 C CNN
	1    3750 4250
	-1   0    0    -1  
$EndComp
$Comp
L Device:R_POT_TRIM_US RV2
U 1 1 60284935
P 5300 4250
F 0 "RV2" H 5233 4204 50  0000 R CNN
F 1 "10k_trim" H 5233 4295 50  0000 R CNN
F 2 "Resistors:3299W_trim_pot" H 5300 4250 50  0001 C CNN
F 3 "~" H 5300 4250 50  0001 C CNN
	1    5300 4250
	-1   0    0    -1  
$EndComp
Connection ~ 5150 4250
Wire Wire Line
	5150 4250 4900 4250
$Comp
L Device:R_POT_TRIM_US RV3
U 1 1 60284991
P 6850 4250
F 0 "RV3" H 6783 4204 50  0000 R CNN
F 1 "10k_trim" H 6783 4295 50  0000 R CNN
F 2 "Resistors:3299W_trim_pot" H 6850 4250 50  0001 C CNN
F 3 "~" H 6850 4250 50  0001 C CNN
	1    6850 4250
	-1   0    0    -1  
$EndComp
Connection ~ 6700 4250
Wire Wire Line
	6700 4250 6450 4250
$Comp
L Device:R_POT_TRIM_US RV4
U 1 1 60284A70
P 8450 4250
F 0 "RV4" H 8383 4204 50  0000 R CNN
F 1 "10k_trim" H 8383 4295 50  0000 R CNN
F 2 "Resistors:3299W_trim_pot" H 8450 4250 50  0001 C CNN
F 3 "~" H 8450 4250 50  0001 C CNN
	1    8450 4250
	-1   0    0    -1  
$EndComp
Connection ~ 8300 4250
Wire Wire Line
	8300 4250 8050 4250
Wire Wire Line
	8450 4100 8450 3850
Connection ~ 8450 3850
Wire Wire Line
	6850 4100 6850 3850
Connection ~ 6850 3850
Connection ~ 3600 4250
Wire Wire Line
	3600 4250 3350 4250
Wire Wire Line
	3750 4100 3750 3850
Connection ~ 3750 3850
Wire Wire Line
	5300 4100 5300 3850
Connection ~ 5300 3850
$Comp
L Device:C C?
U 1 1 60293810
P 2850 3900
AR Path="/60293810" Ref="C?"  Part="1" 
AR Path="/60400F3C/60293810" Ref="C1"  Part="1" 
F 0 "C1" H 2965 3946 50  0000 L CNN
F 1 ".1u-CC" H 2965 3855 50  0000 L CNN
F 2 "Capacitors:C0805" H 2888 3750 50  0001 C CNN
F 3 "~" H 2850 3900 50  0001 C CNN
	1    2850 3900
	1    0    0    -1  
$EndComp
Connection ~ 3950 3450
Wire Wire Line
	3950 3450 4050 3450
Wire Wire Line
	4600 3000 4450 3000
$Comp
L Device:CP C?
U 1 1 602A32DB
P 4150 3900
AR Path="/602A32DB" Ref="C?"  Part="1" 
AR Path="/60400F3C/602A32DB" Ref="C4"  Part="1" 
F 0 "C4" H 4268 3946 50  0000 L CNN
F 1 "1u-FH" H 4268 3855 50  0000 L CNN
F 2 "Capacitors:SMD_4mm_pol" H 4188 3750 50  0001 C CNN
F 3 "~" H 4150 3900 50  0001 C CNN
	1    4150 3900
	1    0    0    -1  
$EndComp
Wire Wire Line
	2850 4050 3050 4050
Wire Wire Line
	2850 3750 3050 3750
Wire Wire Line
	4150 3750 3950 3750
Connection ~ 3950 3750
Wire Wire Line
	4150 4050 3950 4050
Connection ~ 3950 4050
Wire Wire Line
	4400 4050 4600 4050
Wire Wire Line
	4400 3750 4600 3750
Wire Wire Line
	5700 3750 5500 3750
Wire Wire Line
	5700 4050 5500 4050
Wire Wire Line
	5950 4050 6150 4050
Wire Wire Line
	5950 3750 6150 3750
Wire Wire Line
	7250 3750 7050 3750
Wire Wire Line
	7250 4050 7050 4050
Wire Wire Line
	7550 3750 7750 3750
Wire Wire Line
	7550 4050 7750 4050
Wire Wire Line
	8850 3750 8650 3750
Wire Wire Line
	8850 4050 8650 4050
NoConn ~ 8450 4400
NoConn ~ 6850 4400
NoConn ~ 5300 4400
NoConn ~ 3750 4400
$Comp
L Device:CP C?
U 1 1 602DEDA9
P 5500 3900
AR Path="/602DEDA9" Ref="C?"  Part="1" 
AR Path="/60400F3C/602DEDA9" Ref="C7"  Part="1" 
F 0 "C7" H 5618 3946 50  0000 L CNN
F 1 "1u-FI" H 5618 3855 50  0000 L CNN
F 2 "Capacitors:Radial_3mm_pol" H 5538 3750 50  0001 C CNN
F 3 "~" H 5500 3900 50  0001 C CNN
	1    5500 3900
	1    0    0    -1  
$EndComp
$Comp
L Device:CP C?
U 1 1 602DEDAF
P 5700 3900
AR Path="/602DEDAF" Ref="C?"  Part="1" 
AR Path="/60400F3C/602DEDAF" Ref="C8"  Part="1" 
F 0 "C8" H 5818 3946 50  0000 L CNN
F 1 "1u-FH" H 5818 3855 50  0000 L CNN
F 2 "Capacitors:SMD_4mm_pol" H 5738 3750 50  0001 C CNN
F 3 "~" H 5700 3900 50  0001 C CNN
	1    5700 3900
	1    0    0    -1  
$EndComp
Connection ~ 5500 3750
Connection ~ 5500 4050
$Comp
L Device:CP C?
U 1 1 602E3403
P 7050 3900
AR Path="/602E3403" Ref="C?"  Part="1" 
AR Path="/60400F3C/602E3403" Ref="C11"  Part="1" 
F 0 "C11" H 7168 3946 50  0000 L CNN
F 1 "1u-FI" H 7168 3855 50  0000 L CNN
F 2 "Capacitors:Radial_3mm_pol" H 7088 3750 50  0001 C CNN
F 3 "~" H 7050 3900 50  0001 C CNN
	1    7050 3900
	1    0    0    -1  
$EndComp
$Comp
L Device:CP C?
U 1 1 602E340A
P 7250 3900
AR Path="/602E340A" Ref="C?"  Part="1" 
AR Path="/60400F3C/602E340A" Ref="C12"  Part="1" 
F 0 "C12" H 7368 3946 50  0000 L CNN
F 1 "1u-FH" H 7368 3855 50  0000 L CNN
F 2 "Capacitors:SMD_4mm_pol" H 7288 3750 50  0001 C CNN
F 3 "~" H 7250 3900 50  0001 C CNN
	1    7250 3900
	1    0    0    -1  
$EndComp
$Comp
L Device:CP C?
U 1 1 602E7939
P 8650 3900
AR Path="/602E7939" Ref="C?"  Part="1" 
AR Path="/60400F3C/602E7939" Ref="C15"  Part="1" 
F 0 "C15" H 8768 3946 50  0000 L CNN
F 1 "1u-FI" H 8768 3855 50  0000 L CNN
F 2 "Capacitors:Radial_3mm_pol" H 8688 3750 50  0001 C CNN
F 3 "~" H 8650 3900 50  0001 C CNN
	1    8650 3900
	1    0    0    -1  
$EndComp
$Comp
L Device:CP C?
U 1 1 602E7940
P 8850 3900
AR Path="/602E7940" Ref="C?"  Part="1" 
AR Path="/60400F3C/602E7940" Ref="C16"  Part="1" 
F 0 "C16" H 8968 3946 50  0000 L CNN
F 1 "1u-FH" H 8968 3855 50  0000 L CNN
F 2 "Capacitors:SMD_4mm_pol" H 8888 3750 50  0001 C CNN
F 3 "~" H 8850 3900 50  0001 C CNN
	1    8850 3900
	1    0    0    -1  
$EndComp
Connection ~ 3050 3750
Connection ~ 3050 4050
$Comp
L Device:C C?
U 1 1 602F3439
P 4600 3900
AR Path="/602F3439" Ref="C?"  Part="1" 
AR Path="/60400F3C/602F3439" Ref="C6"  Part="1" 
F 0 "C6" H 4715 3946 50  0000 L CNN
F 1 ".1u-FC" H 4715 3855 50  0000 L CNN
F 2 "Capacitors:radial_3mm_cer" H 4638 3750 50  0001 C CNN
F 3 "~" H 4600 3900 50  0001 C CNN
	1    4600 3900
	1    0    0    -1  
$EndComp
$Comp
L Device:C C?
U 1 1 602F3440
P 4400 3900
AR Path="/602F3440" Ref="C?"  Part="1" 
AR Path="/60400F3C/602F3440" Ref="C5"  Part="1" 
F 0 "C5" H 4515 3946 50  0000 L CNN
F 1 ".1u-CC" H 4515 3855 50  0000 L CNN
F 2 "Capacitors:C0805" H 4438 3750 50  0001 C CNN
F 3 "~" H 4400 3900 50  0001 C CNN
	1    4400 3900
	1    0    0    -1  
$EndComp
$Comp
L Device:C C?
U 1 1 602F563A
P 6150 3900
AR Path="/602F563A" Ref="C?"  Part="1" 
AR Path="/60400F3C/602F563A" Ref="C10"  Part="1" 
F 0 "C10" H 6265 3946 50  0000 L CNN
F 1 ".1u-FC" H 6265 3855 50  0000 L CNN
F 2 "Capacitors:radial_3mm_cer" H 6188 3750 50  0001 C CNN
F 3 "~" H 6150 3900 50  0001 C CNN
	1    6150 3900
	1    0    0    -1  
$EndComp
$Comp
L Device:C C?
U 1 1 602F5641
P 5950 3900
AR Path="/602F5641" Ref="C?"  Part="1" 
AR Path="/60400F3C/602F5641" Ref="C9"  Part="1" 
F 0 "C9" H 6065 3946 50  0000 L CNN
F 1 ".1u-CC" H 6065 3855 50  0000 L CNN
F 2 "Capacitors:C0805" H 5988 3750 50  0001 C CNN
F 3 "~" H 5950 3900 50  0001 C CNN
	1    5950 3900
	1    0    0    -1  
$EndComp
$Comp
L Device:C C?
U 1 1 602F7851
P 7750 3900
AR Path="/602F7851" Ref="C?"  Part="1" 
AR Path="/60400F3C/602F7851" Ref="C14"  Part="1" 
F 0 "C14" H 7865 3946 50  0000 L CNN
F 1 ".1u-FC" H 7865 3855 50  0000 L CNN
F 2 "Capacitors:radial_3mm_cer" H 7788 3750 50  0001 C CNN
F 3 "~" H 7750 3900 50  0001 C CNN
	1    7750 3900
	1    0    0    -1  
$EndComp
$Comp
L Device:C C?
U 1 1 602F7858
P 7550 3900
AR Path="/602F7858" Ref="C?"  Part="1" 
AR Path="/60400F3C/602F7858" Ref="C13"  Part="1" 
F 0 "C13" H 7665 3946 50  0000 L CNN
F 1 ".1u-CC" H 7665 3855 50  0000 L CNN
F 2 "Capacitors:C0805" H 7588 3750 50  0001 C CNN
F 3 "~" H 7550 3900 50  0001 C CNN
	1    7550 3900
	1    0    0    -1  
$EndComp
Wire Wire Line
	4600 3150 4600 3450
Wire Wire Line
	6150 3150 6150 3450
Wire Wire Line
	7750 3000 7750 3450
$Comp
L Regulator_Linear:LM317L_TO92 U?
U 1 1 602FEADB
P 4900 3450
AR Path="/602FEADB" Ref="U?"  Part="1" 
AR Path="/60400F3C/602FEADB" Ref="U6"  Part="1" 
F 0 "U6" H 4900 3692 50  0000 C CNN
F 1 "LM317HV" H 4900 3601 50  0000 C CNN
F 2 "Discretes:TO-220" H 4900 3675 50  0001 C CIN
F 3 "" H 4900 3450 50  0001 C CNN
	1    4900 3450
	1    0    0    -1  
$EndComp
Connection ~ 4600 3450
Wire Wire Line
	4600 3450 4600 3750
$Comp
L Regulator_Linear:LM317L_TO92 U?
U 1 1 602FEB57
P 6450 3450
AR Path="/602FEB57" Ref="U?"  Part="1" 
AR Path="/60400F3C/602FEB57" Ref="U7"  Part="1" 
F 0 "U7" H 6450 3692 50  0000 C CNN
F 1 "LM317HV" H 6450 3601 50  0000 C CNN
F 2 "Discretes:TO-220" H 6450 3675 50  0001 C CIN
F 3 "" H 6450 3450 50  0001 C CNN
	1    6450 3450
	1    0    0    -1  
$EndComp
Connection ~ 6150 3450
Wire Wire Line
	6150 3450 6150 3750
$Comp
L Regulator_Linear:LM317L_TO92 U?
U 1 1 602FEBCB
P 8050 3450
AR Path="/602FEBCB" Ref="U?"  Part="1" 
AR Path="/60400F3C/602FEBCB" Ref="U8"  Part="1" 
F 0 "U8" H 8050 3692 50  0000 C CNN
F 1 "LM317HV" H 8050 3601 50  0000 C CNN
F 2 "Discretes:TO-220" H 8050 3675 50  0001 C CIN
F 3 "" H 8050 3450 50  0001 C CNN
	1    8050 3450
	1    0    0    -1  
$EndComp
Connection ~ 7750 3450
Wire Wire Line
	7750 3450 7750 3750
$Comp
L Device:R_US R?
U 1 1 602D5AF1
P 6850 3700
AR Path="/602D5AF1" Ref="R?"  Part="1" 
AR Path="/60400F3C/602D5AF1" Ref="R12"  Part="1" 
F 0 "R12" H 6918 3746 50  0000 L CNN
F 1 "240-CC" H 6918 3655 50  0000 L CNN
F 2 "Resistors:R0805" V 6890 3690 50  0001 C CNN
F 3 "~" H 6850 3700 50  0001 C CNN
	1    6850 3700
	1    0    0    -1  
$EndComp
$Comp
L Device:R_US R?
U 1 1 602D5B57
P 5300 3700
AR Path="/602D5B57" Ref="R?"  Part="1" 
AR Path="/60400F3C/602D5B57" Ref="R9"  Part="1" 
F 0 "R9" H 5368 3746 50  0000 L CNN
F 1 "240-CC" H 5368 3655 50  0000 L CNN
F 2 "Resistors:R0805" V 5340 3690 50  0001 C CNN
F 3 "~" H 5300 3700 50  0001 C CNN
	1    5300 3700
	1    0    0    -1  
$EndComp
$Comp
L Device:R_US R?
U 1 1 602D5C56
P 3750 3700
AR Path="/602D5C56" Ref="R?"  Part="1" 
AR Path="/60400F3C/602D5C56" Ref="R6"  Part="1" 
F 0 "R6" H 3818 3746 50  0000 L CNN
F 1 "240-CC" H 3818 3655 50  0000 L CNN
F 2 "Resistors:R0805" V 3790 3690 50  0001 C CNN
F 3 "~" H 3750 3700 50  0001 C CNN
	1    3750 3700
	1    0    0    -1  
$EndComp
Connection ~ 7750 4250
Wire Wire Line
	3750 3450 3950 3450
Wire Wire Line
	5300 3450 5500 3450
Wire Wire Line
	6850 3450 7050 3450
Wire Wire Line
	8450 3450 8650 3450
$Comp
L Transistor_FET:BS170 Q?
U 1 1 602F2578
P 6350 5000
AR Path="/602F2578" Ref="Q?"  Part="1" 
AR Path="/60400FEA/602F2578" Ref="Q?"  Part="1" 
AR Path="/60400F3C/602F2578" Ref="Q13"  Part="1" 
F 0 "Q13" H 6556 5046 50  0000 L CNN
F 1 "ZVN3306A" H 6556 4955 50  0000 L CNN
F 2 "Discretes:TO-92" H 6550 4925 50  0001 L CIN
F 3 "" H 6350 5000 50  0001 L CNN
	1    6350 5000
	1    0    0    -1  
$EndComp
Wire Wire Line
	4900 4550 6450 4550
Wire Wire Line
	6450 4550 6450 4800
Wire Wire Line
	4900 4250 4900 4550
Wire Wire Line
	7750 4550 6450 4550
Wire Wire Line
	7750 4250 7750 4550
Connection ~ 6450 4550
Wire Wire Line
	6450 4250 6450 4550
Text HLabel 5950 5000 0    50   Input ~ 0
D8
Wire Wire Line
	3050 3000 2900 3000
$Comp
L Device:R_US R?
U 1 1 605274ED
P 6050 5150
AR Path="/605274ED" Ref="R?"  Part="1" 
AR Path="/604011F4/605274ED" Ref="R?"  Part="1" 
AR Path="/60400F3C/605274ED" Ref="R13"  Part="1" 
F 0 "R13" H 6118 5196 50  0000 L CNN
F 1 "10K-CD" H 6118 5105 50  0000 L CNN
F 2 "Resistors:R0805" V 6090 5140 50  0001 C CNN
F 3 "~" H 6050 5150 50  0001 C CNN
	1    6050 5150
	-1   0    0    1   
$EndComp
Wire Wire Line
	6450 5350 6450 5300
Wire Wire Line
	6050 5300 6450 5300
Connection ~ 6450 5300
Wire Wire Line
	6450 5300 6450 5200
Wire Wire Line
	6050 5000 6150 5000
Wire Wire Line
	6050 5000 5950 5000
Connection ~ 6050 5000
$EndSCHEMATC
