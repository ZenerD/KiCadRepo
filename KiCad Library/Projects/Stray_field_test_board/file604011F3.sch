EESchema Schematic File Version 4
LIBS:stray_field_test_board-cache
EELAYER 26 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 6 6
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Wire Wire Line
	2250 1150 2450 1150
Wire Wire Line
	2100 1150 2250 1150
Connection ~ 2250 1150
$Comp
L Device:CP C?
U 1 1 60211F7D
P 2250 1300
AR Path="/60211F7D" Ref="C?"  Part="1" 
AR Path="/604011F4/60211F7D" Ref="C23"  Part="1" 
F 0 "C23" H 2000 1300 50  0000 L CNN
F 1 "10u-FH" H 2000 1200 50  0000 L CNN
F 2 "Capacitors:Radial_5mm_pol" H 2288 1150 50  0001 C CNN
F 3 "~" H 2250 1300 50  0001 C CNN
	1    2250 1300
	1    0    0    -1  
$EndComp
Wire Wire Line
	3200 1150 3350 1150
Connection ~ 3550 1150
$Comp
L Device:CP C?
U 1 1 60211F87
P 3550 1300
AR Path="/60211F87" Ref="C?"  Part="1" 
AR Path="/604011F4/60211F87" Ref="C26"  Part="1" 
F 0 "C26" H 3650 1300 50  0000 L CNN
F 1 "10u-FH" H 3450 1200 50  0000 L CNN
F 2 "Capacitors:Radial_5mm_pol" H 3588 1150 50  0001 C CNN
F 3 "~" H 3550 1300 50  0001 C CNN
	1    3550 1300
	1    0    0    -1  
$EndComp
$Comp
L Device:C C?
U 1 1 60211F8E
P 7750 3100
AR Path="/60211F8E" Ref="C?"  Part="1" 
AR Path="/604011F4/60211F8E" Ref="C33"  Part="1" 
F 0 "C33" H 7700 3200 50  0000 L CNN
F 1 "22pf-FG" H 7550 3000 50  0000 L CNN
F 2 "Capacitors:radial_3mm_cer" H 7788 2950 50  0001 C CNN
F 3 "~" H 7750 3100 50  0001 C CNN
	1    7750 3100
	1    0    0    -1  
$EndComp
$Comp
L Device:C C?
U 1 1 60211F95
P 7150 3100
AR Path="/60211F95" Ref="C?"  Part="1" 
AR Path="/604011F4/60211F95" Ref="C32"  Part="1" 
F 0 "C32" H 7100 3200 50  0000 L CNN
F 1 "22pf-BG" H 7150 3000 50  0000 L CNN
F 2 "Capacitors:C0603" H 7188 2950 50  0001 C CNN
F 3 "~" H 7150 3100 50  0001 C CNN
	1    7150 3100
	1    0    0    -1  
$EndComp
$Comp
L Device:R_US R?
U 1 1 60211F9C
P 6550 3600
AR Path="/60211F9C" Ref="R?"  Part="1" 
AR Path="/604011F4/60211F9C" Ref="R59"  Part="1" 
F 0 "R59" H 6618 3646 50  0000 L CNN
F 1 "10K-CD" H 6618 3555 50  0000 L CNN
F 2 "Resistors:R0805" V 6590 3590 50  0001 C CNN
F 3 "~" H 6550 3600 50  0001 C CNN
	1    6550 3600
	-1   0    0    1   
$EndComp
$Comp
L Device:R_US R?
U 1 1 60211FAA
P 3900 1350
AR Path="/60211FAA" Ref="R?"  Part="1" 
AR Path="/604011F4/60211FAA" Ref="R51"  Part="1" 
F 0 "R51" H 3968 1396 50  0000 L CNN
F 1 "220-CE" H 3968 1305 50  0000 L CNN
F 2 "Resistors:R0805" V 3940 1340 50  0001 C CNN
F 3 "~" H 3900 1350 50  0001 C CNN
	1    3900 1350
	1    0    0    -1  
$EndComp
$Comp
L Regulator_Linear:LM7805_TO220 U?
U 1 1 60211FB1
P 2900 1150
AR Path="/60211FB1" Ref="U?"  Part="1" 
AR Path="/604011F4/60211FB1" Ref="U11"  Part="1" 
F 0 "U11" H 2900 1392 50  0000 C CNN
F 1 "LM7805" H 2900 1301 50  0000 C CNN
F 2 "Discretes:TO-220" H 2900 1375 50  0001 C CIN
F 3 "" H 2900 1100 50  0001 C CNN
	1    2900 1150
	1    0    0    -1  
$EndComp
$Comp
L Transistor_FET:BS170 Q?
U 1 1 60240B70
P 6400 5200
AR Path="/60240B70" Ref="Q?"  Part="1" 
AR Path="/60400FEA/60240B70" Ref="Q?"  Part="1" 
AR Path="/604011F4/60240B70" Ref="Q12"  Part="1" 
F 0 "Q12" H 6605 5246 50  0000 L CNN
F 1 "ZVN3306A" H 6605 5155 50  0000 L CNN
F 2 "Discretes:TO-92" H 6600 5125 50  0001 L CIN
F 3 "" H 6400 5200 50  0001 L CNN
	1    6400 5200
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 60240B77
P 6500 5800
AR Path="/60240B77" Ref="#PWR?"  Part="1" 
AR Path="/60400FEA/60240B77" Ref="#PWR?"  Part="1" 
AR Path="/604011F4/60240B77" Ref="#PWR030"  Part="1" 
F 0 "#PWR030" H 6500 5550 50  0001 C CNN
F 1 "GND" H 6505 5627 50  0000 C CNN
F 2 "" H 6500 5800 50  0001 C CNN
F 3 "" H 6500 5800 50  0001 C CNN
	1    6500 5800
	1    0    0    -1  
$EndComp
Wire Wire Line
	6500 5400 6500 5700
Wire Wire Line
	6150 5650 6150 5700
Wire Wire Line
	6150 5700 6500 5700
Connection ~ 6500 5700
Wire Wire Line
	6500 5700 6500 5800
Wire Wire Line
	6150 5350 6150 5200
Wire Wire Line
	6150 5200 6200 5200
Connection ~ 6150 5200
Text HLabel 2100 1150 0    50   Input ~ 0
Batt_9V
Text HLabel 2250 1550 0    50   Input ~ 0
GND
Wire Wire Line
	2900 1450 2900 1550
Wire Wire Line
	2250 1550 2250 1450
Connection ~ 2900 1550
$Comp
L power:GND #PWR?
U 1 1 6025AC14
P 2900 1550
AR Path="/6025AC14" Ref="#PWR?"  Part="1" 
AR Path="/604011F4/6025AC14" Ref="#PWR025"  Part="1" 
F 0 "#PWR025" H 2900 1300 50  0001 C CNN
F 1 "GND" H 2905 1377 50  0000 C CNN
F 2 "" H 2900 1550 50  0001 C CNN
F 3 "" H 2900 1550 50  0001 C CNN
	1    2900 1550
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 6025AC40
P 7750 3250
AR Path="/6025AC40" Ref="#PWR?"  Part="1" 
AR Path="/604011F4/6025AC40" Ref="#PWR033"  Part="1" 
F 0 "#PWR033" H 7750 3000 50  0001 C CNN
F 1 "GND" H 7755 3077 50  0000 C CNN
F 2 "" H 7750 3250 50  0001 C CNN
F 3 "" H 7750 3250 50  0001 C CNN
	1    7750 3250
	1    0    0    -1  
$EndComp
Wire Wire Line
	5150 4950 5150 5100
$Comp
L Device:LED D9
U 1 1 6025B365
P 3900 1700
F 0 "D9" V 3938 1583 50  0000 R CNN
F 1 "D-CR" V 3847 1583 50  0000 R CNN
F 2 "Discretes:D0805" H 3900 1700 50  0001 C CNN
F 3 "~" H 3900 1700 50  0001 C CNN
	1    3900 1700
	0    -1   -1   0   
$EndComp
$Comp
L power:GND #PWR?
U 1 1 6025BC1A
P 3900 1950
AR Path="/6025BC1A" Ref="#PWR?"  Part="1" 
AR Path="/604011F4/6025BC1A" Ref="#PWR026"  Part="1" 
F 0 "#PWR026" H 3900 1700 50  0001 C CNN
F 1 "GND" H 3905 1777 50  0000 C CNN
F 2 "" H 3900 1950 50  0001 C CNN
F 3 "" H 3900 1950 50  0001 C CNN
	1    3900 1950
	1    0    0    -1  
$EndComp
Wire Wire Line
	3900 1950 3900 1850
Wire Wire Line
	3550 1150 3900 1150
Wire Wire Line
	3900 1200 3900 1150
Connection ~ 3900 1150
Wire Wire Line
	3900 1150 5150 1150
$Comp
L MCU_Microchip_ATmega:ATmega328P-PU U12
U 1 1 6025FCD2
P 5150 3450
F 0 "U12" H 4509 3496 50  0000 R CNN
F 1 "ATmega328P-PU" H 4509 3405 50  0000 R CNN
F 2 "Connectors:28_DIP_SOCKET" H 5150 3450 50  0001 C CIN
F 3 "" H 5150 3450 50  0001 C CNN
	1    5150 3450
	1    0    0    -1  
$EndComp
Text HLabel 5850 1150 2    50   Output ~ 0
5V
Text Label 4100 1150 2    50   ~ 0
5V
Wire Wire Line
	5150 1950 5150 1850
Wire Wire Line
	4550 2250 4550 1850
Wire Wire Line
	4550 1850 5150 1850
Connection ~ 5150 1850
$Comp
L Device:Crystal Y1
U 1 1 602617DD
P 7450 2950
F 0 "Y1" H 7450 2682 50  0000 C CNN
F 1 "Crystal" H 7450 2773 50  0000 C CNN
F 2 "Discretes:SMD_xtal" H 7450 2950 50  0001 C CNN
F 3 "~" H 7450 2950 50  0001 C CNN
	1    7450 2950
	-1   0    0    1   
$EndComp
$Comp
L power:GND #PWR?
U 1 1 60261EBD
P 5150 5100
AR Path="/60261EBD" Ref="#PWR?"  Part="1" 
AR Path="/604011F4/60261EBD" Ref="#PWR028"  Part="1" 
F 0 "#PWR028" H 5150 4850 50  0001 C CNN
F 1 "GND" H 5155 4927 50  0000 C CNN
F 2 "" H 5150 5100 50  0001 C CNN
F 3 "" H 5150 5100 50  0001 C CNN
	1    5150 5100
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 60261F3D
P 7150 3250
AR Path="/60261F3D" Ref="#PWR?"  Part="1" 
AR Path="/604011F4/60261F3D" Ref="#PWR031"  Part="1" 
F 0 "#PWR031" H 7150 3000 50  0001 C CNN
F 1 "GND" H 7155 3077 50  0000 C CNN
F 2 "" H 7150 3250 50  0001 C CNN
F 3 "" H 7150 3250 50  0001 C CNN
	1    7150 3250
	1    0    0    -1  
$EndComp
Wire Wire Line
	7750 2850 7750 2950
Wire Wire Line
	7600 2950 7750 2950
Connection ~ 7750 2950
Wire Wire Line
	7300 2950 7150 2950
Wire Wire Line
	5750 2950 6400 2950
Connection ~ 7150 2950
Wire Wire Line
	5750 3750 6550 3750
Connection ~ 6550 3750
$Comp
L power:GND #PWR?
U 1 1 60264D01
P 7300 4100
AR Path="/60264D01" Ref="#PWR?"  Part="1" 
AR Path="/604011F4/60264D01" Ref="#PWR032"  Part="1" 
F 0 "#PWR032" H 7300 3850 50  0001 C CNN
F 1 "GND" H 7305 3927 50  0000 C CNN
F 2 "" H 7300 4100 50  0001 C CNN
F 3 "" H 7300 4100 50  0001 C CNN
	1    7300 4100
	1    0    0    -1  
$EndComp
Wire Wire Line
	6550 3800 6550 3750
Text Label 6550 3450 0    50   ~ 0
5V
Text HLabel 5750 4350 2    50   Output ~ 0
D4
Text HLabel 5750 4550 2    50   Output ~ 0
D6
Text HLabel 5750 2350 2    50   Output ~ 0
D9
Text HLabel 5750 2450 2    50   Output ~ 0
D10
Text HLabel 5750 2550 2    50   Output ~ 0
D11
Text HLabel 5750 2650 2    50   Output ~ 0
D12
Text HLabel 5750 2750 2    50   Output ~ 0
D13
Text HLabel 5750 3150 2    50   Output ~ 0
A0
Text HLabel 5750 3250 2    50   Output ~ 0
A1
Connection ~ 6400 2950
Wire Wire Line
	6400 2950 6950 2950
$Comp
L power:GND #PWR?
U 1 1 6026760E
P 4700 1350
AR Path="/6026760E" Ref="#PWR?"  Part="1" 
AR Path="/604011F4/6026760E" Ref="#PWR027"  Part="1" 
F 0 "#PWR027" H 4700 1100 50  0001 C CNN
F 1 "GND" H 4705 1177 50  0000 C CNN
F 2 "" H 4700 1350 50  0001 C CNN
F 3 "" H 4700 1350 50  0001 C CNN
	1    4700 1350
	1    0    0    -1  
$EndComp
Wire Wire Line
	5250 1700 5250 1950
Connection ~ 5150 1150
Wire Wire Line
	5150 1150 5350 1150
$Comp
L pspice:INDUCTOR L2
U 1 1 60269D58
P 5450 1400
F 0 "L2" V 5150 1400 50  0000 L CNN
F 1 "10uH_ZD" H 5350 1500 50  0000 L CNN
F 2 "Inductors:Radial_5mm" H 5450 1400 50  0001 C CNN
F 3 "" H 5450 1400 50  0001 C CNN
	1    5450 1400
	0    1    1    0   
$EndComp
Connection ~ 5450 1150
Wire Wire Line
	5450 1150 5850 1150
Wire Wire Line
	5250 1700 5350 1700
Wire Wire Line
	5450 1650 5450 1700
Connection ~ 5450 1700
Wire Wire Line
	5450 1700 5500 1700
Wire Wire Line
	4800 1450 4850 1450
$Comp
L Device:C C?
U 1 1 6026F2D4
P 5650 1700
AR Path="/6026F2D4" Ref="C?"  Part="1" 
AR Path="/604011F4/6026F2D4" Ref="C29"  Part="1" 
F 0 "C29" V 5700 1500 50  0000 L CNN
F 1 ".1u-CC" V 5600 1750 50  0000 L CNN
F 2 "Capacitors:C0805" H 5688 1550 50  0001 C CNN
F 3 "~" H 5650 1700 50  0001 C CNN
	1    5650 1700
	0    1    1    0   
$EndComp
$Comp
L power:GND #PWR?
U 1 1 6026F324
P 6100 1900
AR Path="/6026F324" Ref="#PWR?"  Part="1" 
AR Path="/604011F4/6026F324" Ref="#PWR029"  Part="1" 
F 0 "#PWR029" H 6100 1650 50  0001 C CNN
F 1 "GND" H 6105 1727 50  0000 C CNN
F 2 "" H 6100 1900 50  0001 C CNN
F 3 "" H 6100 1900 50  0001 C CNN
	1    6100 1900
	1    0    0    -1  
$EndComp
Text HLabel 6100 4800 0    50   Input ~ 0
Car_VCC
Wire Wire Line
	5750 4650 6500 4650
Wire Wire Line
	6500 4650 6500 4950
Wire Wire Line
	6150 5150 6150 5200
$Comp
L Device:R_US R?
U 1 1 60274BFA
P 6900 4800
AR Path="/60274BFA" Ref="R?"  Part="1" 
AR Path="/60400FEA/60274BFA" Ref="R?"  Part="1" 
AR Path="/604011F4/60274BFA" Ref="R61"  Part="1" 
F 0 "R61" H 6832 4754 50  0000 R CNN
F 1 "10K-CD-DNP" H 6832 4845 50  0000 R CNN
F 2 "Resistors:R0805" V 6940 4790 50  0001 C CNN
F 3 "~" H 6900 4800 50  0001 C CNN
	1    6900 4800
	-1   0    0    1   
$EndComp
Wire Wire Line
	6900 4950 6500 4950
Connection ~ 6500 4950
Wire Wire Line
	6500 4950 6500 5000
Text Label 6900 4550 0    50   ~ 0
5V
Wire Wire Line
	3900 1550 3900 1500
$Comp
L Device:C C?
U 1 1 60322FD9
P 5650 1900
AR Path="/60322FD9" Ref="C?"  Part="1" 
AR Path="/604011F4/60322FD9" Ref="C30"  Part="1" 
F 0 "C30" V 5700 1700 50  0000 L CNN
F 1 ".1u-FC" V 5700 1950 50  0000 L CNN
F 2 "Capacitors:radial_3mm_cer" H 5688 1750 50  0001 C CNN
F 3 "~" H 5650 1900 50  0001 C CNN
	1    5650 1900
	0    1    1    0   
$EndComp
Wire Wire Line
	5800 1800 5800 1700
Wire Wire Line
	5800 1800 5800 1900
Connection ~ 5800 1800
Wire Wire Line
	5500 1900 5450 1900
Wire Wire Line
	5450 1900 5450 1700
$Comp
L pspice:INDUCTOR L1
U 1 1 6032548F
P 5350 1400
F 0 "L1" V 5050 1300 50  0000 L CNN
F 1 "10uH" H 5300 1350 50  0000 L CNN
F 2 "Inductors:L1008" H 5350 1400 50  0001 C CNN
F 3 "" H 5350 1400 50  0001 C CNN
	1    5350 1400
	0    1    1    0   
$EndComp
Connection ~ 5350 1150
Wire Wire Line
	5350 1150 5450 1150
Wire Wire Line
	5350 1650 5350 1700
Connection ~ 5350 1700
Wire Wire Line
	5350 1700 5450 1700
Wire Wire Line
	4850 1250 4800 1250
Wire Wire Line
	4800 1250 4800 1350
$Comp
L Device:CP C?
U 1 1 603281D6
P 3350 1300
AR Path="/603281D6" Ref="C?"  Part="1" 
AR Path="/604011F4/603281D6" Ref="C25"  Part="1" 
F 0 "C25" H 3100 1300 50  0000 L CNN
F 1 "10u-FI" H 3100 1200 50  0000 L CNN
F 2 "Capacitors:SMD_4mm_pol" H 3388 1150 50  0001 C CNN
F 3 "~" H 3350 1300 50  0001 C CNN
	1    3350 1300
	1    0    0    -1  
$EndComp
Connection ~ 3350 1150
Wire Wire Line
	3350 1150 3550 1150
$Comp
L Device:CP C?
U 1 1 6032821C
P 2450 1300
AR Path="/6032821C" Ref="C?"  Part="1" 
AR Path="/604011F4/6032821C" Ref="C24"  Part="1" 
F 0 "C24" H 2550 1300 50  0000 L CNN
F 1 "10u-FI" H 2350 1200 50  0000 L CNN
F 2 "Capacitors:SMD_4mm_pol" H 2488 1150 50  0001 C CNN
F 3 "~" H 2450 1300 50  0001 C CNN
	1    2450 1300
	1    0    0    -1  
$EndComp
Connection ~ 2450 1150
Wire Wire Line
	2450 1150 2600 1150
Wire Wire Line
	3550 1450 3350 1450
Wire Wire Line
	2450 1450 2250 1450
Connection ~ 2250 1450
Wire Wire Line
	2250 1550 2900 1550
Wire Wire Line
	3550 1550 3550 1450
Wire Wire Line
	2900 1550 3550 1550
Connection ~ 3550 1450
$Comp
L Device:R_US R?
U 1 1 603312E2
P 6550 3050
AR Path="/603312E2" Ref="R?"  Part="1" 
AR Path="/604011F4/603312E2" Ref="R58"  Part="1" 
F 0 "R58" V 6450 3000 50  0000 L CNN
F 1 "1M-CD" V 6600 2900 50  0000 L CNN
F 2 "Resistors:R0805" V 6590 3040 50  0001 C CNN
F 3 "~" H 6550 3050 50  0001 C CNN
	1    6550 3050
	0    -1   -1   0   
$EndComp
Wire Wire Line
	6700 3050 6800 3050
Wire Wire Line
	6800 3050 6800 2850
Connection ~ 6800 2850
Wire Wire Line
	6800 2850 7750 2850
Wire Wire Line
	6400 3050 6400 2950
$Comp
L Device:C C?
U 1 1 60333F60
P 6950 3100
AR Path="/60333F60" Ref="C?"  Part="1" 
AR Path="/604011F4/60333F60" Ref="C31"  Part="1" 
F 0 "C31" H 6900 3200 50  0000 L CNN
F 1 "22pf-FG" H 6750 3000 50  0000 L CNN
F 2 "Capacitors:radial_3mm_cer" H 6988 2950 50  0001 C CNN
F 3 "~" H 6950 3100 50  0001 C CNN
	1    6950 3100
	1    0    0    -1  
$EndComp
Connection ~ 6950 2950
Wire Wire Line
	6950 2950 7150 2950
$Comp
L Device:C C?
U 1 1 60333FAC
P 7950 3100
AR Path="/60333FAC" Ref="C?"  Part="1" 
AR Path="/604011F4/60333FAC" Ref="C34"  Part="1" 
F 0 "C34" H 7900 3200 50  0000 L CNN
F 1 "22pf-BG" H 7950 3000 50  0000 L CNN
F 2 "Capacitors:C0603" H 7988 2950 50  0001 C CNN
F 3 "~" H 7950 3100 50  0001 C CNN
	1    7950 3100
	1    0    0    -1  
$EndComp
Wire Wire Line
	7950 2950 7750 2950
Wire Wire Line
	7950 3250 7750 3250
Connection ~ 7750 3250
Wire Wire Line
	7150 3250 6950 3250
Connection ~ 7150 3250
$Comp
L Device:R_US R?
U 1 1 6034D2AD
P 6150 5500
AR Path="/6034D2AD" Ref="R?"  Part="1" 
AR Path="/60400FEA/6034D2AD" Ref="R?"  Part="1" 
AR Path="/604011F4/6034D2AD" Ref="R54"  Part="1" 
F 0 "R54" H 6082 5454 50  0000 R CNN
F 1 "10K-CD" V 6250 5650 50  0000 R CNN
F 2 "Resistors:R0805" V 6190 5490 50  0001 C CNN
F 3 "~" H 6150 5500 50  0001 C CNN
	1    6150 5500
	-1   0    0    1   
$EndComp
$Comp
L Device:R_US R?
U 1 1 6034D2FD
P 6150 5000
AR Path="/6034D2FD" Ref="R?"  Part="1" 
AR Path="/60400FEA/6034D2FD" Ref="R?"  Part="1" 
AR Path="/604011F4/6034D2FD" Ref="R53"  Part="1" 
F 0 "R53" H 6082 4954 50  0000 R CNN
F 1 "10K-CD" V 6250 5150 50  0000 R CNN
F 2 "Resistors:R0805" V 6190 4990 50  0001 C CNN
F 3 "~" H 6150 5000 50  0001 C CNN
	1    6150 5000
	-1   0    0    1   
$EndComp
Wire Wire Line
	6100 4800 6150 4800
Wire Wire Line
	6150 4800 6150 4850
Wire Wire Line
	6900 4550 6900 4650
Connection ~ 6900 4950
NoConn ~ 5750 3950
NoConn ~ 5750 4050
NoConn ~ 5750 4150
NoConn ~ 5750 4250
NoConn ~ 5750 3350
NoConn ~ 5750 3450
NoConn ~ 5750 3550
NoConn ~ 5750 3650
Text Label 5150 5050 0    50   ~ 0
GND
Text HLabel 7150 5050 2    50   Input ~ 0
Manual_key_sw
Wire Wire Line
	7150 5050 6900 5050
Wire Wire Line
	6900 5050 6900 4950
$Comp
L Connectors:NC_mom_on_off U13
U 1 1 6036BB11
P 7050 3950
F 0 "U13" H 7050 3750 50  0000 C CNN
F 1 "NC_mom_on_off" H 6600 3950 50  0000 C CNN
F 2 "Connectors:SMD_tactile" H 7050 3950 50  0001 C CNN
F 3 "" H 7050 3950 50  0001 C CNN
	1    7050 3950
	1    0    0    1   
$EndComp
$Comp
L Connectors:mom_on_off U14
U 1 1 60378663
P 7500 3950
F 0 "U14" H 7500 4150 50  0000 C CNN
F 1 "mom_on_off" H 7850 3950 50  0000 C CNN
F 2 "Connectors:tactile" H 7500 3950 50  0001 C CNN
F 3 "" H 7500 3950 50  0001 C CNN
	1    7500 3950
	1    0    0    -1  
$EndComp
NoConn ~ 7650 4050
NoConn ~ 6850 4050
Wire Wire Line
	7350 3850 7300 3850
Wire Wire Line
	7650 3850 7650 3800
Wire Wire Line
	6550 3800 6850 3800
Wire Wire Line
	6850 3850 6850 3800
Connection ~ 6850 3800
Wire Wire Line
	6850 3800 7300 3800
Wire Wire Line
	7300 3850 7300 3800
Connection ~ 7300 3850
Wire Wire Line
	7300 3850 7250 3850
Connection ~ 7300 3800
Wire Wire Line
	7300 3800 7650 3800
Wire Wire Line
	7250 4050 7300 4050
Wire Wire Line
	7300 4100 7300 4050
Connection ~ 7300 4050
Wire Wire Line
	7300 4050 7350 4050
Text Label 5800 4650 0    50   ~ 0
D7
Wire Wire Line
	5750 2850 6800 2850
Text HLabel 5750 2250 2    50   Output ~ 0
D8
Text HLabel 5750 4450 2    50   Output ~ 0
D5
Wire Wire Line
	4800 1350 4700 1350
Connection ~ 4800 1350
Wire Wire Line
	4800 1350 4800 1450
Wire Wire Line
	6100 1900 6100 1800
Wire Wire Line
	5800 1800 6100 1800
$Comp
L Device:C C?
U 1 1 60497C4C
P 5000 1250
AR Path="/6041298C/60497C4C" Ref="C?"  Part="1" 
AR Path="/604011F4/60497C4C" Ref="C27"  Part="1" 
F 0 "C27" H 5050 1350 50  0000 L CNN
F 1 "1u-CC" H 5050 1150 50  0000 L CNN
F 2 "Capacitors:C0805" H 5038 1100 50  0001 C CNN
F 3 "~" H 5000 1250 50  0001 C CNN
	1    5000 1250
	0    -1   -1   0   
$EndComp
$Comp
L Device:C C?
U 1 1 60497C53
P 5000 1450
AR Path="/6041298C/60497C53" Ref="C?"  Part="1" 
AR Path="/604011F4/60497C53" Ref="C28"  Part="1" 
F 0 "C28" H 4800 1550 50  0000 L CNN
F 1 "1u-FD" H 4700 1350 50  0000 L CNN
F 2 "Capacitors:radial_5mm_cer" H 5038 1300 50  0001 C CNN
F 3 "~" H 5000 1450 50  0001 C CNN
	1    5000 1450
	0    -1   -1   0   
$EndComp
Wire Wire Line
	5150 1150 5150 1250
Wire Wire Line
	5150 1250 5150 1450
Connection ~ 5150 1250
Wire Wire Line
	5150 1450 5150 1850
Connection ~ 5150 1450
$EndSCHEMATC
