EESchema Schematic File Version 4
LIBS:stray_field_test_board-cache
EELAYER 26 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 4 6
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Comparator:LM339 U?
U 5 1 6022EA9F
P 3350 3550
AR Path="/6022EA9F" Ref="U?"  Part="5" 
AR Path="/60412950/6022EA9F" Ref="U9"  Part="5" 
F 0 "U9" H 3308 3596 50  0000 L CNN
F 1 "LM339" H 3308 3505 50  0000 L CNN
F 2 "ICs:14_DIP" H 3300 3650 50  0001 C CNN
F 3 "http://www.ti.com/lit/ds/symlink/lm339.pdf" H 3400 3750 50  0001 C CNN
	5    3350 3550
	1    0    0    -1  
$EndComp
Text HLabel 3150 3250 0    50   Input ~ 0
5V
Wire Wire Line
	3150 3250 3250 3250
Wire Wire Line
	3650 3400 3650 3250
Wire Wire Line
	3650 3250 3250 3250
Wire Wire Line
	3650 3700 3650 3850
Wire Wire Line
	3650 3850 3250 3850
$Comp
L Comparator:LM339 U?
U 2 1 6022EAB9
P 6100 3650
AR Path="/6022EAB9" Ref="U?"  Part="2" 
AR Path="/60412950/6022EAB9" Ref="U9"  Part="2" 
F 0 "U9" H 6100 4017 50  0000 C CNN
F 1 "LM339" H 6100 3926 50  0000 C CNN
F 2 "ICs:14_DIP" H 6050 3750 50  0001 C CNN
F 3 "http://www.ti.com/lit/ds/symlink/lm339.pdf" H 6150 3850 50  0001 C CNN
	2    6100 3650
	1    0    0    1   
$EndComp
$Comp
L Device:R_US R?
U 1 1 6022EAC0
P 6450 3400
AR Path="/6022EAC0" Ref="R?"  Part="1" 
AR Path="/60412950/6022EAC0" Ref="R46"  Part="1" 
F 0 "R46" H 6518 3446 50  0000 L CNN
F 1 "1k-CD" H 6518 3355 50  0000 L CNN
F 2 "Resistors:R0805" V 6490 3390 50  0001 C CNN
F 3 "~" H 6450 3400 50  0001 C CNN
	1    6450 3400
	1    0    0    -1  
$EndComp
Text HLabel 3450 4000 0    50   Input ~ 0
GND
Wire Wire Line
	6450 3550 6450 3650
Wire Wire Line
	6450 3650 6400 3650
Wire Wire Line
	6600 3650 6450 3650
Connection ~ 6450 3650
$Comp
L Device:R_US R?
U 1 1 6022EAD3
P 4950 3650
AR Path="/6022EAD3" Ref="R?"  Part="1" 
AR Path="/60412950/6022EAD3" Ref="R43"  Part="1" 
F 0 "R43" V 4745 3650 50  0000 C CNN
F 1 "1M-FE" V 4836 3650 50  0000 C CNN
F 2 "Resistors:R-thru" V 4990 3640 50  0001 C CNN
F 3 "~" H 4950 3650 50  0001 C CNN
	1    4950 3650
	0    1    1    0   
$EndComp
Text HLabel 4800 3500 0    50   Input ~ 0
D6
Wire Wire Line
	4800 3500 4800 3650
Text HLabel 5350 3550 0    50   Output ~ 0
A1
Text HLabel 5350 3750 0    50   Output ~ 0
A0
$Comp
L Transistor_FET:IRLIZ44N Q?
U 1 1 6022EADF
P 6800 3650
AR Path="/6022EADF" Ref="Q?"  Part="1" 
AR Path="/60412950/6022EADF" Ref="Q11"  Part="1" 
F 0 "Q11" H 7005 3696 50  0000 L CNN
F 1 "PSMN1R1" H 7005 3605 50  0000 L CNN
F 2 "Discretes:TO-220" H 7050 3575 50  0001 L CIN
F 3 "" H 6800 3650 50  0001 L CNN
	1    6800 3650
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 6022EAE6
P 5300 3050
AR Path="/6022EAE6" Ref="#PWR?"  Part="1" 
AR Path="/60412950/6022EAE6" Ref="#PWR019"  Part="1" 
F 0 "#PWR019" H 5300 2800 50  0001 C CNN
F 1 "GND" H 5305 2877 50  0000 C CNN
F 2 "" H 5300 3050 50  0001 C CNN
F 3 "" H 5300 3050 50  0001 C CNN
	1    5300 3050
	1    0    0    -1  
$EndComp
$Comp
L Device:R_US R?
U 1 1 6022EAED
P 6900 3200
AR Path="/6022EAED" Ref="R?"  Part="1" 
AR Path="/60412950/6022EAED" Ref="R47"  Part="1" 
F 0 "R47" H 6968 3246 50  0000 L CNN
F 1 "1K_FE-dnp" H 6968 3155 50  0000 L CNN
F 2 "Resistors:R-thru" V 6940 3190 50  0001 C CNN
F 3 "~" H 6900 3200 50  0001 C CNN
	1    6900 3200
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 6022EAF5
P 6900 3850
AR Path="/6022EAF5" Ref="#PWR?"  Part="1" 
AR Path="/60412950/6022EAF5" Ref="#PWR020"  Part="1" 
F 0 "#PWR020" H 6900 3600 50  0001 C CNN
F 1 "GND" H 6905 3677 50  0000 C CNN
F 2 "" H 6900 3850 50  0001 C CNN
F 3 "" H 6900 3850 50  0001 C CNN
	1    6900 3850
	1    0    0    -1  
$EndComp
Wire Wire Line
	6900 3350 6900 3400
Wire Wire Line
	6900 3400 7100 3400
Wire Wire Line
	6900 3450 6900 3400
Connection ~ 6900 3400
Text HLabel 7100 3400 2    50   Output ~ 0
Car_OUT
Connection ~ 3250 3250
Wire Wire Line
	5100 3650 5400 3650
Wire Wire Line
	5400 3650 5400 3550
Wire Wire Line
	5400 3550 5350 3550
Connection ~ 5400 3550
Wire Wire Line
	5350 3750 5800 3750
Wire Wire Line
	5400 3550 5800 3550
Wire Wire Line
	5400 3400 5400 3550
Wire Wire Line
	6450 2900 3650 2900
Wire Wire Line
	3650 2900 3650 3250
Connection ~ 3650 3250
$Comp
L Device:R_US R?
U 1 1 6028276A
P 4950 3800
AR Path="/6028276A" Ref="R?"  Part="1" 
AR Path="/60412950/6028276A" Ref="R44"  Part="1" 
F 0 "R44" V 4745 3800 50  0000 C CNN
F 1 "1M-CD" V 4836 3800 50  0000 C CNN
F 2 "Resistors:R0805" V 4990 3790 50  0001 C CNN
F 3 "~" H 4950 3800 50  0001 C CNN
	1    4950 3800
	0    1    1    0   
$EndComp
Connection ~ 5100 3650
Connection ~ 4800 3650
$Comp
L Device:C C?
U 1 1 602FB221
P 5900 3200
AR Path="/602FB221" Ref="C?"  Part="1" 
AR Path="/60400F3C/602FB221" Ref="C?"  Part="1" 
AR Path="/60412950/602FB221" Ref="C20"  Part="1" 
F 0 "C20" H 6015 3246 50  0000 L CNN
F 1 ".1u-FC" H 6015 3155 50  0000 L CNN
F 2 "Capacitors:radial_3mm_cer" H 5938 3050 50  0001 C CNN
F 3 "~" H 5900 3200 50  0001 C CNN
	1    5900 3200
	1    0    0    -1  
$EndComp
$Comp
L Device:C C?
U 1 1 602FB228
P 5700 3200
AR Path="/602FB228" Ref="C?"  Part="1" 
AR Path="/60400F3C/602FB228" Ref="C?"  Part="1" 
AR Path="/60412950/602FB228" Ref="C19"  Part="1" 
F 0 "C19" H 5815 3246 50  0000 L CNN
F 1 ".1u-CC" H 5815 3155 50  0000 L CNN
F 2 "Capacitors:C0805" H 5738 3050 50  0001 C CNN
F 3 "~" H 5700 3200 50  0001 C CNN
	1    5700 3200
	1    0    0    -1  
$EndComp
Wire Wire Line
	5700 3050 5700 3000
Wire Wire Line
	5700 3000 5900 3000
Wire Wire Line
	5900 3000 5900 3050
Connection ~ 5700 3000
Wire Wire Line
	5900 3350 5900 3400
Wire Wire Line
	5400 3400 5700 3400
Wire Wire Line
	5700 3350 5700 3400
Connection ~ 5700 3400
Wire Wire Line
	5700 3400 5900 3400
Wire Wire Line
	3650 3850 3850 3850
Wire Wire Line
	3850 3850 3850 3700
Connection ~ 3650 3850
Wire Wire Line
	3850 3400 3850 3250
Wire Wire Line
	3850 3250 3650 3250
Text Label 5450 3000 0    50   ~ 0
GND
Wire Wire Line
	5300 3050 5300 3000
Wire Wire Line
	5300 3000 5700 3000
Wire Wire Line
	5100 3800 5100 3650
Wire Wire Line
	4800 3800 4800 3650
$Comp
L power:GND #PWR?
U 1 1 6030E6E8
P 6450 4550
AR Path="/6030E6E8" Ref="#PWR?"  Part="1" 
AR Path="/60400F3C/6030E6E8" Ref="#PWR?"  Part="1" 
AR Path="/60412950/6030E6E8" Ref="#PWR09"  Part="1" 
F 0 "#PWR09" H 6450 4300 50  0001 C CNN
F 1 "GND" H 6455 4377 50  0000 C CNN
F 2 "" H 6450 4550 50  0001 C CNN
F 3 "" H 6450 4550 50  0001 C CNN
	1    6450 4550
	1    0    0    -1  
$EndComp
$Comp
L Transistor_FET:BS170 Q?
U 1 1 6030E6EE
P 6350 4100
AR Path="/6030E6EE" Ref="Q?"  Part="1" 
AR Path="/60400FEA/6030E6EE" Ref="Q?"  Part="1" 
AR Path="/60400F3C/6030E6EE" Ref="Q?"  Part="1" 
AR Path="/60412950/6030E6EE" Ref="Q14"  Part="1" 
F 0 "Q14" H 6556 4146 50  0000 L CNN
F 1 "ZVN3306A" H 6556 4055 50  0000 L CNN
F 2 "Discretes:TO-92" H 6550 4025 50  0001 L CIN
F 3 "" H 6350 4100 50  0001 L CNN
	1    6350 4100
	1    0    0    -1  
$EndComp
Wire Wire Line
	6450 3650 6450 3900
Text HLabel 5900 4100 0    50   Input ~ 0
D5
Wire Wire Line
	6450 4500 6450 4300
Wire Wire Line
	6450 4550 6450 4500
Connection ~ 6450 4500
Wire Wire Line
	5900 4100 6050 4100
$Comp
L Device:R_US R?
U 1 1 60318E35
P 6050 4300
AR Path="/60318E35" Ref="R?"  Part="1" 
AR Path="/60412950/60318E35" Ref="R10"  Part="1" 
F 0 "R10" H 6118 4346 50  0000 L CNN
F 1 "10K-CD" H 6118 4255 50  0000 L CNN
F 2 "Resistors:R0805" V 6090 4290 50  0001 C CNN
F 3 "~" H 6050 4300 50  0001 C CNN
	1    6050 4300
	1    0    0    -1  
$EndComp
Wire Wire Line
	6050 4150 6050 4100
Connection ~ 6050 4100
Wire Wire Line
	6050 4100 6150 4100
Wire Wire Line
	6050 4450 6050 4500
Wire Wire Line
	6050 4500 6450 4500
$Comp
L power:GND #PWR?
U 1 1 6042B379
P 3650 4100
AR Path="/6042B379" Ref="#PWR?"  Part="1" 
AR Path="/60400F3C/6042B379" Ref="#PWR?"  Part="1" 
AR Path="/60412950/6042B379" Ref="#PWR0107"  Part="1" 
F 0 "#PWR0107" H 3650 3850 50  0001 C CNN
F 1 "GND" H 3655 3927 50  0000 C CNN
F 2 "" H 3650 4100 50  0001 C CNN
F 3 "" H 3650 4100 50  0001 C CNN
	1    3650 4100
	1    0    0    -1  
$EndComp
Wire Wire Line
	3650 4100 3650 4000
Wire Wire Line
	3450 4000 3650 4000
Connection ~ 3650 4000
Wire Wire Line
	3650 4000 3650 3850
$Comp
L Device:C C?
U 1 1 60495BC7
P 3650 3550
AR Path="/6041298C/60495BC7" Ref="C?"  Part="1" 
AR Path="/60412950/60495BC7" Ref="C17"  Part="1" 
F 0 "C17" H 3700 3650 50  0000 L CNN
F 1 "1u-CC" H 3700 3450 50  0000 L CNN
F 2 "Capacitors:C0805" H 3688 3400 50  0001 C CNN
F 3 "~" H 3650 3550 50  0001 C CNN
	1    3650 3550
	-1   0    0    -1  
$EndComp
$Comp
L Device:C C?
U 1 1 60495BCE
P 3850 3550
AR Path="/6041298C/60495BCE" Ref="C?"  Part="1" 
AR Path="/60412950/60495BCE" Ref="C18"  Part="1" 
F 0 "C18" H 3650 3650 50  0000 L CNN
F 1 "1u-FD" H 3550 3450 50  0000 L CNN
F 2 "Capacitors:radial_5mm_cer" H 3888 3400 50  0001 C CNN
F 3 "~" H 3850 3550 50  0001 C CNN
	1    3850 3550
	-1   0    0    -1  
$EndComp
Wire Wire Line
	6450 2900 6450 3250
Wire Wire Line
	6900 3050 7100 3050
Text HLabel 7100 3050 2    50   Input ~ 0
Car_VCC
$EndSCHEMATC
