%%After loading kicad CSV, spits out a "real" BOM
kicadCSV = xlsread('?C:\Users\rhilton\Documents\KiCadRepo\KiCad Library\Projects\Sixteen_One\sixteen_one.csv');
b=1;
PartNumber = {};
Ref = {};
Quantity = {};
for a = 2:length(DigikeyPN)
    switch DigikeyPN{a}
        case PartNumber
            temp = find(strcmpi(DigikeyPN{a},PartNumber),1,'first');
            Quantity{temp} = Quantity{temp}+1;
            Ref{temp} = [Ref{temp}, ', ', Reference{a}];
        otherwise
            PartNumber{b} = DigikeyPN{a};
            Ref{b} = Reference{a};
            Quantity{b} = 1;
            b=b+1;
    end
end

DiscriptionOut = {};
ManufacturerPNOut = {};

for a = 1:length(PartNumber)
    in = find(strcmpi(PartNumber{a}, Digikey));
    if(isempty(in))
        disp(['No Part found for ' PartNumber{a}])
    else
        DiscriptionOut{a} = Discription{in};
        ManufacturerPNOut{a} = ManufacturerPN{in};
    end
end
    

[~,Index] = sort(Ref);
PartNumber = {'Digikey Part Number', PartNumber{Index}};
Ref = {'Component Reference', Ref{Index}};
Quantity = {'Quantity', Quantity{Index}};
DiscriptionOut = {'Discription', DiscriptionOut{Index}};
ManufacturerPNOut = {'Manufacturer Part Number', ManufacturerPNOut{Index}};
xlswrite('BOM.xls',[Quantity',Ref',PartNumber',DiscriptionOut',ManufacturerPNOut'])