TextIn = importdata('C:\Users\bgeiger\Desktop\Q12.txt',' ',5000);
% for a = 1:length(TextIn)
%     if(~isempty(regexp(TextIn{a},'LINE')))
%         out = regexp(TextIn{a},' ');
%         x1 = str2num(TextIn{a}(out(1):out(2)));
%         x2 = str2num(TextIn{a}(out(3):out(4)));
%         y1 = str2num(TextIn{a}(out(2):out(3)));
%         y2 = str2num(TextIn{a}(out(4):end));
%         line([x1,x2],[y1,y2])
%         hold on
%     elseif(~isempty(regexp(TextIn{a},'PAD')))
%         out = regexp(TextIn{a},' ');
%         x1 = str2num(TextIn{a}(out(3):out(4)));
%         y1 = str2num(TextIn{a}(out(4):out(5)));
%         text(x1,y1,['p' TextIn{a}(out(1):out(2))])
%         hold on
%     end
% end

for a = 1:length(TextIn)
    if(~isempty(regexp(TextIn{a},'BOTTOM')) &&...
            ~isempty(regexp(TextIn{a},'PAD')))
        out = regexp(TextIn{a},' ');
        if(str2num(TextIn{a}(out(4):out(5))) == 0)
            TextOut{a,1} = TextIn{a};
        elseif(str2num(TextIn{a}(out(4):out(5))) < 0)
            TextOut{a,1} = TextIn{a}([1:out(4),out(4)+2:end]);
        else
            TextOut{a,1} = [TextIn{a}([1:out(4)]) '-' ,TextIn{a}([out(4)+1:end])];
        end
    else
        TextOut{a,1} = TextIn{a};
    end
end

fid = fopen('C:\Users\bgeiger\Desktop\ShapesFixed.txt','wt');
for a = 1:length(TextOut)
    fprintf(fid,[TextOut{a} char(10)]);
end
fclose(fid)

